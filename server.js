'use strict';

process.env.NODE_ENV = 'development';
global.__base = __dirname + '/';

// Include config
const config = require('./server/config/env'),
app = require('./server/config/express');

// Create server

app.listen(config.port, function listen() {
  console.log('Server listening on port '+config.port+'!');
});
