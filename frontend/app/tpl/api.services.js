(function() {
  'use strict';
  var app = angular.module('mean');
    app.factory('ApiServices', function($http, $q, $localStorage, $rootScope, $window, $location ) {

      var authToken = '',
      //baseUrl = 'http://182.75.177.246:8181/wberc/api',
      baseUrl = 'http://182.75.177.246:8181/wberc_v2/api',
      // localUrl = 'http://localhost:4001/app/',
      localUrl = 'http://staging.techl33t.com:4001/app/',
      //baseUrl = 'http://182.75.177.246:8181/wberc_staging/api',

      startProcess = 'http://182.75.177.246:8080/engine-rest/process-definition/arrpetition-approval:4:c881f77a-f77e-11e7-aa4a-484d7ec32400/start',

      ApiServices = {};
      $rootScope.loadSuccessfull = false;
        // console.log('$rootScope.formIdClick: ', $rootScope.formIdClick);
      ApiServices.loggedIn = function() {
        if (localStorage.getItem("token") == null || localStorage.getItem("user") == null) {
          return false;
        }else{
          return true;
        }
      };

      ApiServices.loadToken = function(){
        var token = localStorage.getItem('token');
        authToken = token;
      };

      ApiServices.getAuthUser = function(){
        return JSON.parse(localStorage.getItem("user"));
      };
      //console.log('$localStorage',$localStorage);
    	var Phid = $localStorage.petitionId;
    	var utilid = $localStorage.utilId;
    	var formid = $localStorage.formId;
    	var stationid = $localStorage.stationId;
      var pyid =   $localStorage.pyid;
      //var userid =   $localstorage.userId;

  ApiServices.sidebarMenu = function(callback){
    var cb = callback || angular.noop;
    var deferred = $q.defer();
    return $http.get(baseUrl+'/getAnnexureList?phid='+$localStorage.petitionId+'&utilid='+$localStorage.utilId,
      {
      headers: {
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
      }
    ).success(function(data) {
        deferred.resolve(data);
        $rootScope.loadSuccessfull = true;
        return cb(data);
      }).error(function(err) {
          deferred.reject(err);

          return cb(err);
      }.bind(this));
      return deferred.promise;
  };
/*create new petition start*/
ApiServices.reqPetition = function(){
  var deferred = $q.defer();
  $http({
    url : baseUrl+'/pbyear',
    method : 'GET',
    headers: {
          "Authorization": 'Bearer ' + localStorage.getItem('token')
      }
  }).success(function(param, status, headers, config){
      deferred.resolve(param);
      $rootScope.loadSuccessfull = true;
  }).error(function(){
       deferred.reject('data can\'t be retrieved');

  });
  return deferred.promise;
};

ApiServices.setBaseYearFn = function(petitionBaseYear,petitionPrev,petitionLength){
    var deferred = $q.defer();
    $http({
			url : baseUrl+'/new_pyear_msg?base_year='+petitionBaseYear[1]+'&prev_year='+petitionPrev+'&ensu_year='+petitionLength,
			method : 'get',
	    headers: {
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
		}).success(function(data, status, headers, config){
        deferred.resolve(data);
        $rootScope.loadSuccessfull = true;
        //console.log('submitting base_year: '+JSON.stringify(data));
		}).error(function(){
		     deferred.reject('data can\'t be retrieved');

		});
  	return deferred.promise;
};
ApiServices.confirmYearFn = function(petitionBaseYear,petitionPrev,petitionLength,utilId,userId){
    var deferred = $q.defer();
    $http({
      url : baseUrl+'/create_new_petition',
      method : 'POST',
      data: {
        'base_year': petitionBaseYear[1],
        'prev_year': petitionPrev,
        'ensu_year': petitionLength,
        'utilid': utilId,
        'userid': userId,
        "module":"ARR",
      },
      headers: {
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
    }).success(function(data, status, headers, config){
        deferred.resolve(data);
        $localStorage.petitionId = data.data.phid;
        $localStorage.utilId = data.data.utilid;
        $rootScope.loadSuccessfull = true;
        //console.log('data confirmMsg service', data);
    }).error(function(){
         deferred.reject('data can\'t be retrieved');

    });
    return deferred.promise;
};

/*create new petition end*/
    ApiServices.checkLogin = function(user, callback){
      var cb = callback || angular.noop;
      var deferred = $q.defer();
      //console.log("test Login1", user);
      return $http.post(baseUrl+'/login', user,{
      headers: {
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
      })
      .success(function(data) {
        //console.log("test Login1", data);
        deferred.resolve(data);
        $rootScope.loadSuccessfull = true;
        return cb(data);
      }).error(function(err) {
          deferred.reject(err);

          return cb(err);
      }.bind(this));

      return deferred.promise;;
    };
    ApiServices.requestData = function(ApiEndPoints, callback){
      var cb = callback || angular.noop;
      var deferred = $q.defer();
      var url = '';
      return $http.get(baseUrl+ApiEndPoints,  {
        headers: {
             "Authorization": 'Bearer ' + localStorage.getItem('token')
         }
       })
      .success(function(data) {
        deferred.resolve(data);
        $rootScope.loadSuccessfull = true;
        return cb(data);
      }).error(function(err) {
          deferred.reject(err);

          return cb(err);
      }.bind(this));

      return deferred.promise;;
    };
    ApiServices.requestDataWithDropdown = function(ApiEndPoints,callback){
      var cb = callback || angular.noop;
      var deferred = $q.defer();
      var url = '';
      return $http.get(baseUrl+ApiEndPoints,  {
        headers: {
              "Authorization": 'Bearer ' + localStorage.getItem('token')
         }
       })
      .success(function(data) {
        deferred.resolve(data);
        $rootScope.loadSuccessfull = true;
        return cb(data);
      }).error(function(err) {
          deferred.reject(err);

          return cb(err);
      }.bind(this));

      return deferred.promise;;
    };

    ApiServices.storeUser = function(user,token){
      localStorage.setItem('user',JSON.stringify(user));
      localStorage.setItem('token',token);
    };


  ApiServices.getSubstaions = function(post, callback){
      var cb = callback || angular.noop;
      var deferred = $q.defer();
      return $http.post(baseUrl+'/typewisestation', post,{
      headers: {
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
      })
      .success(function(data) {
        deferred.resolve(data);
        $rootScope.loadSuccessfull = true;
        return cb(data);
      }).error(function(err) {
          deferred.reject(err);

          return cb(err);
      }.bind(this));

      return deferred.promise;;
    };

    /******For form 1.3********/
    ApiServices.getSeasonalSubstaions = function(post, callback){
      var cb = callback || angular.noop;
      var deferred = $q.defer();
      //console.log("test Login1", user);
      return $http.post(baseUrl+'/typewisestation', post,{
      headers: {
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
      })
      .success(function(data) {
        //console.log("test Login1", data);
        deferred.resolve(data);
        $rootScope.loadSuccessfull = true;
        return cb(data);
      }).error(function(err) {
          deferred.reject(err);

          return cb(err);
      }.bind(this));

      return deferred.promise;;
    };
/******For form 1.3********/
ApiServices.getSubstaionData = function(post, url, callback){
  var cb = callback || angular.noop;
  var deferred = $q.defer();
  //console.log("test Login1", user);
  return $http.post(baseUrl+url, post,{
  headers: {
        "Authorization": 'Bearer ' + localStorage.getItem('token')
    }
  })
  .success(function(data) {
    //console.log("test Login1", data);
    deferred.resolve(data);
    $rootScope.loadSuccessfull = true;
    return cb(data);
  }).error(function(err) {
      deferred.reject(err);

      return cb(err);
  }.bind(this));

  return deferred.promise;;
};

ApiServices.setSubstaionData1 = function(post, callback){
    var base_url = '';
    var cb = callback || angular.noop;
    var deferred = $q.defer();
    return $http.post(baseUrl+'/'+post.url, post,{
    headers: {
          "Authorization": 'Bearer ' + localStorage.getItem('token')
      }
    })
    .success(function(data) {
      deferred.resolve(data);
      $rootScope.loadSuccessfull = true;
      return cb(data);
    }).error(function(err) {
        deferred.reject(err);

        return cb(err);
    }.bind(this));

    return deferred.promise;
};

ApiServices.setSubstaionData2 = function(post, callback){
      var cb = callback || angular.noop;
      var deferred = $q.defer();
      return $http.post(baseUrl+'/'+post.url, post,{
      headers: {
            "Authorization": 'Bearer ' + localStorage.getItem('token')
        }
      })
      .success(function(data) {
        deferred.resolve(data);
        $rootScope.loadSuccessfull = true;
        return cb(data);
      }).error(function(err) {
          deferred.reject(err);

          return cb(err);
      }.bind(this));

      return deferred.promise;
  };

ApiServices.getUnitstaions = function(post, callback){
    var cb = callback || angular.noop;
    var deferred = $q.defer();
    return $http.post(baseUrl+'/stationwiseunit', post,{
    headers: {
          "Authorization": 'Bearer ' + localStorage.getItem('token')
      }
    })
    .success(function(data) {
      deferred.resolve(data);
      $rootScope.loadSuccessfull = true;
      return cb(data);
    }).error(function(err) {
        deferred.reject(err);

        return cb(err);
    }.bind(this));

    return deferred.promise;;
  };

ApiServices.startProcess = function(post, callback){
    var cb = callback || angular.noop;
    var deferred = $q.defer();
    return $http.post(startProcess, post,{
    headers: {
          "Authorization": 'Bearer ' + localStorage.getItem('token')
      }
    })
    .success(function(data) {
      deferred.resolve(data);
      $rootScope.loadSuccessfull = true;
      return cb(data);
    }).error(function(err) {
        deferred.reject(err);

        return cb(err);
    }.bind(this));

    return deferred.promise;;
  };
ApiServices.getRequest = function(url, callback){
  var cb = callback || angular.noop;
  var deferred = $q.defer();
   var post = '';
  return $http.get(url,{
  headers: {
        "Authorization": 'Bearer ' + localStorage.getItem('token')
    }
  })
  .success(function(data) {
    deferred.resolve(data);
    $rootScope.loadSuccessfull = true;
    return cb(data);
  }).error(function(err) {
       deferred.reject(err);

      return cb(err);
  }.bind(this));

  return deferred.promise;
};

 ApiServices.PostRequest = function(url,post, callback){
  var cb = callback || angular.noop;
  var deferred = $q.defer();
  return $http.post(url,post,{
  headers: {
        "Authorization": 'Bearer ' + localStorage.getItem('token'),
        "Content-Type": undefined
    }
  })
  .success(function(data) {
    deferred.resolve(data);
    $rootScope.loadSuccessfull = true;
    return cb(data);
  }).error(function(err) {
	deferred.reject(err);

        return cb(err);
    }.bind(this));

  return deferred.promise;
};

ApiServices.PostGlobalFileUpload = function(url, formdata, callback){
   var cb = callback || angular.noop;
   var deferred = $q.defer();
   return $http.post(url, formdata, {
      headers: {'Content-Type': undefined}
   }).success(function(data) {
     deferred.resolve(data);
     $rootScope.loadSuccessfull = true;
     return cb(data);
   }).error(function(err) {
     deferred.reject(err);

       return cb(err);
   }.bind(this));

   return deferred.promise;
};

ApiServices.getFileDownload = function(url, callback){
  var cb = callback || angular.noop;
  var deferred = $q.defer();
  return $http.get(url, {
     headers:  {
           "Authorization": 'Bearer ' + localStorage.getItem('token')
           //"Content-Type": undefined
       }
  }).success(function(data) {
    deferred.resolve(data);
    $rootScope.loadSuccessfull = true;
    return cb(data);
  }).error(function(err) {
    deferred.reject(err);

      return cb(err);
  }.bind(this));

  return deferred.promise;
};

ApiServices.GetAllAttachFiles = function( data, callback){
       var cb = callback || angular.noop;
       var deferred = $q.defer();

      return $http.get(baseUrl+'/file_listing_generic?ph_id='+$localStorage.petitionId,{
       headers: {
             "Authorization": 'Bearer ' + localStorage.getItem('token')
         }
       })
       .success(function(data) {
         deferred.resolve(data);
         $rootScope.loadSuccessfull = true;
         return cb(data.data.fileList);
       }).error(function(err) {
           deferred.reject(err);

           return cb(err);
       }.bind(this));

       return deferred.promise;;
};

/*attachment start*/
ApiServices.attachmentType = function(){
    var deferred = $q.defer();
    $http({
    url : baseUrl+'/attachment_type_listing?form_id='+$localStorage.showTypeRef,
    method : 'GET',
    headers: {
        "Authorization": 'Bearer ' + localStorage.getItem('token')
    }
    }).success(function(param, status, headers, config){
        deferred.resolve(param);
        $rootScope.loadSuccessfull = true;
    }).error(function(){
        deferred.reject('data can\'t be retrieved');

    });
    return deferred.promise;
};
ApiServices.attachmentTypeForm = function(){
  var deferred = $q.defer();
  $http({
  url : baseUrl+'/attachment_type_listing?form_id='+$localStorage.formId,
  method : 'GET',
  headers: {
      "Authorization": 'Bearer ' + localStorage.getItem('token')
  }
  }).success(function(param, status, headers, config){
    $rootScope.loadSuccessfull = true;
      deferred.resolve(param);
  }).error(function(){
      deferred.reject('data can\'t be retrieved');

  });
  return deferred.promise;
};
ApiServices.attachFileList = function(){
    var deferred = $q.defer();
    $http({
    url : baseUrl+'/file_listing?ph_id='+$localStorage.petitionId+'&form_id='+$localStorage.formId,
    method : 'GET',
    headers: {
        "Authorization": 'Bearer ' + localStorage.getItem('token')
    }
    }).success(function(param, status, headers, config){
        deferred.resolve(param);
        $rootScope.loadSuccessfull = true;
    }).error(function(){
        deferred.reject('data can\'t be retrieved');

    });
    return deferred.promise;
};





ApiServices.downloadFile = function(){
    var deferred = $q.defer();
    $http({
    url : baseUrl+'/file_download_by_id?docId='+$localStorage.docId,
    method : 'GET',
    headers: {
        "Authorization": 'Bearer ' + localStorage.getItem('token')
    }
    }).success(function(param, status, headers, config){
        deferred.resolve(param);
        $rootScope.loadSuccessfull = true;
    }).error(function(){
        deferred.reject('data can\'t be retrieved');

    });
    return deferred.promise;
};
/*Dashboard Petition Service end*/
ApiServices.logout = function(callback){
  var cb = callback || angular.noop;
  var deferred = $q.defer();
  //console.log("test Login1", user);
  $http({
  url : baseUrl+'/logout',
  method : 'GET',
  headers: {
      "Authorization": 'Bearer ' + localStorage.getItem('token')
  }
  })
  .success(function(data) {
    //console.log("test log", data);
    $window.localStorage.clear();
    $location.path('/');
    deferred.resolve(data);
    $rootScope.loadSuccessfull = true;
    return cb(data);
  }).error(function(err) {
      $window.localStorage.clear();
      $location.path('/');
      deferred.reject(err);

      return cb(err);
  }.bind(this));

  return deferred.promise;
};

ApiServices.tableHd = function(){
  var deferred = $q.defer();
  $http({
    url : 'http://localhost:4001/app/dashboard/demo2.json',
    method : 'GET',
  }).success(function(data){
      deferred.resolve(data);
      $rootScope.loadSuccessfull = true;
  }).error(function(){
       deferred.reject('data can\'t be retrieved');

  });
  return deferred.promise;
};

ApiServices.formB = function(){
  var deferred = $q.defer();
  $http({
    url : 'http://localhost:4001/app/dashboard/demo2.json',
    method : 'GET',

  }).success(function(data){
      deferred.resolve(data);
      $rootScope.loadSuccessfull = true;
  }).error(function(){
       deferred.reject('data can\'t be retrieved');

  });
  return deferred.promise;
};


ApiServices.expanditure = function(ApiEndPoints,callback){
  var cb = callback || angular.noop;
  var deferred = $q.defer();
    return $http.get(localUrl+ApiEndPoints,  {

   })
  .success(function(data) {
    deferred.resolve(data);
    $rootScope.loadSuccessfull = true;
    return cb(data);
  }).error(function(err) {
      deferred.reject(err);

      return cb(err);
  }.bind(this));

  return deferred.promise;;
};

ApiServices.getFormComment = function(ApiEndPoints,callback){
  var cb = callback || angular.noop;
  var deferred = $q.defer();
  var url = '';
  return $http.get(baseUrl+ApiEndPoints,  {
    headers: {
          "Authorization": 'Bearer ' + localStorage.getItem('token')
     }
   })
  .success(function(data) {
    deferred.resolve(data);
    $rootScope.loadSuccessfull = true;
    return cb(data);
  }).error(function(err) {
      deferred.reject(err);

      return cb(err);
  }.bind(this));
  return deferred.promise;;
};
/*call api to fetch form data*/

ApiServices.fetchFormData = function(ApiEndPoints,callback){
  var cb = callback || angular.noop;
  var deferred = $q.defer();
  var url = '';
  return $http.get(baseUrl+ApiEndPoints,  {
    headers: {
          "Authorization": 'Bearer ' + localStorage.getItem('token')
     }
   })
  .success(function(data) {
    deferred.resolve(data);
    $rootScope.statusZero = false;
    if(data.status==0){
      $rootScope.statusZero = true;
      $rootScope.alertMessage = data.message;
    }else if(data.status==1){
        $rootScope.statusZero = false;
    }
    $rootScope.loadSuccessfull = true;
    console.log('success: ',data);
    return cb(data);
  }).error(function(err) {
      deferred.reject(err);

      return cb(err);
  }.bind(this));
  return deferred.promise;
};


ApiServices.attachmentTypeForm = function(ApiEndPoints,callback){
  var cb = callback || angular.noop;
  var deferred = $q.defer();
  var url = '';
  return $http.get(baseUrl+ApiEndPoints,  {
    headers: {
          "Authorization": 'Bearer ' + localStorage.getItem('token')
     }
   })
  .success(function(data) {
    deferred.resolve(data);
    $rootScope.loadSuccessfull = true;
    return cb(data);
  }).error(function(err) {
      deferred.reject(err);

      return cb(err);
  }.bind(this));
  return deferred.promise;
};

ApiServices.attachFileList = function(ApiEndPoints,callback){
  var cb = callback || angular.noop;
  var deferred = $q.defer();
  var url = '';
  return $http.get(baseUrl+ApiEndPoints,  {
    headers: {
          "Authorization": 'Bearer ' + localStorage.getItem('token')
     }
   })
  .success(function(data) {
    deferred.resolve(data);
    $rootScope.loadSuccessfull = true;
    return cb(data);
  }).error(function(err) {
      deferred.reject(err);

      return cb(err);
  }.bind(this));
  return deferred.promise;
};




ApiServices.GetGlobalFiles = function(ApiEndPoints,callback){
    var cb = callback || angular.noop;
    var deferred = $q.defer();
    var url = '';
    return $http.get(baseUrl+ApiEndPoints,  {
      headers: {
            "Authorization": 'Bearer ' + localStorage.getItem('token')
       }
     })
       .success(function(data) {
         deferred.resolve(data);
         $rootScope.loadSuccessfull = true;
         return cb(data.data.fileList);
       }).error(function(err) {
           deferred.reject(err);

           return cb(err);
       }.bind(this));

       return deferred.promise;;
};

ApiServices.GetAllAttachFiles = function(ApiEndPoints,callback){
    var cb = callback || angular.noop;
    var deferred = $q.defer();
    var url = '';
    return $http.get(baseUrl+ApiEndPoints,  {
      headers: {
            "Authorization": 'Bearer ' + localStorage.getItem('token')
       }
     })
       .success(function(data) {
         deferred.resolve(data);
         $rootScope.loadSuccessfull = true;
         return cb(data.data.fileList);
       }).error(function(err) {
           deferred.reject(err);
           return cb(err);
       }.bind(this));

       return deferred.promise;;
};

//For Reset Password
ApiServices.resetPassword = function(resetObj, callback){
  var cb = callback || angular.noop;
  var deferred = $q.defer();
  var payload = new FormData();
  payload.append("login_id", resetObj.loginID);
  payload.append("reset_password", resetObj.currentPwd);
  payload.append("new_password", resetObj.newPwd);
  payload.append("confirm_password", resetObj.confirmPwd);

  var url =baseUrl+ '/change_password';
  return $http.post(url, payload,{
    headers: {'Content-Type': undefined}
  })
  .success(function(data) {
    deferred.resolve(data);
    $rootScope.loadSuccessfull = true;
    return cb(data);
  }).error(function(err) {
      deferred.reject(err);

      return cb(err);
  }.bind(this));

  return deferred.promise;
};

// To send url in mail
ApiServices.sendPasswordInMail = function(userId, callback){
  var cb = callback || angular.noop;
  var deferred = $q.defer();
  var payload = new FormData();
  payload.append("login_id", userId);
  var url =baseUrl+ '/send_fpwd_mail';
  return $http.post(url, payload,{
    headers: {'Content-Type': undefined}
  })
  .success(function(data) {
    deferred.resolve(data);
    $rootScope.loadSuccessfull = true;
    return cb(data);
  }).error(function(err) {
      deferred.reject(err);

      return cb(err);
  }.bind(this));

  return deferred.promise;
};












      return ApiServices;
    });
})();
