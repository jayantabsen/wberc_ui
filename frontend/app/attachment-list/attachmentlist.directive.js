(function() {
  'use strict';
  var app = angular.module('mean');
  app.directive('attachmentList', [function(){

    return{
    	restrict : 'AEC',
    	//replace : true,
    	transclude :  true,
    	controller : 'AttachmentlistController',
    	templateUrl : 'app/attachment-list/attachmentlist.html'
    };

  }]);
})();
