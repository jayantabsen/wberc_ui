(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('AttachmentlistController', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','$rootScope', '$stateParams','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,$rootScope, $stateParams,$state,$localStorage,$window){

      $scope.signedIn = false;
        $rootScope.validateTyperef = false;
        $rootScope.validateDescription = false;
        $rootScope.validateFile = false;
        $scope.test = 'testing';
        $scope.hideDocType=true;

      var tokenValue =  localStorage.getItem('token');
    if($localStorage.petitionId==undefined || $localStorage.petitionId == ""){
        $rootScope.message = 'You Cannot access the link directly. Please create a new petition first.';
         $location.path('/dashboard');
    }
    var Phid = $localStorage.petitionId;
    var utilid = $localStorage.utilId;
    $scope.petitionId = $localStorage.petitionId;
    $scope.user = ApiServices.getAuthUser();
    var valUser = JSON.parse(localStorage.getItem("user"));

      $rootScope.loadAllAttachFiles = function(formId,stationId){
        $rootScope.loadSuccessfull = false;
        var ApiEndPoints = '/file_listing_generic?ph_id='+$localStorage.petitionId;
        $scope.allAttachfiles = [];
             ApiServices.fetchFormData(ApiEndPoints,function(data){
               $scope.allAttachfiles = data.data.fileList;
               $scope.attachmentLength =$scope.allAttachfiles.length;
               if( $scope.allAttachfiles.length==0){
                 //$rootScope.loadSuccessfull = true;
                 $scope.showHideAttachment = true;
               }else if( $scope.allAttachfiles.length>0){
                 $scope.showHideAttachment = false;
               }
                 $rootScope.loadSuccessfull = true;
               var formList = $localStorage.sidebarMenuList;
               var dataX = data.data.fileList;
               for(var i=0; i<dataX.length; i++){
                 for(var j=0; j<formList.length; j++){
                   if(dataX[i].formId==formList[j].formid){
                     dataX[i].form_code=formList[j].form_code;
                   }
                 }
               }
          });
      };
      $rootScope.loadAllAttachFiles();


       $scope.add_attachment = function(){
      	  var modalInstance = $uibModal.open({
                templateUrl: 'app/dashboard/views/attachment/attachment_list.html',
                size:"lg",
                scope: $scope,
                controller: ModalInstanceCtrl,
                resolve: {
                  attachment: function () {
                    return $scope.attachment;
                  }
                }
              });
       };
       var ModalInstanceCtrl = function ($scope, $uibModalInstance, attachment) {
          $scope.attachment = {};
          $scope.closeModal = function(){
            $uibModalInstance.dismiss('cancel');
          }

          $scope.addDescription =function (){
            $scope.validateDescription = false;
          }
          //$scope.validate = false;
          $scope.submit_attachment = function(){
            $rootScope.loadSuccessfull = false;
            if($scope.attachment.file != undefined  && $scope.attachment.comment != undefined && $scope.attachment.typeRef != undefined){
              $rootScope.validateFile = false;
              $rootScope.validateDescription = false;
              $rootScope.validateTyperef = false;
              var url = "http://182.75.177.246:8181/wberc_v2/api/file_upload_v2";
              var formdata = new FormData();
              formdata.append('file', $scope.attachment.file);
              formdata.append('docName', $scope.attachment.file.name);
              formdata.append('description', $scope.attachment.comment);
              formdata.append('petitionHeaderId', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
              formdata.append('AttachmentType', $scope.attachment.attachment_type_desc)
              formdata.append('formID', $scope.attachment.typeRef);
              formdata.append('utilid', parseInt(localStorage.getItem('ngStorage-utilId').replace(/['"]+/g, '')));
              formdata.append('module', 'ARR');
              formdata.append('user_id', parseInt(localStorage.getItem("ngStorage-userId").replace(/['"]+/g, '')));
              ApiServices.PostGlobalFileUpload(url, formdata,function(data){
                  $rootScope.loadSuccessfull = true;
                  //$scope.showHideAttachment = true;
                $scope.loadGlobalFiles();
                $rootScope.loadAllAttachFiles();
                $uibModalInstance.dismiss('cancel');
              });
            }
            if($scope.attachment.file == undefined || $scope.attachment.file == ''){
                $rootScope.loadSuccessfull = true;
                $rootScope.validateFile = true;
            }
            if($scope.attachment.comment == undefined || $scope.attachment.comment == ''){
                $rootScope.loadSuccessfull = true;
                $rootScope.validateDescription = true;
            }
            if($scope.attachment.typeRef == undefined || $scope.attachment.typeRef == ''){
                $rootScope.loadSuccessfull = true;
                $rootScope.validateTyperef = true;
            }
          };
          $scope.cancel = function () {
              $uibModalInstance.dismiss('cancel');
          };
      };

      // $scope.submit_attachment_Form = function(){
      //   if($scope.attachment.file != undefined){
      //     var url = "http://182.75.177.246:8181/wberc_v2/api/file_upload_v2";
      //     var formdata = new FormData();
      //     formdata.append('file', $scope.attachment.file);
      //     formdata.append('docName', $scope.attachment.file.name);
      //     formdata.append('description', $scope.attachment.comment);
      //     formdata.append('AttachmentType', $scope.attachment.attachment_type_desc);
      //     formdata.append('petitionHeaderId', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
      //     formdata.append('formID', $scope.attachment.typeRef);
      //     formdata.append('utilid', parseInt(localStorage.getItem('ngStorage-utilId').replace(/['"]+/g, '')));
      //     formdata.append('module', 'ARR');
      //     formdata.append('user_id', parseInt(localStorage.getItem("ngStorage-userId").replace(/['"]+/g, '')));
      //     ApiServices.PostGlobalFileUpload(url, formdata,function(data){
      //       alert(data);
      //       $scope.loadGlobalFiles();
      //       $rootScope.loadAllAttachFiles();
      //       $uibModalInstance.dismiss('cancel');
      //     });
      //   }else {
      //     alert("Please select file to attach..");
      //   }
      // };

       $scope.Assign_RejectPetition = function(status){
      	 var url = "http://182.75.177.246:8080/engine-rest/task/"+taskId+"/complete";
      	 if(status=='complete'){
      		var post = {"variables":
      						{"petitionversion":{"type":"String","value":"1","valueInfo":{}},
      						"petitionyear":{"type":"String","value":"2017-18","valueInfo":{}},
      						"petitionid":{"type":"String","value":Phid,"valueInfo":{}},
      						"isverified":{"type":"boolean","value":true,"valueInfo":{}}}
      						}
      	}else{
      			var post = {"variables":
      						{"petitionversion":{"type":"String","value":"1","valueInfo":{}},
      						"petitionyear":{"type":"String","value":"2017-18","valueInfo":{}},
      						"petitionid":{"type":"String","value":Phid,"valueInfo":{}},
      						"isverified":{"type":"boolean","value":false,"valueInfo":{}}}
      						}
      		}
      	ApiServices.PostRequest(url,post,function(data){
      			$rootScope.message = 'Your Petition has been sent Successfully.';
      			localStorage.removeItem('ngStorage-petitionId');
      			localStorage.removeItem('ngStorage-utilId');
      			localStorage.removeItem('ngStorage-taskId');
      			$state.go('dashboard',null);
      	});

      };


      $scope.showTypeRef = function (data) {
        $localStorage.showTypeRef = ''
        $localStorage.showTypeRef = data;
          $scope.validateTyperef = false;
        var showTypeRef = data;
        if(showTypeRef!='' || showTypeRef!=undefined){
          //attachmentTypeApi(showTypeRef);
          attachmentTypeApiSingle(showTypeRef);
            $scope.validate = false;
        }else{
          //alert('Select form reference');
            $scope.validate = true;
        }

      };


var attachmentTypeApiSingle = function(typeRef){
	var ApiEndPoints = '/attachment_type_listing?form_id='+typeRef;
  ApiServices.attachmentTypeForm(ApiEndPoints,function(data){
      $scope.attachmentType = data.data.vals;
      $rootScope.attachmentTypeId = data;
      $localStorage.attachmentTypeList = $scope.attachmentType;
      if($scope.attachmentType!=''){
        $scope.hideDocType=false;
      }else{
        $scope.hideDocType=true;
      }

 });
};

$scope.loadGlobalFiles = function(){
   $scope.globalfiles = [];
     $rootScope.loadSuccessfull = false;
   var ApiEndPoints = '/file_listing?ph_id='+$localStorage.petitionId+'&form_id='+$localStorage.formId;
   ApiServices.GetGlobalFiles(ApiEndPoints,function(data){
     $scope.globalfiles = data;

    });
};
$scope.docIdFn = function(docId){
    var url = "http://182.75.177.246:8181/wberc_v2/api/file_delete_by_id?docId="+docId;
      $rootScope.loadSuccessfull = false;
    ApiServices.getFileDownload(url,function(data){
      if(data.status==1){
        $rootScope.loadSuccessfull = true;
        $scope.loadGlobalFiles();
        $rootScope.loadAllAttachFiles();
      }
    });
};
// $scope.submit_attachment_Form = function(){
//   $rootScope.loadSuccessfull = false;
//   if($scope.attachment.file != undefined){
//     var url = "http://182.75.177.246:8181/wberc_v2/api/file_upload_v2";
//     var formdata = new FormData();
//     formdata.append('file', $scope.attachment.file);
//     formdata.append('docName', $scope.attachment.file.name);
//     formdata.append('description', $scope.attachment.comment);
//     formdata.append('AttachmentType', $scope.attachment.attachment_type_desc)
//     formdata.append('petitionHeaderId', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
//     formdata.append('formID', $localStorage.formId);
//     formdata.append('utilid', parseInt(localStorage.getItem('ngStorage-utilId').replace(/['"]+/g, '')));
//     formdata.append('module', 'ARR');
//     formdata.append('user_id', parseInt(localStorage.getItem("ngStorage-userId").replace(/['"]+/g, '')));
//     ApiServices.PostGlobalFileUpload(url, formdata,function(data){
//         $rootScope.loadSuccessfull = true;
//       $scope.showHideAttachment = false;
//       $scope.loadGlobalFiles();
//       $rootScope.loadAllAttachFiles();
//       $uibModalInstance.dismiss('cancel');
//     });
//   }else {
//     alert("Please select file to attach..");
//   }
//   $scope.attachment = {
//     'comment':'',
//     'form_ref':'',
//     'file':'',
//   };
// };


  $scope.loadGlobalFiles();


$scope.init();



  }]);
})();
