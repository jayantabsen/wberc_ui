(function() {
  'use strict';
  var app = angular.module('mean');
  app.directive('commentTab', [function(){

    return{
    	restrict : 'AEC',
    	replace : true,
    	transclude :  true,
    	controller : 'CommentController',
    	templateUrl : 'app/comment-tab/comment-tab.html'
    	//template : '<p>header</p>'
    };




    //$scope.init();
  }]);
})();
