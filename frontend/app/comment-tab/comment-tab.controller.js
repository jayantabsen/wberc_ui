(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('CommentController', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','$rootScope', '$stateParams','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,$rootScope, $stateParams,$state,$localStorage,$window){

      $scope.setTab = function(newTab){
        $scope.tab = newTab;
      };
      $scope.isSet = function(tabNum){
        return $scope.tab === tabNum;
      };
      // $scope.attachment.file = '';
      // $scope.attachment.comment = '';
      // if($localStorage.formId==1 || $localStorage.formId==2 || $localStorage.formId==3 || $localStorage.formId==4 ){
      //      $scope.attachment.attachment_type_desc = '';
      // }
        $rootScope.validateTyperef = false;
        $rootScope.validateDescription = false;
        //$rootScope.validateFile = false;
        $rootScope.validateComment = false;
        $scope.doctypeIndividual = false;

      $scope.getFormComment = function(){
    		var ApiEndPoints = '/get_form_comment?form_id='+$localStorage.formId+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.getFormComment(ApiEndPoints,function(data){
            $scope.commentList = data.data.comments;
            if( $scope.commentList.length==0){
              $scope.showHideComment = true;
            }
            if($scope.commentList[0].comments==' '){
              $scope.showHideComment = true;
            }
       });
      };
      $scope.getFormComment();

      $scope.daleteComment = function(commentId){
          var url = "http://182.75.177.246:8181/wberc_v2/api/delete_1_comment?arr_comment_id="+commentId;
          ApiServices.getFileDownload(url,function(data){
            if(data.status==1){
              $scope.getFormComment();
            }
          });
      };
      $scope.submit_comment_Form = function(){
        if($scope.comments=='' || $scope.comments == undefined){
          $rootScope.validateComment = true;
        }else if($scope.comments!='' || $scope.comments!= undefined){
          var url = "http://182.75.177.246:8181/wberc_v2/api/insert_1_comment";
          var formdata = new FormData();
          formdata.append('comment', $scope.comments.comment);
          formdata.append('form_id',$localStorage.formId);
          formdata.append('ph_id', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
          formdata.append('user_id', parseInt(localStorage.getItem("ngStorage-userId").replace(/['"]+/g, '')));
          ApiServices.PostRequest(url, formdata,function(data){
            //alert(data.message);
            $scope.comments.comment=" ";
            $scope.showHideComment = false;
            $scope.getFormComment();
              $rootScope.validateComment = false;
          });
        }
      };

  var attachmentTypeApiSingle = function(){
		var ApiEndPoints = '/attachment_type_listing?form_id='+$localStorage.formId;
    ApiServices.attachmentTypeForm(ApiEndPoints,function(data){
        $scope.attachmentType = data.data.vals;
        $localStorage.attachmentTypeId = data.data.attachment_type_desc;

   });
  };

  //  $scope.attachment ={"comment":"","attachment_type_desc":"Not specifid","file":""}
  if($localStorage.formId==1 || $localStorage.formId==2 || $localStorage.formId==3 || $localStorage.formId==4 ){
    attachmentTypeApiSingle();
    $scope.doctypeIndividual = true;
    //var attachmentType = $scope.attachment.attachment_type_desc;
  }
  //attachmentTypeApiSingle();

  // $scope.loadGlobalFiles = function(){
  //    $scope.globalfiles = [];
  //    var ApiEndPoints = '/file_listing?ph_id='+$localStorage.petitionId+'&form_id='+$localStorage.formId;
  //    ApiServices.GetGlobalFiles(ApiEndPoints,function(data){
  //      $scope.globalfiles = data;
  //      console.log(' $scope.globalfiles.length: ', $scope.globalfiles.length);
  //        $scope.globalfiles = data;
  //        if( $scope.globalfiles.length==0){
  //          $scope.showHideAttachment = false;
  //        }
  //     });
  // };
  // $scope.docIdFn = function(docId){
  //     var url = "http://182.75.177.246:8181/wberc_v2/api/file_delete_by_id?docId="+docId;
  //     ApiServices.getFileDownload(url,function(data){
  //       if(data.status==1){
  //         $scope.loadGlobalFiles();
  //         $rootScope.loadAllAttachFiles();
  //       }
  //     });
  // };

 $scope.Assign_RejectPetition = function(status){
	 var url = "http://182.75.177.246:8080/engine-rest/task/"+taskId+"/complete";
	 if(status=='complete'){
		var post = {"variables":
						{"petitionversion":{"type":"String","value":"1","valueInfo":{}},
						"petitionyear":{"type":"String","value":"2017-18","valueInfo":{}},
						"petitionid":{"type":"String","value":Phid,"valueInfo":{}},
						"isverified":{"type":"boolean","value":true,"valueInfo":{}}}
						}
	}else{
			var post = {"variables":
						{"petitionversion":{"type":"String","value":"1","valueInfo":{}},
						"petitionyear":{"type":"String","value":"2017-18","valueInfo":{}},
						"petitionid":{"type":"String","value":Phid,"valueInfo":{}},
						"isverified":{"type":"boolean","value":false,"valueInfo":{}}}
						}
		}
	ApiServices.PostRequest(url,post,function(data){
			$rootScope.message = 'Your Petition has been sent Successfully.';
			localStorage.removeItem('ngStorage-petitionId');
			localStorage.removeItem('ngStorage-utilId');
			localStorage.removeItem('ngStorage-taskId');
			$state.go('dashboard',null);
	});

};

var attachmentTypeApi = function(showTypeRef){
 ApiServices.attachmentType().then(
   function(data){
       $scope.attachmentType = data.data.vals;
       $localStorage.attachmentTypeId = data.data.attachment_type_desc;
   },
   function(){
     console.log('data can not be retrieved');
   }
 )
};
$scope.showTypeRef = function (data) {
  $localStorage.showTypeRef = ''
  $localStorage.showTypeRef = data;
    $scope.validateTyperef = false;
  var showTypeRef = data;
  //console.log('showTypeRef: ',showTypeRef);
  if(showTypeRef!='' || showTypeRef!=undefined){
  console.log('showTypeRef: ',showTypeRef);
    attachmentTypeApi(showTypeRef);
    attachmentTypeApiSingle(showTypeRef);
      $scope.validate = false;
  }else{
    //alert('Select form reference');
      $scope.validate = true;
  }

};
        // $scope.addAttachment = function(){
        //   $scope.validate = false;
        // }
var attachmentTypeApiSingle = function(showTypeRef){
 ApiServices.attachmentTypeForm().then(
   function(data){
       $scope.attachmentType = data.data.vals;
       $localStorage.attachmentTypeId = data.data.attachment_type_desc;
       console.log('$rootScope.attachmentTypeId',$rootScope.attachmentTypeId);
   },
   function(){
   }
 )
};

  var attachmentTypeApiSingle = function(typeRef){
  	var ApiEndPoints = '/attachment_type_listing?form_id='+typeRef;
    ApiServices.attachmentTypeForm(ApiEndPoints,function(data){
        $scope.attachmentType = data.data.vals;
        $rootScope.attachmentTypeId = data;
        $localStorage.attachmentTypeList = $scope.attachmentType;
        console.log('$localStorage.attachmentType popup: ',$localStorage.attachmentTypeList);

   });
  };

  $scope.loadGlobalFiles = function(){
     $scope.globalfiles = [];
       $rootScope.loadSuccessfull = false;
     var ApiEndPoints = '/file_listing?ph_id='+$localStorage.petitionId+'&form_id='+$localStorage.formId;
     ApiServices.GetGlobalFiles(ApiEndPoints,function(data){
       $scope.globalfiles = data;

         if( $scope.globalfiles==0){
           $rootScope.loadSuccessfull = true;
           $scope.showAttachment = false;
           $scope.hideAttachment = true;
         }else if( $scope.globalfiles!=0){
         $scope.showAttachment = true;
         $scope.hideAttachment = false;

         }
      });
  };
  $scope.docIdFn = function(docId){
      var url = "http://182.75.177.246:8181/wberc_v2/api/file_delete_by_id?docId="+docId;
        $rootScope.loadSuccessfull = false;
      ApiServices.getFileDownload(url,function(data){
        if(data.status==1){
          $rootScope.loadSuccessfull = true;
          $scope.loadGlobalFiles();
          $rootScope.loadAllAttachFiles();
        }
      });
  };
$scope.validFile = function(){
  //alert('$scope.attachment.file: ',$scope.attachment.comment);
      // if( $scope.attachment.file == ''){
      //     $rootScope.validateFile = true;
      //       alert('$scope.attachment.file: ',$rootScope.validateFile);
      //   }else if ($scope.attachment.file != '') {
      //     $rootScope.validateFile = false;
      //   }
}
$scope.attachment = {
     file: "",
     comment: ""
   };
var attachmentCopy = angular.copy($scope.attachment);
  $scope.submit_attachment_Form = function(){
    //alert('$scope.attachment.file: ',$scope.attachment.file);
    if( $scope.attachment.file == undefined || $scope.attachment.file == ''){
        $rootScope.validateFile = true;
      }else if ( $scope.attachment.file.name != undefined || $scope.attachment.file.name != '') {
        $rootScope.validateFile = false;
      var url = "http://182.75.177.246:8181/wberc_v2/api/file_upload_v2";
      var formdata = new FormData();
      formdata.append('file', $scope.attachment.file);
      formdata.append('docName', $scope.attachment.file.name);
      formdata.append('description', $scope.attachment.comment);
      //formdata.append('AttachmentType', $scope.attachment.attachment_type_desc)attachmentType
    //  formdata.append('AttachmentType', $scope.attachment.attachment_type_desc)
      if($localStorage.formId==1 || $localStorage.formId==2 || $localStorage.formId==3 || $localStorage.formId==4 ){
        //$scope.attachment.attachment_type_desc = 'Not specified'
          formdata.append('AttachmentType', $scope.attachment.attachment_type_desc)
      }else{
        formdata.append('AttachmentType', 'Not specified')
      }
      formdata.append('petitionHeaderId', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
      formdata.append('formID', $localStorage.formId);
      formdata.append('utilid', parseInt(localStorage.getItem('ngStorage-utilId').replace(/['"]+/g, '')));
      formdata.append('module', 'ARR');
      formdata.append('user_id', parseInt(localStorage.getItem("ngStorage-userId").replace(/['"]+/g, '')));
      ApiServices.PostGlobalFileUpload(url, formdata,function(data){
          $rootScope.loadSuccessfull = true;
        //$scope.showHideAttachment = false;
        if(data.status==0){
          $scope.showMsg = true;
          $scope.attachmentMsg = data.message;
        }if(data.status==1){
          $scope.showMsg = false;
          $scope.attachmentMsg = data.message;
          $scope.attachment = angular.copy(attachmentCopy);
          $scope.uploadform.$setPristine();
          $scope.loadGlobalFiles();
          $rootScope.loadAllAttachFiles();

        }
        //$uibModalInstance.dismiss('cancel');
      });
    }

    // else if( $scope.attachment.typeRef != undefined || $scope.attachment.typeRef == ''){
    //     //$rootScope.loadSuccessfull = true;
    //     $rootScope.validateTyperef = true;
    // }
    // $scope.attachment = {
    //   'comment':'',
    //   'form_ref':'',
    //   'file':'',
    // };
  };

    $scope.loadGlobalFiles();



  }]);
})();
