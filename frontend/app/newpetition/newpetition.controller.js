(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('NewpetitionController', ['$rootScope','$scope','$location','ApiServices','NewpetitionServices','$filter','$state','DashboardServices','$window',function($rootScope,$scope,$location,ApiServices,NewpetitionServices,$filter,$state,DashboardServices,$window){


  $scope.init = function(){
	  $scope.signedIn = ApiServices.loggedIn();
    var tokenValue =  localStorage.getItem('token');
    console.log('token: ',localStorage.getItem('token'));
    if(tokenValue==undefined || tokenValue==null){
      $state.go('login');
    }else if(tokenValue!=undefined || tokenValue!=null){


        $scope.signedIn = false;
        $scope.user = {};
        $scope.prevYearModel = 0;
        $scope.LengthYearModel = 0;
        var respComboApi = function(){
         ApiServices.reqPetition().then(
           function(data){
               $scope.comboYear = data.data.base_year;
           },
           function(){
             console.log('data can not be retrieved');
           }
         )
       };
        respComboApi();
        $scope.user = ApiServices.getAuthUser();
        $scope.utilId =  $scope.user.utilid;
        $scope.userId =  $scope.user.userid;
        /*menu start*/
            var valUser=JSON.parse(localStorage.getItem("user"));
            $scope.allMenu = valUser.menu_status;
            var showState;
            for(var i =0; i<$scope.allMenu.length; i++){
              showState = $scope.allMenu[i].state;
            }
            if(showState=='1'){
              $scope.showMenu == true;
            }else if(showState=='0'){
              $scope.showMenu == !$scope.showMenu;
            }



           $scope.proceed = function(petitionBaseYear,petitionPrev,petitionLength){
             		ApiServices.setBaseYearFn($scope.baseYearModel,$scope.prevYearModel,$scope.LengthYearModel).then(
                  function(data){
                      $scope.petitionMsg = data.data.msg;
                  },
                  function(){
                    console.log('data can not be retrieved');
                  }
                );

               }
            $scope.confirmPetition = function(){
              $rootScope.loadSuccessfull = false;
              ApiServices.confirmYearFn($scope.baseYearModel,$scope.prevYearModel,$scope.LengthYearModel,$scope.utilId,$scope.userId).then(
                function(data){
                      $state.go('file-petition',{petitionId: data.data.phid ,utilId: data.data.utilid});

                      $rootScope.loadSuccessfull = true;
                        console.log('$rootScope.loadSuccessfull: ',$rootScope.loadSuccessfull);

                },
                function(){
                  console.log('data can not be retrieved');
                }
              );
            };
            $scope.cancelFn = function(){
              $scope.prevYearModel = 0;
              $scope.LengthYearModel = 0;
              if(angular.isDefined($scope.baseYearModel)){
                  delete $scope.baseYearModel;
              }
            }
  		  $scope.user = ApiServices.getAuthUser();
      }
  };
  $scope.add_attachment = function(){
     var modalInstance = $uibModal.open({
           templateUrl: 'app/newpetition/views/petitionPopup.html',
           size:"lg",
           scope: $scope,
           controller: PetitionpopupCtrl,
           resolve: {
             attachment: function () {
               return $scope.attachment;
             }
           }
         });
  };

  var PetitionpopupCtrl = function ($scope, $uibModalInstance) {
     $scope.attachment = {};
     $scope.closeModal = function(){
       $uibModalInstance.dismiss('cancel');
     }
     $scope.confirmPetition = function(){
       $rootScope.loadSuccessfull = false;
       ApiServices.confirmYearFn($scope.baseYearModel,$scope.prevYearModel,$scope.LengthYearModel,$scope.utilId,$scope.userId).then(
         function(data){
               $state.go('file-petition',{petitionId: data.data.phid ,utilId: data.data.utilid});

               $rootScope.loadSuccessfull = true;
                 console.log('$rootScope.loadSuccessfull: ',$rootScope.loadSuccessfull);

         },
         function(){
           console.log('data can not be retrieved');
         }
       );
     };
     $scope.cancel = function () {
         $uibModalInstance.dismiss('cancel');
     };
 };

/******Logout*******/
    $scope.logout = function(){
      console.log('logout');
      ApiServices.logout(function(_data){
        if(_data.status == '1'){
          console.log('logout',_data.message);
          $window.localStorage.clear();
          $location.path('/login')
        }else{
          $scope.error.msg = _data.message;
        }
      });
    }
/******Logout*******/



    $scope.init();
  }]);
})();
