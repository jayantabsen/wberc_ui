(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('DetailsController', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','$rootScope', '$stateParams','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,$rootScope, $stateParams,$state,$localStorage,$window){


$scope.init = function(){
  $scope.signedIn = ApiServices.loggedIn();
  var tokenValue =  localStorage.getItem('token');
  console.log('token: ',localStorage.getItem('token'));
  if(tokenValue==undefined || tokenValue==null){
    $state.go('login');
  }else if(tokenValue!=undefined || tokenValue!=null){
      $scope.signedIn = false;
      $scope.test ='details';
      $scope.loadAlEnsuingYear = function(){
      	$scope.Ensuringyears = [];
      		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
      		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
      				$scope.Ensuringyears = data.data.ensuing_petition_year;
              $scope.prevYear=$scope.Ensuringyears[0].year_nm;
              $scope.baseYear=$scope.Ensuringyears[1].year_nm;
              $scope.nextYear=$scope.Ensuringyears[2].year_nm;

      		  });
      };
      $scope.loadAlEnsuingYear();
    }
  }






$scope.init();



  }]);
})();
