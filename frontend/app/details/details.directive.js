(function() {
  'use strict';
  var app = angular.module('mean');
  app.directive('mainDetails', [function(){

    return{
    	restrict : 'AEC',
    	//replace : true,
    	transclude :  true,
    	controller : 'DetailsController',
    	templateUrl : 'app/details/details.html'
    };
    //$scope.init();
  }]);
})();
