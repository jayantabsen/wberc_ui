(function() {
  'use strict';

var app = angular.module('mean');

  app.controller('FrontController', ['$scope','$location','$filter','$timeout','ApiServices','DashboardServices','$rootScope', '$state','$localStorage','$window',function($scope,$location,$filter,$timeout,ApiServices,DashboardServices,$rootScope, $state,$localStorage,$window){

    // var tokenValue =  localStorage.getItem('token');
    //
    // $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    //   if(tokenValue==undefined || tokenValue==null){
    //         event.preventDefault();
    //         return $state.go('login');
    //     }else{}
    //
    //     return;
    // });
	$scope.init = function(){
	  $scope.signedIn = ApiServices.loggedIn();
    var tokenValue =  localStorage.getItem('token');
    console.log('token: ',localStorage.getItem('token'));
    if(tokenValue==undefined || tokenValue==null){
      $state.go('login');
    }else if(tokenValue!=undefined || tokenValue!=null){


        $scope.signedIn = false;
      $scope.currentPage = 1;
      $scope.pageSize = 5;
      $scope.dashBoardData = [];

      console.log('token: ',localStorage.getItem('token'));
      if(tokenValue==undefined || tokenValue==null){
        $state.go('login');
      }else if(tokenValue!=undefined || tokenValue!=null){
	       $scope.signedIn = ApiServices.loggedIn();

		  var localStore = JSON.parse(localStorage.getItem('user'));
		  $scope.loginId = localStore.loginid;
		  $scope.roleCode = localStore.role_code;
		  $scope.utilHeading = localStore.utilName;
		  $scope.roleid = localStore.roleid;
		  $scope.utilId = localStore.utilid;
  		  if($rootScope.message){
  			     $scope.message = $rootScope.message;
  		  }else{
  			     $scope.message = '';
  			}

        	$scope.getAllTask = function(){
        		$scope.groupTasks = [];
        		$scope.mytasks = [];
        		if($scope.roleid!=1){
        				var url =  "http://182.75.177.246:8080/engine-rest/task?candidateGroups="+$scope.roleCode;
        				var TempGroupTask = [];
        				ApiServices.getRequest(url,function(data){
        					$scope.groupTasks = data;
        						var ii = 0;
        						angular.forEach(data, function(value, key) {
        							var url =  "http://182.75.177.246:8080/engine-rest/task/"+value.id+'/variables/petitionid';
        							ApiServices.getRequest(url,function(resp){
        								TempGroupTask[value.id] = resp.value;
        								ii++;
        								if(ii == data.length) {
        									var kk = 0;
        									for (var key in $scope.groupTasks){
        										$scope.groupTasks[kk].petitionId = TempGroupTask[$scope.groupTasks[kk]['id']];
        										kk++;
        									}
        								}
        							});
        					});
        				});

        				var url =  "http://182.75.177.246:8080/engine-rest/task?assignee="+$scope.loginId;
        				var TempGroupTask1 = [];
        				ApiServices.getRequest(url,function(data){
        					$scope.mytasks = data;
        						var xx = 0;
        						angular.forEach(data, function(value, key) {
        							var url =  "http://182.75.177.246:8080/engine-rest/task/"+value.id+'/variables/petitionid';
        							ApiServices.getRequest(url,function(resp){
        								TempGroupTask1[value.id] = resp.value;
        								xx++;
        								if(xx == data.length) {
        									var kk1 = 0;
        									for (var key in $scope.mytasks){
        										$scope.mytasks[kk1].petitionId = TempGroupTask1[$scope.mytasks[kk1]['id']];
        										kk1++;
        									}
        								}
        							});
        					});
        				});

        		}else{
        			// get Draft values...
        				var url = "http://182.75.177.246:8181/wberc_v2/api/all_phids";
        				ApiServices.getRequest(url,function(data){
        						$scope.myDraft = data.data.phidAl;
        				});
        		}
        	}
		  /**trigger the elements first time **/
		  $scope.getAllTask();
		  $scope.user = ApiServices.getAuthUser();
    //};
/*menu start*/
    var valUser=JSON.parse(localStorage.getItem("user"));
    $scope.allMenu = valUser.menu_status;
    var showState;
    for(var i =0; i<$scope.allMenu.length; i++){
      showState = $scope.allMenu[i].state;
    }
    if(showState=='1'){
      $scope.showMenu == true;
    }else if(showState=='0'){
      $scope.showMenu == !$scope.showMenu;
    }


	$scope.viewPetitions = function(petitionId,taskId){
		$state.go('file-petition', {petitionId: petitionId,taskId:taskId,utilId:$scope.utilId});
	};

	$scope.claimtask = function(id){
		var url = "http://182.75.177.246:8080/engine-rest/task/"+id+"/claim";
		var post = {
			"userId": $scope.loginId,
		}
		ApiServices.PostRequest(url,post,function(data){
			$scope.getAllTask();
		});

	};
	$scope.Unclaimtask = function(id){
		var url = "http://182.75.177.246:8080/engine-rest/task/"+id+"/unclaim";
		var post = "";
		ApiServices.PostRequest(url,post,function(data){
			$scope.getAllTask();
		});
	};
	$scope.viewSavedDrafts = function(petitionId){
    $localStorage.petitionId = petitionId;
    var ApiEndPoints = '/getAnnexureList?phid='+$localStorage.petitionId+'&utilid='+$scope.utilId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
        $localStorage.formId = data.data.formList[0].formid;
     });
		$state.go('file-petition', {petitionId: $localStorage.petitionId,utilId:$scope.utilId});
	};
  var dashboardPetitionApi = function(){
      var ApiEndPoints = '/dashboard_petition_details?user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
            $scope.petitionDetails =  data.data.petitions;
            $scope.pageNumber = Math.ceil($scope.petitionDetails.length/$scope.pageSize);
            // if($scope.petitionDetails.length % $scope.pageSize == 0){
            //   $scope.pageNumber = pageNumber ;
            // }else{
            //     $scope.pageNumber = pageNumber + 1;
            // }
            $scope.getDashBoardData($scope.currentPage);
       });
    }
  dashboardPetitionApi();
}


$scope.getDashBoardData = function(currentPage) {
  if(currentPage>$scope.pageNumber){
    $scope.validPageMsg = "Please insert valid page number";
    $scope.showHideMsg = true;
  }else{
    $scope.showHideMsg = false;
    $scope.dashBoardData = [];
    var allData = $scope.petitionDetails;
    var begin = ((currentPage-1) * $scope.pageSize);
    var end = (begin + $scope.pageSize);
    for (var i=0; i<$scope.pageSize; i++) {
      if($.isEmptyObject(allData[begin+i])){
      }else{
        var data =  allData[begin+i];
        $scope.dashBoardData.push(data);
      }
    }
  }
  //console.log("$scope.dashBoardData:",$scope.dashBoardData);
};
}
}
    $scope.init();



  }]);
})();
