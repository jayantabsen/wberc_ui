(function() {
  'use strict';
  var app = angular.module('mean');
  app.directive('mainHeader', [function(){

    return{
    	restrict : 'AEC',
    	replace : true,
    	transclude :  true,
    	controller : 'HeaderController',
    	templateUrl : 'app/header/header.html'
    	//template : '<p>header</p>'
    };




    //$scope.init();
  }]);
})();
