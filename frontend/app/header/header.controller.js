(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('HeaderController', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','$rootScope', '$stateParams','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,$rootScope, $stateParams,$state,$localStorage,$window){

      $scope.signedIn = false;

      var tokenValue =  localStorage.getItem('token');
    if($localStorage.petitionId==undefined || $localStorage.petitionId == ""){
        $rootScope.message = 'You Cannot access the link directly. Please create a new petition first.';
         $location.path('/dashboard');
    }
    var Phid = $localStorage.petitionId;
    var utilid = $localStorage.utilId;
    $scope.petitionId = $localStorage.petitionId;
    $scope.user = ApiServices.getAuthUser();
    var valUser = JSON.parse(localStorage.getItem("user"));


      $scope.init = function(){
        $scope.signedIn = ApiServices.loggedIn();
          if(!$scope.signedIn){
            $location.path('/login')
          }else{
          /*menu start*/
             $scope.utilid = valUser.utilid;
              $scope.allMenu = valUser.menu_status;
              var showState;
              for(var i =0; i<$scope.allMenu.length; i++){
                showState = $scope.allMenu[i].state;
              }
              if(showState=='1'){
                $scope.showMenu == true;
              }else if(showState=='0'){
                $scope.showMenu == !$scope.showMenu;
              }
          }

      };

    /******Logout*******/
    //$scope.test = "abc";
    $scope.logout = function(){
    ApiServices.logout(function(_data){
      if(_data.status == '1'){
        $window.localStorage.clear();
        $location.path('/login')
      }else{
        //$scope.error.msg = _data.message;
      }
    });
    }
    /******Logout*******/

// change password
// $scope.hideForgotBox = false;
// $scope.forgotError = false;
// $scope.forgotSuccess = false;
$scope.forgotUserId="";
// $scope.forgotErrorMsg="";
// $scope.forgotSuccessMsg="";
$scope.changePassword = function(){
  var loginId = JSON.parse(localStorage.getItem('user')).loginid;
  if(loginId != "" && loginId != undefined){
    ApiServices.sendPasswordInMail(loginId, function(_data){
      if(_data.status == '1'){
        ApiServices.logout(function(_data){
          if(_data.status == '1'){
            $window.localStorage.clear();

          }else{
            //$scope.error.msg = _data.message;
          }
        });
        $location.path('/login');
        $rootScope.forgotSuccessMsg = _data.message;
        $rootScope.hideForgotBox = true;
        $rootScope.forgotPwd = true;
        $rootScope.forgotSuccess = true;
        $rootScope.forgotError = false;
        }
        else{
          $rootScope.forgotErrorMsg = _data.message;
          $rootScope.forgotSuccess = false;
          $rootScope.forgotError = true;
          $rootScope.hideForgotBox = false;
          $timeout(function() {
            $rootScope.forgotError = false;
            $rootScope.forgotErrorMsg = "";
          }, 2000);
        }
    });
  }

}





$scope.init();



  }]);
})();
