(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab96Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab96';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };


        $scope.getDataByForm = function(uid,tab,data) {
        		var result = [];
            result=data['valAll'][uid]['valAll'][0];
            return result;
        }

        $scope.loadPlumpExcelData1 = function(formId,pyId,vId,conClassId,conTypeId){
          //182.75.177.246:8181/wberc_v2/api/f3_4JExcel?ph_id=409&form_id=96&user_id=6&py_id=3021&consumer_class_id=10&consumer_type_id=1&voltage_level_id=1
          var ApiEndPoints = '/f3_4JExcel?ph_id='+$localStorage.petitionId+'&form_id='+96+'&user_id='+$localStorage.userId+'&py_id='+pyId+'&consumer_class_id='+conClassId+'&consumer_type_id='+conTypeId+'&voltage_level_id='+vId;
           ApiServices.fetchFormData(ApiEndPoints,function(data){
                $localStorage.baseData = data;
                $scope.subStationsData = [];
                $scope.subStations = [];

                var l=0;
                var slabDesc = "";
                var tableData = data.data.valAll;
                console.log("data.data:",data.data)
                var dataArrayAll = [];
                for(var i=0;i<data.data.valAll.length;i++){
                  var arr1 = data.data.valAll;

                    if(arr1[i].valAll.length==1){
                      for(var j=0;j<arr1[i].valAll.length;j++){
                        var arr2 = arr1[i].valAll;

                        for(var k=0;k<arr2[j].length;k++){
                          var arr3 = arr2[j][k];
                          var dataArray = [(slabDesc==arr3.slab) ? "" : arr3.slab,arr3.item,arr3.unit,arr3.summer_val,arr3.monsoon_val,arr3.winter_val];
                          data.data.valAll[i].valAll[j][k] = dataArray;
                          slabDesc=arr3.slab;
                        }
                      }
                    } else {
                      dataArrayAll = [];
                      for(var j=0;j<arr1[i].valAll.length;j++){
                        var arr2 = arr1[i].valAll;

                        for(var k=0;k<arr2[j].length;k++){
                          l=i;
                          var arr3 = arr2[j][k];
                          var dataArray = [(slabDesc==arr3.slab) ? "" : arr3.slab,arr3.item,arr3.unit,arr3.summer_val,arr3.monsoon_val,arr3.winter_val];
                          dataArrayAll.push(dataArray);
                          slabDesc=arr3.slab;
                        }
                      }
                      data.data.valAll[l].valAll[0]=[];
                      data.data.valAll[l].valAll[0]=dataArrayAll;
                    }
                  }
                //console.log('modified data: ',data.data.valAll);
                $scope.subStations = data.data.valAll;
                $scope.subStationsData = data.data;
             });
        	}

  $scope.loadAlEnsuingYear = function(formId){
  	$scope.Ensuringyears = [];
  		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.Ensuringyears = data.data.ensuing_petition_year;
          $localStorage.firstPyid=$scope.Ensuringyears[0].py_id;
          //console.log($scope.Ensuringyears);
          $timeout(function() {
            $scope.loadPlumpExcelData1(96,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
          }, 1000);
  		  });
  };
  $scope.loadAlEnsuingYear(96);

    //$scope.loadPlumpExcelData1(31,2518);

    $scope.loadConsumerType = function(formId){
    	$scope.ConsumerTypes = [];
    		var ApiEndPoints = '/consumerTypesCombo';
    		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
    				$scope.ConsumerTypes = data.data.consumer_types;
            $localStorage.firstConsumerId=$scope.ConsumerTypes[0].consumer_type_id;
            console.log('ConsumerTypes',data.data);
          $scope.loadConsumerTypeSub1(96,$localStorage.firstConsumerId);
    		  });
    };
    $scope.loadConsumerType(96);

      $scope.loadConsumerTypeSub1 = function(formId,sub_id_1){
      	$scope.consumerClassList = [];
      		var ApiEndPoints = '/consumerClassNVoltageLevelCombo?consumer_type_id='+sub_id_1;
      		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
            $scope.consumer = data.data;
      				$scope.consumerClassList = data.data.consumerClassList.consumer_classes;
              $scope.voltageLevelList = data.data.voltageLevelList.voltage_levels;
              $localStorage.firstClass=$scope.consumerClassList[0].consumer_class_id;
      				$localStorage.firstvoltage = $scope.voltageLevelList[0].voltage_level_id;
              console.log($scope.consumerClassList);
      		  });
      };

$scope.getSingleStations1 = function(Id,pyId){
  if(pyId==null){
    return false;
  }
   $scope.is_show = 'yes';
    $scope.selectedTab = 'tab96';
    $scope.selectedItem = 96;
    $localStorage.firstPyid=pyId;
    $scope.loadPlumpExcelData1(96,pyId,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
    $timeout( function(){
      $scope.is_show = 'no';
    }, 500 );
  }
$scope.getSingleStations2 = function(Id,consumerId){
      if(consumerId==null){
      	return false;
      }
  $scope.is_show = 'yes';
	$scope.selectedTab = 'tab96';
	$scope.selectedItem = 96;
  $localStorage.firstConsumerId = consumerId;
  $scope.loadConsumerTypeSub1(96,consumerId);
	$timeout( function(){
    $scope.loadPlumpExcelData1(96,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,consumerId);
		$scope.is_show = 'no';
	}, 500 );
}
$scope.getSingleStations3 = function(Id,classId){
  if(classId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab96';
  	$scope.selectedItem = 96;
		$timeout( function(){
      $localStorage.firstClass = classId;
      $scope.loadPlumpExcelData1(96,$localStorage.firstPyid,$localStorage.firstvoltage,classId,$localStorage.firstConsumerId)
			$scope.is_show = 'no';
		}, 500 );
  }
$scope.getSingleStations4 = function(Id,vId){
  if(vId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab96';
  	$scope.selectedItem = 96;
		$timeout( function(){
      $localStorage.firstvoltage = vId;
      $scope.loadPlumpExcelData1(96,$localStorage.firstPyid,vId,$localStorage.firstClass,$localStorage.firstConsumerId);
			$scope.is_show = 'no';
		}, 500 );
  }



  $scope.saveStationData = function(Seloption,formId){
  	  var raw = $scope.subStationsData;
      //console.log("raw:",raw)
      var slabDesc = "";
      var dataArrayAll = [];
      for(var i=0;i<raw.valAll.length;i++){
        var arr1 = raw.valAll;
        if(arr1[i].valAll.length==1){
          for(var j=0;j<arr1[i].valAll.length;j++){
            var arr2 = arr1[i].valAll;
            for(var k=0;k<arr2[j].length;k++){
                var arr3 = arr2[j][k];

              if(arr3[0]!=""){
                slabDesc = arr3[0];
              }
                var dataArray = {slab : slabDesc, item : arr3[1], unit : arr3[2], summer_val : arr3[3], monsoon_val : arr3[4] , winter_val : arr3[5]};
                raw.valAll[i].valAll[j][k] = dataArray;
            }
          }
        } else {
            var j=1;
            var arr2 = arr1[i].valAll;
            slabDesc = "";
            for(var k=0;k<arr2[j].length;k++){
                var arr3 = arr2[j][k];
                if(arr3[0]==""){
                  //slabDesc = slabDesc;
                }else{
                  slabDesc = arr3[0];
                }
                var dataArray = {slab : slabDesc, item : arr3[1], unit : arr3[2], summer_val : arr3[3], monsoon_val : arr3[4] , winter_val : arr3[5]};
                dataArrayAll.push(dataArray);
                if(dataArrayAll.length==6){
                  raw.valAll[i].valAll[j] = [];
                  raw.valAll[i].valAll[j] = dataArrayAll;
                  dataArrayAll = [];
                  j++;
                }
              }
              raw.valAll[i].values.shift();
         }
      }
     //console.log("$scope.subStationsData:",raw);
      var url = 'f3_4DataUpdate';
      ApiServices.setSubstaionData1({ message:'', data:raw, status:1, url:url,}, function(data){
           $scope.updateMessage=data.message;
           $timeout(function() {
               $scope.updateMessage='';
          }, 3000);
            //$scope.loadPlumpExcelData1(96,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
            var l=0;
            var slabDesc = "";
            console.log("raw.data:",raw)
            var dataArrayAll = [];
            for(var i=0;i<$scope.subStationsData.valAll.length;i++){
              var arr1 = $scope.subStationsData.valAll;

                if(arr1[i].values.length==1){
                  for(var j=0;j<arr1[i].values.length;j++){
                    var arr2 = arr1[i].values;

                    for(var k=0;k<arr2[j].length;k++){
                      var arr3 = arr2[j][k];
                      var dataArray = [(slabDesc==arr3.slab) ? "" : arr3.slab,arr3.item,arr3.unit,arr3.summer_val,arr3.monsoon_val,arr3.winter_val];
                      $scope.subStationsData.valAll[i].values[j][k] = dataArray;
                      slabDesc=arr3.slab;
                    }
                  }
                } else {
                  dataArrayAll = [];
                  for(var j=0;j<arr1[i].values.length;j++){
                    var arr2 = arr1[i].values;

                    for(var k=0;k<arr2[j].length;k++){
                      l=i;
                      var arr3 = arr2[j][k];
                      var dataArray = [(slabDesc==arr3.slab) ? "" : arr3.slab,arr3.item,arr3.unit,arr3.summer_val,arr3.monsoon_val,arr3.winter_val];
                      dataArrayAll.push(dataArray);
                      slabDesc=arr3.slab;
                    }
                  }
                  $scope.subStationsData.valAll[l].values[0]=[];
                  $scope.subStationsData.valAll[l].values[0]=dataArrayAll;
                }
              }
            //console.log('modified data2: ',$scope.subStationsData.valAll);
            //$localStorage.previousData = raw;
            $scope.subStations = $scope.subStationsData.valAll;
            $scope.subStationsData = $scope.subStationsData;
        })
  };

  $scope.addTable = function(tableIndex){
     var baseObj = $localStorage.baseData.data;
     var table1 = baseObj.valAll[0].valAll[0];
     for(var i=0;i<6;i++){
       var array1 = ["",table1[i][1],table1[i][2],"","",""];
       baseObj.valAll[tableIndex].valAll[0].push(array1);
     }
    $scope.subStations = baseObj.valAll;
    $scope.subStationsData = baseObj;
  };

  $scope.getSingleStations5 = function(Id,pyId){
    if(pyId==null){
      return false;
    }
     $scope.is_show = 'yes';
      $scope.selectedTab = 'tab96';
      $scope.selectedItem = 96;
      $localStorage.firstPyid=pyId;
      $scope.loadPlumpExcelData1(96,pyId,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
      $timeout( function(){
        $scope.viewTabDetail(96,pyId,$localStorage.firstvoltage,$localStorage.firstConsumerId);
        $scope.is_show = 'no';
      }, 400 );
    }

    $scope.getSingleStations6 = function(Id,consumerId){
        if(consumerId==null){
          return false;
        }
    $scope.is_show = 'yes';
    $scope.selectedTab = 'tab96';
    $scope.selectedItem = 96;
    $localStorage.firstConsumerId = consumerId;
    $scope.loadConsumerTypeSub1(96,consumerId);
      $scope.loadPlumpExcelData1(96,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,consumerId);
    $timeout( function(){
      $scope.viewTabDetail(96,$localStorage.firstPyid,$localStorage.firstvoltage,consumerId);
      $scope.is_show = 'no';
    }, 500 );
  }
    $scope.getSingleStations7 = function(Id,vId){
    if(vId==null){
      return false;
    }
     $scope.is_show = 'yes';
      $scope.selectedTab = 'tab96';
      $scope.selectedItem = 96;
        $scope.loadPlumpExcelData1(96,$localStorage.firstPyid,vId,$localStorage.firstClass,$localStorage.firstConsumerId);
      $timeout( function(){
        $localStorage.firstvoltage = vId;
        $scope.viewTabDetail(96,$localStorage.firstPyid,vId,$localStorage.firstConsumerId);
        $scope.is_show = 'no';
      }, 500 );
    }


  $scope.viewTabDetail = function(formId,pyId,vId,consumerId){
    var ApiEndPoints = '/f3_4JExcelView?ph_id='+$localStorage.petitionId+'&form_id='+formId+'&user_id='+$localStorage.userId+'&py_id='+$localStorage.firstPyid+'&consumer_type_id='+consumerId+'&voltage_level_id='+vId;
    ApiServices.fetchFormData(ApiEndPoints,function(data){
       $scope.viewTable = data.listData;
        $scope.total = data.listData.total;
        if($scope.viewTable.length==0){
          $scope.showHidedata = true;
        }else if($scope.viewTable.length>0){
          $scope.showHidedata = false;
        }
     });
  }
  $scope.viewTabDetail(96,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstConsumerId);


$scope.loadHeader = function(){
$rootScope.tableHeader96 = "";
  var ApiEndPoints = '/form_header3_4?form_id='+96+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    ApiServices.fetchFormData(ApiEndPoints,function(data){
        //$scope.formHeader = data.data.header;
        $rootScope.tableHeader96 = data.data.header;
    });
}
$scope.loadHeader()






    /*formtab in form start*/

    $scope.formtab = 1;
    $scope.formsetTab = function(formnewTab){
      if(formnewTab==2){
        $scope.viewTabDetail(96,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstConsumerId);
        console.log('$scope.viewTable: ',$scope.viewTable);
      }
      $scope.formtab = formnewTab;
    };

    $scope.isformSet = function(formtabNum){
      return $scope.formtab === formtabNum;
    };


  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };


  }]
)}
)()
