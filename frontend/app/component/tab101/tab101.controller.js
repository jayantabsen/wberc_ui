(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab101Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab101';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



    $scope.getDataByForm = function(uid,tab,data) {
    		var result = [];
        result = data.vals;
        return result;
    }
  $scope.loadPlumpExcelData1 = function(formId,pyId,vId,conClassId,conTypeId,phaseId){
    console.log('phaseId: ',phaseId);
    var ApiEndPoints =
 '/f3_6JExcel?py_id='+pyId+'&phase_id='+phaseId+'&voltage_level_id='+vId+'&consumer_class_id='+conClassId+'&consumer_type_id='+conTypeId+'&form_id='+101+'&user_id='+$localStorage.userId;
     ApiServices.fetchFormData(ApiEndPoints,function(data){
         $scope.subStationsData = data.data;
         console.log('$scope.subStationsData: ',$scope.subStationsData);
       });
  	}

  $scope.loadAlEnsuingYear = function(formId){
  	$scope.Ensuringyears = [];
  		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.Ensuringyears = data.data.ensuing_petition_year;
          $localStorage.firstPyid=$scope.Ensuringyears[0].py_id;
          //console.log($scope.Ensuringyears);
          $timeout(function() {
            $scope.loadPlumpExcelData1(101,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId,$localStorage.phaseid);
          }, 1000);
  		  });
  };
  $scope.loadAlEnsuingYear(101);

    $scope.loadAlPhase = function(formId){
    	$scope.Ensuringyears = [];
        //182.75.177.246:8181/wberc_v2/api/phase_combo?form_id=101&user_id=6
    		var ApiEndPoints = '/phase_combo?form_id='+formId+'&user_id='+$localStorage.userId;
    		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
    				$scope.allphase = data.listData;
            $localStorage.phaseid=$scope.allphase[0].phase_id;
            console.log($scope.allphase);
            $timeout(function() {
              $scope.loadPlumpExcelData1(101,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId,$localStorage.phaseid);
            }, 1000);
    		  });
    };
    $scope.loadAlPhase(101);
    //$scope.loadPlumpExcelData1(31,2518);

    $scope.loadConsumerType = function(formId){
    	$scope.ConsumerTypes = [];
    		var ApiEndPoints = '/consumerTypesCombo';
    		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
    				$scope.ConsumerTypes = data.data.consumer_types;
            $localStorage.firstConsumerId=$scope.ConsumerTypes[0].consumer_type_id;
            console.log('ConsumerTypes',data.data);
          $scope.loadConsumerTypeSub1(101,$localStorage.firstConsumerId);
    		  });
    };
    $scope.loadConsumerType(101);

      $scope.loadConsumerTypeSub1 = function(formId,sub_id_1){
      	$scope.consumerClassList = [];
      		var ApiEndPoints = '/consumerClassNVoltageLevelCombo?consumer_type_id='+sub_id_1;
      		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
            $scope.consumer = data.data;
      				$scope.consumerClassList = data.data.consumerClassList.consumer_classes;
              $scope.voltageLevelList = data.data.voltageLevelList.voltage_levels;
              $localStorage.firstClass=$scope.consumerClassList[0].consumer_class_id;
      				$localStorage.firstvoltage = $scope.voltageLevelList[0].voltage_level_id;
              console.log($scope.consumerClassList);
      		  });
      };

$scope.getSingleStations1 = function(Id,pyId){
  if(pyId==null){
    return false;
  }
   $scope.is_show = 'yes';
    $scope.selectedTab = 'tab101';
    $scope.selectedItem = 101;
    $localStorage.firstPyid=pyId;
    $scope.loadPlumpExcelData1(101,pyId,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId,$localStorage.phaseid);
    $timeout( function(){
      $scope.is_show = 'no';
    }, 500 );
  }
$scope.getSingleStations2 = function(Id,consumerId){
      if(consumerId==null){
      	return false;
      }
  $scope.is_show = 'yes';
	$scope.selectedTab = 'tab101';
	$scope.selectedItem = 101;
  $localStorage.firstConsumerId = consumerId;
  $scope.loadConsumerTypeSub1(101,consumerId);
	$timeout( function(){
    $scope.loadPlumpExcelData1(101,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,consumerId,$localStorage.phaseid);
		$scope.is_show = 'no';
	}, 500 );
}
$scope.getSingleStations3 = function(Id,classId){
  if(classId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab101';
  	$scope.selectedItem = 101;
		$timeout( function(){
      $localStorage.firstClass = classId;
      $scope.loadPlumpExcelData1(101,$localStorage.firstPyid,$localStorage.firstvoltage,classId,$localStorage.firstConsumerId,$localStorage.phaseid)
			$scope.is_show = 'no';
		}, 500 );
  }
$scope.getSingleStations4 = function(Id,vId){
  if(vId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab101';
  	$scope.selectedItem = 101;
		$timeout( function(){
      $localStorage.firstvoltage = vId;
      $scope.loadPlumpExcelData1(101,$localStorage.firstPyid,vId,$localStorage.firstClass,$localStorage.firstConsumerId,$localStorage.phaseid);
			$scope.is_show = 'no';
		}, 500 );
  }
$scope.getSingleStations6 = function(Id,phaseid){
  if(phaseid==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab101';
  	$scope.selectedItem = 101;
		$timeout( function(){
      $localStorage.phaseid = phaseid;
      $scope.loadPlumpExcelData1(101,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId,phaseid);
			$scope.is_show = 'no';
		}, 500 );
  }

  $scope.loadHeader = function(){
  $rootScope.tableHeader86 = "";
    var ApiEndPoints = '/form_header3_6?form_id='+101+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          //$scope.formHeader = data.data.header;
          $rootScope.tableHeader86 = data.data.header;
      });
  }
  $scope.loadHeader()
  $scope.loadPlumpExcelData1(101,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId,$localStorage.phaseid);


  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f3_6Update';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);
        
        })
        console.log('raw: ', $scope.subStationsData);
  };

    $scope.loadHeader = function(){
    $rootScope.tableHeader101 = "";
      var ApiEndPoints = '/form_header3_6?form_id='+101+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
            //$scope.formHeader = data.data.header;
            $rootScope.tableHeader101 = data.data.header;
        });
    }
    $scope.loadHeader()



  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

  }]
)}
)()
