(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab90Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab90';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



    $scope.getDataByForm = function(uid,tab,data) {
    		var result = [];
        result = data.valAll;
        return result;
    }
    $scope.getDataByForm1 = function(uid,tab,data) {
    		var result = [];
        result.push(data['minCharge']);
        console.log('min charge: ',result);
        return result;
    }
    $scope.getDataByForm2 = function(uid,tab,data) {
    		var result = [];
        console.log('min charge: ',data);
        result = data.thirdTable;
        return result;
    }

    $scope.viewHeader = function(formId,vId,conClassId,conTypeId){
      var ApiEndPoints = '/all_petition_years?form_id=73&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
       ApiServices.fetchFormData(ApiEndPoints,function(data){
            $scope.viewHeader = data.data.years;
            $scope.yearLength =  ( $scope.viewHeader.length)+3;
         });
      }
    $scope.viewHeader();
  $scope.loadPlumpExcelData1 = function(formId,pyId,vId,conClassId,conTypeId){
    // 182.75.177.246:8181/wberc_v2/api/f3_1JExcelViewAllSeason?ph_id=578&form_id=90&user_id=7&py_id=4275&consumer_type_id=1&voltage_level_id=10&searchType=Yearwise&form_id1=91&form_id2=92&form_id3=93
    var ApiEndPoints = '/f3_1JExcelViewAllSeason?ph_id='+$localStorage.petitionId+'&form_id='+90+'&user_id='+$localStorage.userId+'&py_id='+pyId+'&consumer_type_id='+conTypeId+'&voltage_level_id='+vId+'&searchType=Yearwise&form_id1=91&form_id2=92&form_id3=93';
     ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.subStations = data.listData;
          $scope.subStationsData = data.listData;
          $rootScope.tableHeader90 = data.listData[0].header;

          console.log('$scope.subStationsData: ',$scope.subStationsData);
          if($.isEmptyObject(data.listData[0].vo1)){
              $scope.showHidedata = false;
          }else{
              $scope.showHidedata = true;
          }
       });
  	}

  $scope.loadAlEnsuingYear = function(formId){
  	$scope.Ensuringyears = [];
  		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.Ensuringyears = data.data.ensuing_petition_year;
          $localStorage.firstPyid=$scope.Ensuringyears[0].py_id;
          //console.log($scope.Ensuringyears);
          $timeout(function() {
            $scope.loadPlumpExcelData1(90,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
          }, 1000);
  		  });
  };
  $scope.loadAlEnsuingYear(90);

    $scope.loadConsumerType = function(formId){
    	$scope.ConsumerTypes = [];
    		var ApiEndPoints = '/consumerTypesCombo';
    		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
    				$scope.ConsumerTypes = data.data.consumer_types;
            $localStorage.firstConsumerId=$scope.ConsumerTypes[0].consumer_type_id;
            console.log('ConsumerTypes',data.data);
          $scope.loadConsumerTypeSub1(90,$localStorage.firstConsumerId);
    		  });
    };
    $scope.loadConsumerType(90);

  $scope.loadConsumerTypeSub1 = function(formId,sub_id_1){
  	$scope.consumerClassList = [];
  		var ApiEndPoints = '/consumerClassNVoltageLevelCombo?consumer_type_id='+sub_id_1;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
          $scope.consumer = data.data;
  				$scope.consumerClassList = data.data.consumerClassList.consumer_classes;
          $scope.voltageLevelList = data.data.voltageLevelList.voltage_levels;
          $localStorage.firstClass=$scope.consumerClassList[0].consumer_class_id;
  				$localStorage.firstvoltage = $scope.voltageLevelList[0].voltage_level_id;
  		  });
  };

$scope.getSingleStations1 = function(Id,pyId){
  if(pyId==null){
    return false;
  }
   $scope.is_show = 'yes';
    $scope.selectedTab = 'tab90';
    $scope.selectedItem = 90;
    $localStorage.firstPyid=pyId;
    $scope.loadPlumpExcelData1(90,pyId,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
    $timeout( function(){
      $scope.is_show = 'no';
    }, 500 );
  }
$scope.getSingleStations2 = function(Id,consumerId){
      if(consumerId==null){
      	return false;
      }
  $scope.is_show = 'yes';
	$scope.selectedTab = 'tab90';
	$scope.selectedItem = 90;
  $localStorage.firstConsumerId = consumerId;
  $scope.loadConsumerTypeSub1(90,consumerId);
	$timeout( function(){
    $scope.loadPlumpExcelData1(90,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,consumerId);
		$scope.is_show = 'no';
	}, 500 );
}
// $scope.getSingleStations3 = function(Id,classId){
//   if(classId==null){
//   	return false;
//   }
//    $scope.is_show = 'yes';
//   	$scope.selectedTab = 'tab90';
//   	$scope.selectedItem = 90;
// 		$timeout( function(){
//       $localStorage.firstClass = classId;
//       $scope.loadPlumpExcelData1(90,$localStorage.firstPyid,$localStorage.firstvoltage,classId,$localStorage.firstConsumerId)
// 			$scope.is_show = 'no';
// 		}, 500 );
//   }
$scope.getSingleStations4 = function(Id,vId){
  if(vId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab90';
  	$scope.selectedItem = 90;
		$timeout( function(){
      $localStorage.firstvoltage = vId;
      $scope.loadPlumpExcelData1(90,$localStorage.firstPyid,vId,$localStorage.firstClass,$localStorage.firstConsumerId);
			$scope.is_show = 'no';
		}, 500 );
  }



  // $scope.saveStationData = function(Seloption,formId){
  // 	 var postData = {};
  // 	  var raw = $scope.subStationsData;
  //          var url = 'f2_3DataUpdate';
  //     ApiServices.setSubstaionData1({
  //         message:'',
  //         data:raw,
  //         status:1,
  //         url:url,
  //       }, function(data){
  //          $scope.updateMessage=data.message;
  //          $timeout(function() {
  //              $scope.updateMessage='';
  //         }, 3000);
  //       })
  // };




  // $scope.viewTabDetail = function(formId,pyId,vId,consumerId){
  //
  //     var ApiEndPoints = '/f3_1JExcelViewAllSeason?ph_id='+$localStorage.petitionId+'&form_id='+formId+'&user_id='+$localStorage.userId+'&py_id='+pyId+'&consumer_type_id='+consumerId+'&voltage_level_id='+vId+'&season=Summer&searchType=';
  //   ApiServices.fetchFormData(ApiEndPoints,function(data){
  //      $scope.viewTable = data.listData;
  //       $scope.total = data.listData.total;
  //      console.log('$scope.viewTable:', $scope.viewTable);
  //      if($scope.viewTable.length==0){
  //        $scope.showHidedata = true;
  //      }else if($scope.viewTable.length>0){
  //        $scope.showHidedata = false;
  //      }
  //    });
  //    $scope.viewTableYearwise = $localStorage.viewTabYearwiseData;
  // }
  //  $scope.viewTabDetail(90,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstConsumerId);



  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };



  }]
)}
)()
