(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab33Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab33';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    }



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data.vals;
      return result;
  }
  $scope.loadPlumpExcelData1 = function(formId,pyId){
    //182.75.177.246:8181/wberc_v2/api/f1_17fJExcel?form_id=33&ph_id=76&user_id=2
     var ApiEndPoints = '/f1_17fJExcel?form_id=33&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;
          		$scope.subStationsData = data.data;
          		 for(var jj=0;jj < data.data.vals.length;jj++){
                       data.data.vals[jj]['valForStationType'];
          		}
       });
  	}

  $scope.loadPlumpExcelData1(33);
  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f1_17fUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };
  // $scope.ctrlFn= function(){
  //     var raw = angular.copy($scope.subStationsData);
  //     var calcArray = raw.vals;
  //     var result = calcArray.reduce(function(r,o){
  //       console.log('o',o);
  //   		  o.slice(1).forEach(function(v,i){
  //   			   r[i] = (r[i] || 0 ) + parseFloat(v);
  //       	});
  //     	return r;
  //   },[]);
  //   $scope.totalArray = result;
  //   $scope.$apply();
  // }
  $scope.$on('updateTotal', function(evt, data) {
      $scope.totalExpenditure1_17f=data;
          $scope.$apply();
  });


  $scope.loadHeader = function(){
  $rootScope.tableHeader33 = "";
    var ApiEndPoints = '/form_header1_17f?form_id='+33+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.formHeader = data.data.header;
          $rootScope.tableHeader33 = data.data.header;
      });
  }
  $scope.loadHeader()
  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

  $scope.grandTotal = function(){
   var mainraw = angular.copy($scope.subStationsData);
   var rawData = mainraw.vals;
   console.log("mainraw.cols:",mainraw.cols);
   var arr = [];
   for(var i = 0; i<mainraw.cols.length; i++){
     arr.push(0);
   }
   var sums = rawData.reduce(function(result,row,i){
     for(var j = 0; j<mainraw.cols.length; j++){
       result[j] += parseInt(row[j+1]);
     }
     return result
   },arr)
        $scope.grandTotalArr = sums;
         console.log(sums)
  }

  }]
)}
)()
