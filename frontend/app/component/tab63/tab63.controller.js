(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab63Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab63';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data.vals;
      return result;
  }
  $scope.loadPlumpExcelData1 = function(formId,stnId,pyId){
      //182.75.177.246:8181/wberc_v2/api/fD3JExcel?form_id=63&py_id=3493&util_coal_field_id=6&user_id=6
     var ApiEndPoints = '/fD3JExcel?form_id='+formId+'&py_id='+pyId+'&util_coal_field_id='+stnId+'&user_id='+$localStorage.userId+'&ph_id='+$localStorage.petitionId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;
          		$scope.subStationsData = data.data;
          		 for(var jj=0;jj < data.data.vals.length;jj++){
                       data.data.vals[jj]['valForStationType'];
          		}
       });
  	}

  $scope.loadAllStations = function(formId){
  	$scope.Ensuringyears = [];
    //182.75.177.246:8181/wberc_v2/api/fD3CoalFields?form_id=63&ph_id=478&user_id=6
    		var ApiEndPoints = '/fD3CoalFields?form_id='+63+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.AllStationsFuel = data.data.coal_fields;
          $localStorage.firstStn=$scope.AllStationsFuel[0].id;
          console.log($scope.AllStationsFuel);
  		  });
      // $scope.loadPlumpExcelData1(57,$localStorage.firstStn,$localStorage.firstYear);
  };
  $scope.loadAllStations(63);
  $scope.loadAllYears = function(formId){
  	$scope.Ensuringyearsforeign = [];
  		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.Ensuringyearsforeign = data.data.ensuing_petition_year;
          $localStorage.firstYear=$scope.Ensuringyearsforeign[0].py_id;
          console.log($scope.Ensuringyearsforeign);
          $scope.loadPlumpExcelData1(63,$localStorage.firstStn,$localStorage.firstYear);
  		});
  };
  $scope.loadAllYears(63);




  //$scope.loadPlumpExcelData1(31,2518);
  $scope.getSingleStations = function(Id,stationId){
    if(stationId==null){
    	return false;
    }
     $scope.is_show = 'yes';
    	$scope.selectedTab = 'tab63';
    	$scope.selectedItem = 63;
    	$scope.loadPlumpExcelData1(63,stationId,$localStorage.firstYear);
      $localStorage.firstStn=stationId;
  		$timeout( function(){
  			$scope.is_show = 'no';
  		}, 1000 );
    }
    $scope.getSingleYears = function(Id,pyId){
      // alert('pyId',pyId);
      if(pyId==null){
      	return false;
      }
       $scope.is_show = 'yes';
      	$scope.selectedTab = 'tab63';
      	$scope.selectedItem = 63;
        $localStorage.firstYear=pyId;
      	$scope.loadPlumpExcelData1(63,$localStorage.firstStn,pyId);
    		$timeout( function(){
    			$scope.is_show = 'no';
    		}, 1000 );
      }

  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'fD3Update';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
               $scope.errorMessage='';
          }, 5000);
        })
  };
  $scope.loadHeader = function(){
  $rootScope.tableHeader63 = "";
    var ApiEndPoints = '/form_headerD_3?form_id='+63+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          //$scope.formHeader = data.data.header;
          $rootScope.tableHeader63 = data.data.header;
      });
  }
  $scope.loadHeader()
  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };



  }]
)}
)()
