(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab62Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab62';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data.vals;
      return result;
  }
  $scope.loadPlumpExcelData1 = function(formId,stnId,pyId){
    //182.75.177.246:8181/wberc_v2/api/f1_23JExcel?stn_id=5&ph_id=365&form_id=51&user_id=2
    //182.75.177.246:8181/wberc_v2/api/f_A_JExcel?stn_id=1&py_id=512&form_id=57&user_id=2
    //182.75.177.246:8181/wberc_v2/api/fD1JExcel?form_id=61&user_id=2&stn_id=1&py_id=2463
  //  182.75.177.246:8181/wberc_v2/api/f_D2_JExcel?stn_id=5&ph_id=365&form_id=62&user_id=2
     var ApiEndPoints = '/f_D2_JExcel?stn_id='+stnId+'&ph_id='+$localStorage.petitionId+'&form_id=62'+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;
          		$scope.subStationsData = data.data;
          		//  for(var jj=0;jj < data.data.vals.length;jj++){
              //          data.data.vals[jj]['valForStationType'];
          		// }
       });
  	}

  $scope.loadAllStations = function(formId){
  	$scope.AllStationsFuel = [];
      //182.75.177.246:8181/wberc_v2/api/stncombo?form_id=5&util_id=1
      //182.75.177.246:8181/wberc_v2/api/fD1Stations?form_id=61&ph_id=366&user_id=2
  		//var ApiEndPoints = '/fD1Stations?form_id='+61+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;

        //182.75.177.246:8181/wberc_v2/api/fD2Stations?form_id=62&user_id=2&ph_id=365
  		var ApiEndPoints = '/fD2Stations?form_id=62&user_id='+$localStorage.userId+'&ph_id='+$localStorage.petitionId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.AllStationsFuel = data.data.stations;
          $scope.firstStn=$scope.AllStationsFuel[0].id;
          console.log($scope.AllStationsFuel);
          $scope.loadPlumpExcelData1(62,$scope.firstStn);
  		  });
      // $scope.loadPlumpExcelData1(57,$localStorage.firstStn,$localStorage.firstYear);
  };
  $scope.loadAllStations(62);
  // $scope.loadAllYears = function(formId){
  // 	$scope.Ensuringyearsforeign = [];
  // 		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
  // 		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  // 				$scope.Ensuringyearsforeign = data.data.ensuing_petition_year;
  //         $localStorage.firstYear=$scope.Ensuringyearsforeign[0].py_id;
  //         console.log($scope.Ensuringyearsforeign);
  //         $scope.loadPlumpExcelData1(61,$localStorage.firstStn,$localStorage.firstYear);
  // 		});
  // };
  // $scope.loadAllYears(61);




  //$scope.loadPlumpExcelData1(31,2518);
  $scope.getSingleStations = function(Id,stationId){
    if(stationId==null){
    	return false;
    }
     $scope.is_show = 'yes';
    	$scope.selectedTab = 'tab62';
    	$scope.selectedItem = 62;
    	$scope.loadPlumpExcelData1(62,stationId);
      $localStorage.firstStn=stationId;
  		$timeout( function(){
  			$scope.is_show = 'no';
  		}, 1000 );
    }
    // $scope.getSingleYears = function(Id,pyId){
    //   // alert('pyId',pyId);
    //   if(pyId==null){
    //   	return false;
    //   }
    //    $scope.is_show = 'yes';
    //   	$scope.selectedTab = 'tab61';
    //   	$scope.selectedItem = 61;
    //     $localStorage.firstYear=pyId;
    //   	$scope.loadPlumpExcelData1(61,$localStorage.firstStn,pyId);
    // 		$timeout( function(){
    // 			$scope.is_show = 'no';
    // 		}, 1000 );
    //   }

  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f_D2_DataUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
               $scope.errorMessage='';
          }, 5000);
        })
  };


  $scope.loadHeader = function(){
  $rootScope.tableHeader62 = "";
    var ApiEndPoints = '/form_headerD_2?form_id='+62+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.formHeader = data.data.header;
          $rootScope.tableHeader62 = data.data.header;
      });
  }
  $scope.loadHeader()


  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

  }]
)}
)()
