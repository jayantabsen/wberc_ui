(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab80Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab80';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



    $scope.getDataByForm = function(uid,tab,data) {
    		var result = [];
        result = data.valAll;
        return result;
    }
    $scope.getDataByForm1 = function(uid,tab,data) {
    		var result = [];
        result.push(data['minCharge']);
        console.log('min charge: ',result);
        return result;
    }
    $scope.getDataByForm2 = function(uid,tab,data) {
    		var result = [];
        console.log('min charge: ',data);
        result = data.thirdTable;
        return result;
    }
  $scope.loadPlumpExcelData1 = function(formId,pyId,vId,conClassId,conTypeId){
    var ApiEndPoints = '/f2_3JExcel?ph_id='+$localStorage.petitionId+'&form_id='+80+'&user_id='+$localStorage.userId+'&py_id='+pyId+'&consumer_class_id='+conClassId+'&consumer_type_id='+conTypeId+'&voltage_level_id='+vId+'&season=Winter';
     ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.subStations = data.data.valAll;
          $scope.subStationsData = data.data;
          $rootScope.tableHeader80 = data.data.header;

          console.log('$scope.subStationsData: ',$scope.subStationsData);
       });
  	}

  $scope.loadAlEnsuingYear = function(formId){
  	$scope.Ensuringyears = [];
  		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.Ensuringyears = data.data.ensuing_petition_year;
          $localStorage.firstPyid=$scope.Ensuringyears[0].py_id;
          //console.log($scope.Ensuringyears);
          $timeout(function() {
            $scope.loadPlumpExcelData1(80,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
          }, 1000);
  		  });
  };
  $scope.loadAlEnsuingYear(80);

    $scope.loadConsumerType = function(formId){
    	$scope.ConsumerTypes = [];
    		var ApiEndPoints = '/consumerTypesCombo';
    		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
    				$scope.ConsumerTypes = data.data.consumer_types;
            $localStorage.firstConsumerId=$scope.ConsumerTypes[0].consumer_type_id;
            console.log('ConsumerTypes',data.data);
          $scope.loadConsumerTypeSub1(80,$localStorage.firstConsumerId);
    		  });
    };
    $scope.loadConsumerType(80);

  $scope.loadConsumerTypeSub1 = function(formId,sub_id_1){
  	$scope.consumerClassList = [];
  		var ApiEndPoints = '/consumerClassNVoltageLevelCombo?consumer_type_id='+sub_id_1;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
          $scope.consumer = data.data;
  				$scope.consumerClassList = data.data.consumerClassList.consumer_classes;
          $scope.voltageLevelList = data.data.voltageLevelList.voltage_levels;
          $localStorage.firstClass=$scope.consumerClassList[0].consumer_class_id;
  				$localStorage.firstvoltage = $scope.voltageLevelList[0].voltage_level_id;
  		  });
  };

$scope.getSingleStations1 = function(Id,pyId){
  if(pyId==null){
    return false;
  }
   $scope.is_show = 'yes';
    $scope.selectedTab = 'tab80';
    $scope.selectedItem = 80;
    $localStorage.firstPyid=pyId;
    $scope.loadPlumpExcelData1(80,pyId,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
    $timeout( function(){
      $scope.is_show = 'no';
    }, 500 );
  }
$scope.getSingleStations2 = function(Id,consumerId){
      if(consumerId==null){
      	return false;
      }
  $scope.is_show = 'yes';
	$scope.selectedTab = 'tab80';
	$scope.selectedItem = 80;
  $localStorage.firstConsumerId = consumerId;
  $scope.loadConsumerTypeSub1(80,consumerId);
	$timeout( function(){
    $scope.loadPlumpExcelData1(80,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,consumerId);
		$scope.is_show = 'no';
	}, 500 );
}
$scope.getSingleStations3 = function(Id,classId){
  if(classId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab80';
  	$scope.selectedItem = 80;
		$timeout( function(){
      $localStorage.firstClass = classId;
      $scope.loadPlumpExcelData1(80,$localStorage.firstPyid,$localStorage.firstvoltage,classId,$localStorage.firstConsumerId)
			$scope.is_show = 'no';
		}, 500 );
  }
$scope.getSingleStations4 = function(Id,vId){
  if(vId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab80';
  	$scope.selectedItem = 80;
		$timeout( function(){
      $localStorage.firstvoltage = vId;
      $scope.loadPlumpExcelData1(80,$localStorage.firstPyid,vId,$localStorage.firstClass,$localStorage.firstConsumerId);
			$scope.is_show = 'no';
		}, 500 );
  }



  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f2_3DataUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };
  $scope.getSingleStations5 = function(Id,pyId){
    if(pyId==null){
      return false;
    }
     $scope.is_show = 'yes';
      $scope.selectedTab = 'tab80';
      $scope.selectedItem = 80;
      $localStorage.firstPyid=pyId;
      $scope.loadPlumpExcelData1(80,pyId,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
      $timeout( function(){
        //$scope.viewTabDetail(80,pyId,$localStorage.firstvoltage,$localStorage.firstConsumerId);
          $scope.viewTabDetailYearwise(80,pyId,$localStorage.firstvoltage,$localStorage.firstConsumerId);
        $scope.is_show = 'no';
      }, 400 );
    }


    $scope.getSingleStations6 = function(Id,consumerId){
        if(consumerId==null){
          return false;
        }
    $scope.is_show = 'yes';
    $scope.selectedTab = 'tab80';
    $scope.selectedItem = 80;
    $localStorage.firstConsumerId = consumerId;
    $scope.loadConsumerTypeSub1(80,consumerId);
    $timeout( function(){
      $scope.viewTabDetail(80,$localStorage.firstPyid,$localStorage.firstvoltage,consumerId);
      $scope.is_show = 'no';
    }, 500 );
    }

    $scope.getSingleStations7 = function(Id,vId){
    if(vId==null){
      return false;
    }
     $scope.is_show = 'yes';
      $scope.selectedTab = 'tab80';
      $scope.selectedItem = 80;
      $timeout( function(){
        $localStorage.firstvoltage = vId;
        $scope.viewTabDetail(80,$localStorage.firstPyid,vId,$localStorage.firstConsumerId);
        $scope.is_show = 'no';
      }, 500 );
    }




  $scope.viewTabDetail = function(formId,pyId,vId,consumerId){
      var ApiEndPoints = '/f2_3JExcelView?ph_id='+$localStorage.petitionId+'&form_id='+formId+'&user_id='+$localStorage.userId+'&py_id='+pyId+'&consumer_type_id='+consumerId+'&voltage_level_id='+vId+'&season=Winter&searchType=';
    ApiServices.fetchFormData(ApiEndPoints,function(data){
       $scope.viewTable = data.listData;
        $scope.total = data.listData.total;
       console.log('$scope.viewTable:', $scope.viewTable);
       // if($scope.viewTable.length==0){
       //   $scope.showHidedata = true;
       // }else if($scope.viewTable.length>0){
       //   $scope.showHidedata = false;
       // }
       if($.isEmptyObject(data.listData[0])){
           $scope.showHidedata = false;
       }else{
           data.listData[0].thirdTable = $localStorage.thirdTable;
           $scope.showHidedata = true;
       }
     });
     $scope.viewTableYearwise = $localStorage.viewTabYearwiseData;
  }
  //  $scope.viewTabDetail(80,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstConsumerId);

    $scope.viewTabDetailYearwise = function(formId,pyId,vId,consumerId){
        var ApiEndPoints = '/f2_3JExcelView?ph_id='+$localStorage.petitionId+'&form_id='+formId+'&user_id='+$localStorage.userId+'&py_id='+pyId+'&consumer_type_id='+consumerId+'&voltage_level_id='+vId+'&season=Winter&searchType=Yearwise';
      ApiServices.fetchFormData(ApiEndPoints,function(data){
         $scope.viewTable = data.listData;
          $scope.viewTableYearwise = data.listData;
          $scope.total = data.listData.total;
          $localStorage.viewTabYearwiseData = data.listData;
          // $scope.thirdTable = data.listData.thirdTable;
         console.log('$scope.viewTable:', $scope.viewTable);
         // if($scope.viewTable.length==0){
         //   $scope.showHidedata = true;
         // }else if($scope.viewTable.length>0){
         //   $scope.showHidedata = false;
         // }
         $localStorage.thirdTable = data.listData[0].thirdTable;
         if($.isEmptyObject(data.listData[0])){
             $scope.showHidedata = false;
         }else{
             $scope.showHidedata = true;
         }
       });
    }
    $scope.viewTabDetailYearwise(80,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstConsumerId);

    $scope.formtab = 1;
    $scope.formsetTab = function(formnewTab){
      if(formnewTab==2){
      $scope.viewTabDetailYearwise(80,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstConsumerId);
        console.log('$scope.viewTable: ',$scope.viewTable);
      }else if(formnewTab==1){
        $scope.loadPlumpExcelData1(80,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
      }
      $scope.formtab = formnewTab;
    };

    $scope.isformSet = function(formtabNum){
      return $scope.formtab === formtabNum;
    };







  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };



  }]
)}
)()
