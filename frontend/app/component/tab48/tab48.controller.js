(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab48Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab48';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



$scope.getDataByForm = function(uid,tab,data) {
		var result = [];
    result = data.vals;
    return result;
}
$scope.loadPlumpExcelData1 = function(formId){
		var ApiEndPoints = '/f1_20bJExcel?form_id='+48+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
            		$scope.subStations = data.data.vals;
            		$scope.subStationsData = data.data;
     });
	}
$scope.loadPlumpExcelData1(48);

  $scope.saveStationData = function(Seloption,formId){
  	  var postData = {};
  	  var raw = $scope.subStationsData;
      var url = 'f1_20bDataUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };


  $scope.$on('updateTotal', function(evt, data) {
      $scope.totalExpenditure=data;
          console.log('sum Total Array',$scope.totalExpenditure);
          $scope.$apply();
  });


    $scope.loadHeader = function(){
    $rootScope.tableHeader48 = "";
      var ApiEndPoints = '/form_header1_20b?form_id='+48+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
            $scope.formHeader = data.data.header;
            $rootScope.tableHeader48 = data.data.header;
        });
    }
    $scope.loadHeader()
  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

  }]
)}
)()
