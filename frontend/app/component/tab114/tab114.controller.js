(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab114Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab114';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



    $scope.getDataByForm = function(uid,tab,data) {
    		var result = [];
        result = data.vals;
        return result;
    }
  $scope.loadPlumpExcelData1 = function(formId,vId){
    //182.75.177.246:8181/wberc_v2/api/f5_iJExcel?form_id=114&ph_id=564&voltage_fluctuation_system_id=2&user_id=7
   //182.75.177.246:8181/wberc_v2/api/f5_iJExcel?form_id=114&ph_id=567&voltage_fluctuation_system_id=9&user_id=6

    var ApiEndPoints = '/f5_iJExcel?form_id='+formId+'&ph_id='+$localStorage.petitionId+'&voltage_fluctuation_system_id='+vId+'&user_id='+$localStorage.userId;
     ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.subStationsData = [];
      		$scope.subStations = data.data.vals;
      		$scope.subStationsData = data.data;
          $rootScope.tableHeader114 = data.data.header;
       });
  	}





$scope.loadConsumerTypeSub1 = function(formId){
	$scope.voltageLevelList = [];
  //182.75.177.246:8181/wberc_v2/api/voltage_fluctuation_system_for_combo?form_id=114&ph_id=564&user_id=7

		var ApiEndPoints = '/voltage_fluctuation_system_for_combo?form_id='+formId+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
          $scope.voltageLevelList = data.data.voltageFluctuationSystemAl;
          console.log($scope.voltageLevelList);
  				$localStorage.firstvoltageFluc = $scope.voltageLevelList[0].id;
          $scope.loadPlumpExcelData1(114,$localStorage.firstvoltageFluc);
		  });
};
$scope.loadConsumerTypeSub1(114)
$scope.getSingleStations2 = function(Id,vId){
  console.log("id",Id,vId);


      if(vId==null){
      	return false;
      }
  $scope.is_show = 'yes';
	$scope.selectedTab = 'tab114';
	$scope.selectedItem = 114;
  // $scope.loadConsumerTypeSub1(114);
	// $timeout( function(){
    $scope.loadPlumpExcelData1(114,vId);
		$scope.is_show = 'no';
	// }, 500 );
}




  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f5_iUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };

  $scope.viewTabDetail = function(){
    //182.75.177.246:8181/wberc_v2/api/f5_iJExcelTab2?form_id=114&ph_id=564&user_id=7
    var ApiEndPoints = '/f5_iJExcelTab2?form_id='+114+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    ApiServices.fetchFormData(ApiEndPoints,function(data){
       $scope.viewTable = data.data.f5_ivo3Al;
        $scope.total = data.listData.total;
       console.log('$scope.viewTable:', $scope.viewTable.length);
       if($scope.viewTable.length==0){
         $scope.showHidedata = true;
       }else if($scope.viewTable.length>0){
         $scope.showHidedata = false;
       }
     });
  }
  $scope.viewTabDetail();


    // $scope.loadHeader = function(){
    // $rootScope.tableHeader114 = "";
    //   var ApiEndPoints = '/form_header2_1a?form_id='+114+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    //     ApiServices.fetchFormData(ApiEndPoints,function(data){
    //         $scope.formHeader = data.data.header;
    //         $rootScope.tableHeader114 = data.data.header;
    //     });
    // }
    // $scope.loadHeader()

$scope.viewHeader = function(formId,vId,conClassId,conTypeId){
  var ApiEndPoints = '/all_petition_years?form_id=67&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
   ApiServices.fetchFormData(ApiEndPoints,function(data){
        $scope.viewHeader = data.data.years;
        $scope.yearLength =  ( $scope.viewHeader.length)+3;
     });
	}
$scope.viewHeader();






    /*formtab in form start*/

    $scope.formtab = 1;
    $scope.formsetTab = function(formnewTab){
      if(formnewTab==2){
        $scope.viewTabDetail();
        console.log('$scope.viewTable: ',$scope.viewTable);
      }else if(formnewTab==1){
        $scope.loadPlumpExcelData1(114,$localStorage.firstvoltageFluc);
      }
      $scope.formtab = formnewTab;
    };

    $scope.isformSet = function(formtabNum){
      return $scope.formtab === formtabNum;
    };







  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

  }]
)}
)()
