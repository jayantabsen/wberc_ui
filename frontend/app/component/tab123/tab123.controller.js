(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab123Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab123';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



$scope.getDataByForm = function(uid,tab,data) {
		var result = [];
    result = data['vals'];
    return result;
}
$scope.loadPlumpExcelData1 = function(formId,pyId){
    var ApiEndPoints = '/f5_xJExcel?form_id=123&py_id='+pyId+'&user_id='+$localStorage.userId;
		//var ApiEndPoints = '/f_5_8JExcel?ph_id='+$localStorage.petitionId+'&form_id=123&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
        		$scope.subStations = data.data.vals;
        		$scope.subStationsData = data.data;
        		//  for(var jj=0;jj < data.data.vals.length;jj++){
            //          data.data.vals[jj]['valForStationType'];
        		// }
     });
	}

// $scope.loadPlumpExcelData1($localStorage.formId);
$scope.loadAllStations = function(formId){
  $scope.Ensuringyears = [];
    var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
    ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
        $scope.Ensuringyears = data.data.ensuing_petition_year;
        $scope.firstStn=$scope.Ensuringyears[0].py_id;
        console.log($scope.Ensuringyears);
        $scope.loadPlumpExcelData1(123,$scope.firstStn);
      });
};
$scope.loadAllStations(123);
$scope.getSingleStations = function(Id,stationId){
  if(stationId==null){
    return false;
  }
   $scope.is_show = 'yes';
    $scope.selectedTab = 'tab123';
    $scope.selectedItem = 123;
    $scope.loadPlumpExcelData1(123,stationId);
    $timeout( function(){
      $scope.is_show = 'no';
    }, 1000 );
  }

  $scope.saveStationData = function(Seloption,formId){
  	  var postData = {};
  	  var raw = $scope.subStationsData;
      var url = 'f5_xUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };
  $scope.loadHeader = function(){
  $rootScope.tableHeader123 = "";
    var ApiEndPoints = '/form_header5_x?form_id='+123+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.formHeader = data.data.header;
          $rootScope.tableHeader123 = data.data.header;
      });
  }
  $scope.loadHeader()
  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

  }]
)}
)()
