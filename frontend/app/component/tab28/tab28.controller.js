(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab28Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab28';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };
    $scope.subStationsDatax = {};


  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data['vals'];
      return result;
  }
  $scope.loadPlumpExcelData1 = function(formId){
    console.log('tab28 controller selectedTable: ',$localStorage.formId);
    //182.75.177.246:8181/wberc_v2/api/f1_17JExcel?form_id=27&ph_id=478&user_id=6
     var ApiEndPoints = '/f1_17aJExcel?form_id='+28+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;

          		$scope.subStationsData = data.data;
          		//  for(var jj=0;jj < data.data.vals.length;jj++){
              //          data.data.vals[jj]['valForStationType'];
          		// }

       });
  	}

  $scope.loadPlumpExcelData1($localStorage.formId);


  $scope.saveStationData = function(Seloption,formId){
  	  var postData = {};
  	  var raw = $scope.subStationsData;
      var url = 'f1_17aUpdate';
      console.log("raw",$scope);
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };

        // $scope.loadHeader = function(){
        //   var ApiEndPoints = '/form_header1_16?form_id='+26+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        //     ApiServices.fetchFormData(ApiEndPoints,function(data){
        //         $scope.formHeader = data.data.header;
        //         $localStorage.tableHeader = data.data.header;
        //     });
        // }
        // $scope.loadHeader()

        $scope.loadHeader = function(){
          $rootScope.tableHeader28 = "";
          var ApiEndPoints = '/form_header1_17a?form_id='+28+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
            ApiServices.fetchFormData(ApiEndPoints,function(data){
                //$scope.formHeader = data.data.header;
                $rootScope.tableHeader28 = data.data.header;
            });
        }
        $scope.loadHeader()




  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };



  }]
)}
)()
