(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab89Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab89';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };

    $scope.finance = {};

        $scope.getDataByForm = function(uid,tab,data) {
        		var result = [];
            result = data.valAll;
            return result;
        }

  $scope.loadPlumpExcelData1 = function(formId,pyId,vId,conClassId,conTypeId){
    var ApiEndPoints = '/f2_9JExcel?ph_id='+$localStorage.petitionId+'&form_id='+89+'&user_id='+$localStorage.userId+'&py_id='+pyId+'&consumer_class_id='+conClassId+'&consumer_type_id='+conTypeId+'&voltage_level_id='+vId;
     ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.subStations = data.data.valAll;
          $scope.subStationsData = data.data;
          for(var key in $scope.subStations){
            console.log('loadPlumpExcelData1 key: ', $scope.subStations[key].val);
              //$scope.subStations[key].val ='';
            //$scope.finance.attachment_path[key]='';
            //$scope.finance.attachment_path[key]=$scope.subStations[key].val;
          }
          //console.log('modified data: ',$scope.finance);
       });
  	}

  $scope.loadAlEnsuingYear = function(formId){
  	$scope.Ensuringyears = [];
  		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.Ensuringyears = data.data.ensuing_petition_year;
          $localStorage.firstPyid=$scope.Ensuringyears[0].py_id;
          $timeout(function() {
            $scope.loadPlumpExcelData1(89,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
          }, 1000);
  		  });
  };
  $scope.loadAlEnsuingYear(89);

    $scope.loadConsumerType = function(formId){
    	$scope.ConsumerTypes = [];
    		var ApiEndPoints = '/consumerTypesCombo';
    		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
    				$scope.ConsumerTypes = data.data.consumer_types;
            $localStorage.firstConsumerId=$scope.ConsumerTypes[0].consumer_type_id;
            console.log('ConsumerTypes',data.data);
          $scope.loadConsumerTypeSub1(89,$localStorage.firstConsumerId);
    		  });
    };
    $scope.loadConsumerType(89);

      $scope.loadConsumerTypeSub1 = function(formId,sub_id_1){
      	$scope.consumerClassList = [];
      		var ApiEndPoints = '/consumerClassNVoltageLevelCombo?consumer_type_id='+sub_id_1;
      		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
            $scope.consumer = data.data;
      				$scope.consumerClassList = data.data.consumerClassList.consumer_classes;
              $scope.voltageLevelList = data.data.voltageLevelList.voltage_levels;
              $localStorage.firstClass=$scope.consumerClassList[0].consumer_class_id;
      				$localStorage.firstvoltage = $scope.voltageLevelList[0].voltage_level_id;
              console.log($scope.consumerClassList);
      		  });
      };

$scope.getSingleStations1 = function(Id,pyId){
  if(pyId==null){
    return false;
  }
   $scope.is_show = 'yes';
    $scope.selectedTab = 'tab89';
    $scope.selectedItem = 89;
    $localStorage.firstPyid=pyId;
    $scope.loadPlumpExcelData1(89,pyId,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
    $timeout( function(){
      $scope.is_show = 'no';
    }, 500 );
  }
$scope.getSingleStations2 = function(Id,consumerId){
      if(consumerId==null){
      	return false;
      }
  $scope.is_show = 'yes';
	$scope.selectedTab = 'tab89';
	$scope.selectedItem = 89;
  $localStorage.firstConsumerId = consumerId;
  $scope.loadConsumerTypeSub1(89,consumerId);
	$timeout( function(){
    $scope.loadPlumpExcelData1(89,$localStorage.firstPyid,$localStorage.firstvoltage,$localStorage.firstClass,consumerId);
		$scope.is_show = 'no';
	}, 500 );
}
$scope.getSingleStations3 = function(Id,classId){
  if(classId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab89';
  	$scope.selectedItem = 89;
		$timeout( function(){
      $localStorage.firstClass = classId;
      $scope.loadPlumpExcelData1(89,$localStorage.firstPyid,$localStorage.firstvoltage,classId,$localStorage.firstConsumerId)
			$scope.is_show = 'no';
		}, 500 );
  }
$scope.getSingleStations4 = function(Id,vId){
  if(vId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab89';
  	$scope.selectedItem = 89;
		$timeout( function(){
      $localStorage.firstvoltage = vId;
      $scope.loadPlumpExcelData1(89,$localStorage.firstPyid,vId,$localStorage.firstClass,$localStorage.firstConsumerId);
			$scope.is_show = 'no';
		}, 500 );
  }


  $scope.saveStationData = function(data){
  	 var postData = {};
  	  var raw = $scope.subStationsData
   	  var rawPrev = $scope.subStationsData.valAll;
      //var dataArr = [];
      raw.valAll = rawPrev;
           var url = 'f2_9DataUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);
        
        })
  };

  $scope.getSingleStations5 = function(Id,pyId){
    if(pyId==null){
      return false;
    }
     $scope.is_show = 'yes';
      $scope.selectedTab = 'tab89';
      $scope.selectedItem = 89;
      $localStorage.firstPyid=pyId;
      $scope.loadPlumpExcelData1(89,pyId,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
      $timeout( function(){
        $scope.viewTabDetail();
        $scope.is_show = 'no';
      }, 400 );
    }

  $scope.viewTabDetail = function(){
    var ApiEndPoints = '/f2_9JExcelView?ph_id='+$localStorage.petitionId+'&form_id='+89+'&user_id='+$localStorage.userId+'&py_id='+$localStorage.firstPyid;
    ApiServices.fetchFormData(ApiEndPoints,function(data){
       $scope.viewTable = data.listData;
           // if($scope.viewTable.length==0){
           //   $scope.showHidedata = true;
           // }else if($scope.viewTable.length>0){
           //   $scope.showHidedata = false;
           // }
     });
  }
  $scope.viewTabDetail();









    /*formtab in form start*/

    $scope.formtab = 1;
    $scope.formsetTab = function(formnewTab){
      if(formnewTab==2){
        $scope.viewTabDetail();
        console.log('$scope.viewTable: ',$scope.viewTable);
      }
      $scope.formtab = formnewTab;
    };

    $scope.isformSet = function(formtabNum){
      return $scope.formtab === formtabNum;
    };


  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };


  }]
)}
)()
