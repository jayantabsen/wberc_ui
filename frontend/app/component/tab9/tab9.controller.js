(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab9Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab9';



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result =  data['vals'][uid]['usage'];
      return result;
  }
  $scope.loadEnergyData = function(formId,sourceId){
    //182.75.177.246:8181/wberc_v2/api/f1_6aJExcel?form_id=9&ph_id=242&source_id=1&user_id=2
    var ApiEndPoints = '/f1_6aJExcel?form_id='+9+'&ph_id='+$localStorage.petitionId+'&source_id='+sourceId+'&user_id='+$localStorage.userId;
    $scope.subStationsData = [];
      var verison = 1;
         ApiServices.fetchFormData(ApiEndPoints,function(data){
  		    $scope.subStationsData = [];
  			$scope.subStations = data.data.vals;
  			$scope.subStationsData = data.data;
  	  });
};

$scope.loadAllStations = function(formId){
	$scope.AllSource = [];
  //182.75.177.246:8181/wberc_v2/api/sourceListing1_6a?form_id=9&ph_id=396&user_id=2
		var ApiEndPoints = '/sourceListing1_6a?form_id='+formId+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
  //  console.log('ApiEndPoints', ApiEndPoints);
		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
				$scope.AllSource = data.data.sources;
        $scope.firstStn=$scope.AllSource[0].id;
        $scope.loadEnergyData(9,$scope.firstStn);
		  });
};
$scope.loadAllStations(9);
$scope.getSingleStations = function(Id,stationId){
  if(stationId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab9';
  	$scope.selectedItem = 9;
  	$scope.loadEnergyData(9,stationId);
		$timeout( function(){
			$scope.is_show = 'no';
		}, 1000 );
  }


  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
   	  var raw = $scope.subStationsData;
           var url = 'f1_6aUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };


  $scope.grandTotal = function(){
   var mainraw = angular.copy($scope.subStationsData);
   var raw = mainraw.vals;
   $scope.grandTotalArr = raw.reduce(function(r,o){
       console.log('o.usage: ', o.usage.pop());
       o.usage.forEach(function(a){
         //console.log('a.slice(1): ', a.slice(1));
             a.slice(1).forEach(function(v,i){
                r[i] = (r[i] || 0 ) + parseFloat(v);
             });
         });
       return r;
     },[])
     console.log($scope.grandTotalArr);
  }

    // $scope.loadHeader = function(){
    //   var ApiEndPoints = '/form_header1_5?form_id='+8+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    //     ApiServices.fetchFormData(ApiEndPoints,function(data){
    //         $scope.formHeader = data.data.header;
    //         $localStorage.tableHeader = data.data.header;
    //     });
    // }
    // $scope.loadHeader()

    $scope.loadHeader = function(){
      $rootScope.tableHeader8 = "";
      var ApiEndPoints = '/form_header1_5?form_id='+9+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
            $scope.formHeader = data.data.header;
            $rootScope.tableHeader9 = data.data.header;
        });
    }
    $scope.loadHeader()


  /*tab in form start*/
  $scope.tab=1;
    $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };
  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

  }]
)}
)()
