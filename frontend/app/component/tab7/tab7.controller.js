(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab7Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab7';



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
 			result = data['vals'][uid]['usage'];
      return result;
  }

  $scope.plumpStationAvailability = function(){
     var ApiEndPoints = '/pump_stn_availability?form_id='+formId+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
     ApiServices.fetchFormData(ApiEndPoints,function(data){
       if(data.status==1){
          $scope.pumpingStnError="";
       }else{
          $scope.pumpingStnError=data.message;
       }
     });
  };
  $scope.plumpStationAvailability();

  $scope.loadPlumpExcelData = function(formId,stationId){
    var ApiEndPoints = '/f1_4bJExcel?form_id='+7+'&ph_id='+$localStorage.petitionId+'&pump_stn_id='+stationId+'&user_id='+$localStorage.userId;
    $scope.subStationsData = [];
     ApiServices.fetchFormData(ApiEndPoints,function(data){
       $scope.subStationsData = [];
  	   $scope.subStations = data.data.vals;
  	   $scope.subStationsData = data.data;
  });
};

$scope.loadAllStations = function(formId){
	$scope.AllStations = [];
		var ApiEndPoints = '/pmp_stncombo?form_id='+7+'&util_id='+$localStorage.utilId;
  //  console.log('ApiEndPoints', ApiEndPoints);
		ApiServices.fetchFormData(ApiEndPoints,function(data){
				$scope.AllPumpStationCombo = data.data.pump_stations;
        $scope.firstStn=$scope.AllPumpStationCombo[0].pump_stn_id;
        $scope.loadPlumpExcelData(7,$scope.firstStn);
		  });
};
$scope.loadAllStations(7);
$scope.getSingleStations = function(Id,stationId){
  if(stationId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab7';
  	$scope.selectedItem = 7;
  	$scope.loadPlumpExcelData(7,stationId);
		$timeout( function(){
			$scope.is_show = 'no';
		}, 1000 );
  }


  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
   	  var raw = $scope.subStationsData;
           var url = 'f1_4bUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };


  $scope.grandTotal = function(){
   var mainraw = angular.copy($scope.subStationsData);
   var raw = mainraw.vals;
   $scope.grandTotalArr = raw.reduce(function(r,o){
       console.log('o.usage: ', o.usage.pop());
       o.usage.forEach(function(a){
         //console.log('a.slice(1): ', a.slice(1));
             a.slice(1).forEach(function(v,i){
                r[i] = (r[i] || 0 ) + parseFloat(v);
             });
         });
       return r;
     },[])
     console.log($scope.grandTotalArr);
     raw= 0;
  }
  // $scope.loadHeader = function(){
  //   var ApiEndPoints = '/form_header1_4b?form_id='+7+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
  //     ApiServices.fetchFormData(ApiEndPoints,function(data){
  //         $scope.formHeader = data.data.header;
  //         $localStorage.tableHeader = data.data.header;
  //     });
  // }
  // $scope.loadHeader()
  $scope.loadHeader = function(){
    $rootScope.tableHeader7 = "";
    var ApiEndPoints = '/form_header1_4b?form_id='+7+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.formHeader = data.data.header;
          $rootScope.tableHeader7 = data.data.header;
      });
  }
  $scope.loadHeader()
  /*tab in form start*/
  $scope.tab=1;
    $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };
  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };








  }]
)}
)()
