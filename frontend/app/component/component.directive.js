(function() {
  'use strict';
  var app = angular.module('mean');
  app.directive('formTemplate', [function(){

    return{
    	restrict : 'AEC',
    	replace : true,
    	transclude :  true,
      scope: {
        tabname: '@'
      },
     controller:'@', //specify the controller is an attribute
     name:'ctrlName', //name of the attribute.
     //controller : 'Tab1Controller',

      templateUrl: function(element, attrs) {
        return 'app/component/tab' + attrs.tab + '/tab'+ attrs.tab + '.html';
      }
    };




    //$scope.init();
  }]);
})();
