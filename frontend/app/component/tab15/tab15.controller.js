(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab15Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'ta15';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };
$scope.totalExpenditure ='';



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data.vals;
      return result;
  }
  $scope.loadPlumpExcelData1 = function(formId,pyId){
    //182.75.177.246:8181/wberc_v2/api/f1_9aJExcel?form_id=15&ph_id=256&user_id=6
     var ApiEndPoints = '/f1_9aJExcel?form_id=15&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;
          		$scope.subStationsData = data.data;
          		 for(var jj=0;jj < data.data.vals.length;jj++){
                       data.data.vals[jj]['valForStationType'];
          		}
       });
  	}

  $scope.loadPlumpExcelData1(15);
  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f1_9aUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };
  // $scope.ctrlFn= function(){
  //     var raw = angular.copy($scope.subStationsData);
  //     var calcArray = raw.vals;
  //     var result = calcArray.reduce(function(r,o){
  //       console.log('o',o);
  //   		  o.slice(1).forEach(function(v,i){
  //   			   r[i] = (r[i] || 0 ) + parseFloat(v);
  //       	});
  //     	return r;
  //   },[]);
  //   $scope.totalArray = result;
  //   $scope.$apply();
  // }

  $scope.$on('updateTotal', function(evt, data) {
      $scope.totalExpenditure1_9a=data;
          $scope.$apply();
  });

  // $scope.loadHeader = function(){
  //   var ApiEndPoints = '/form_header1_9a?form_id='+15+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
  //     ApiServices.fetchFormData(ApiEndPoints,function(data){
  //         $scope.formHeader = data.data.header;
  //         $localStorage.tableHeader = data.data.header;
  //     });
  // }
  // $scope.loadHeader()

  $scope.loadHeader = function(){
    $rootScope.tableHeader15 = "";
    //182.75.177.246:8181/wberc_v2/api/form_header1_9a?form_id=15&ph_id=475&user_id=6

    var ApiEndPoints = '/form_header1_9a?form_id='+15+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          //$scope.formHeader = data.data.header;
          $rootScope.tableHeader15 = data.data.header;
      });
  }
  $scope.loadHeader()
  /*tab in form start*/

    $scope.tab = 1;
    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };


    $scope.getFormComment = function(){
  		var ApiEndPoints = '/get_form_comment?form_id='+15+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.getFormComment(ApiEndPoints,function(data){
          $scope.commentList = data.data.comments;
          if($scope.commentList[0].comments==' '){
            $scope.showHideComment = true;
          }
     });
    };
    $scope.getFormComment();

    $scope.daleteComment = function(commentId){
        var url = "http://182.75.177.246:8181/wberc_v2/api/delete_1_comment?arr_comment_id="+commentId;
        ApiServices.getFileDownload(url,function(data){
          if(data.status==1){
            $scope.getFormComment();
          }
        });
    };
    $scope.submit_comment_Form = function(){
        var url = "http://182.75.177.246:8181/wberc_v2/api/insert_1_comment";
        var formdata = new FormData();
        formdata.append('comment', $scope.comments.comment);
        formdata.append('form_id',15);
        formdata.append('ph_id', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
        formdata.append('user_id', parseInt(localStorage.getItem("ngStorage-userId").replace(/['"]+/g, '')));
        ApiServices.PostRequest(url, formdata,function(data){
          alert(data.message);
          $scope.comments.comment=" ";
          $scope.showHideComment = false;
          $scope.getFormComment();
        });
    };

    

      $scope.loadGlobalFiles = function(){
         $scope.globalfiles = [];
         var ApiEndPoints = '/file_listing?ph_id='+$localStorage.petitionId+'&form_id='+15;
         ApiServices.GetGlobalFiles(ApiEndPoints,function(data){
           $scope.globalfiles = data;
          });
      };
      $scope.docIdFn = function(docId){
          var url = "http://182.75.177.246:8181/wberc_v2/api/file_delete_by_id?docId="+docId;
          ApiServices.getFileDownload(url,function(data){
            if(data.status==1){
              $scope.loadGlobalFiles();
            }
          });
      };
      $scope.submit_attachment_Form = function(){
        if($scope.attachment.file != undefined){
          var url = "http://182.75.177.246:8181/wberc_v2/api/file_upload_v2";
          var formdata = new FormData();
          formdata.append('file', $scope.attachment.file);
          formdata.append('docName', $scope.attachment.file.name);
          formdata.append('description', $scope.attachment.comment);
          formdata.append('AttachmentType', $scope.attachment.attachment_type_desc)
          formdata.append('petitionHeaderId', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
          formdata.append('formID', 15);
          formdata.append('utilid', parseInt(localStorage.getItem('ngStorage-utilId').replace(/['"]+/g, '')));
          formdata.append('module', 'ARR');
          ApiServices.PostGlobalFileUpload(url, formdata,function(data){
            alert(data.message);
            $scope.loadGlobalFiles();
            $uibModalInstance.dismiss('cancel');
          });
        }else {
          alert("Please select file to attach..");
        }
        $scope.attachment = {
          'comment':'',
          'form_ref':'',
          'file':'',
        };
      };

      $scope.loadGlobalFiles();


    }]
  )}
)()
