(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab5Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab5';
    $scope.selectedTab2 = 'tab5-1';
    $scope.selectedTab5_1 = false;
    $scope.selectedTab5 = true;



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data['vals'][uid]['usage'];
      return result;
  }
  $scope.loadseasonalData = function(formId,stationId){
    var ApiEndPoints = '/f1_3JExcel?form_id='+5+'&ph_id='+$localStorage.petitionId+'&stn_id='+stationId+'&user_id='+$localStorage.userId;
    $scope.subStationsData = [];

      var verison = 1;
         ApiServices.fetchFormData(ApiEndPoints,function(data){
  		    $scope.subStationsData = [];
  			$scope.subStations = data.data.vals;
  			$scope.subStationsData = data.data;
  	  });
};
$scope.loadGrossSeasonalData = function(formId){
  var ApiEndPoints = '/f1_3SumJExcel?form_id='+5+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
  $scope.totalStationsData = [];

    var verison = 1;
       ApiServices.fetchFormData(ApiEndPoints,function(data){
        $scope.totalStationsData = [];
      $scope.totalStations = data.data.vals;
      $scope.totalStationsData = data.data;
    });
};

$scope.loadAllStations = function(formId){
	$scope.AllStations = [];
		var ApiEndPoints = '/stncombo?form_id='+5+'&util_id='+$localStorage.utilId;
		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
				$scope.AllStations = data.data.stations;
        $scope.firstStn=$scope.AllStations[0].id;
        $scope.AllStations.push({"id":"grossId","name":"GROSS"});
        $scope.loadseasonalData(5,$scope.firstStn);
		  });
};
$scope.loadAllStations(5);
$scope.getSingleStations = function(Id,stationId){
  if(stationId==null){
  	return false;
  }
    if(stationId=="grossId"){
      $scope.selectedTab5_1 = true;
      $scope.selectedTab5 = false;
      $scope.loadGrossSeasonalData(5);
    }else{
        $scope.is_show = 'yes';
  	    $scope.selectedTab = 'tab5';
  	    $scope.selectedItem = 5;
        $scope.selectedTab5_1 = false;
        $scope.selectedTab5 = true;
  	    $scope.loadseasonalData(5,stationId);
      }
		$timeout( function(){
			$scope.is_show = 'no';
		}, 1000 );
  }


  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
   	  var raw = $scope.subStationsData;
           var url = 'f1_3Update';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };


$scope.grandTotal = function(){
 var mainraw = angular.copy($scope.subStationsData);
 var raw = mainraw.vals;
 $scope.grandTotalArr = raw.reduce(function(r,o){
      o.usage.pop();
     o.usage.forEach(function(a){
           a.slice(1).forEach(function(v,i){
              r[i] = (r[i] || 0 ) + parseFloat(v);
           });
       });
     return r;
   },[])
   raw= 0;
}

$scope.grossGrandTotal = function(){
  console.log('$scope.totalStationsData: ',$scope.totalStationsData);
 var mainraw = angular.copy($scope.totalStationsData);
 var raw = mainraw.vals;
 $scope.grossGrandTotalArr = raw.reduce(function(r,o){
      o.usage.pop();
     o.usage.forEach(function(a){
           a.slice(1).forEach(function(v,i){
              r[i] = (r[i] || 0 ) + parseFloat(v);
           });
       });
     return r;
   },[])
   raw= 0;
}

  $scope.loadHeader = function(){
  $rootScope.tableHeader5 = "";
  var ApiEndPoints = '/form_header1_3?form_id='+5+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
  ApiServices.fetchFormData(ApiEndPoints,function(data){
      $scope.formHeader = data.data.header;
      $rootScope.tableHeader5 = data.data.header;
  });
  }
  $scope.loadHeader();

  /*tab in form start*/

    /*tab in form start*/
    $scope.tab=1;
      $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };
    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };
    //
    // $scope.getFormComment = function(){
  	// 	var ApiEndPoints = '/get_form_comment?form_id='+$localStorage.formId +'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    //   ApiServices.getFormComment(ApiEndPoints,function(data){
    //       $scope.commentList = data.data.comments;
    //       if($scope.commentList[0].comments==' '){
    //         $scope.showHideComment = true;
    //       }
    //  });
    // };
    // $scope.getFormComment();
    //
    // $scope.daleteComment = function(commentId){
    //     var url = "http://182.75.177.246:8181/wberc_v2/api/delete_1_comment?arr_comment_id="+commentId;
    //     ApiServices.getFileDownload(url,function(data){
    //       if(data.status==1){
    //         $scope.getFormComment();
    //       }
    //     });
    // };
    // $scope.submit_comment_Form = function(){
    //     var url = "http://182.75.177.246:8181/wberc_v2/api/insert_1_comment";
    //     var formdata = new FormData();
    //     formdata.append('comment', $scope.comments.comment);
    //     formdata.append('form_id', $localStorage.formId);
    //     formdata.append('ph_id', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
    //     formdata.append('user_id', parseInt(localStorage.getItem("ngStorage-userId").replace(/['"]+/g, '')));
    //     ApiServices.PostRequest(url, formdata,function(data){
    //         alert(data.message);
    //         $scope.comments.comment=" ";
    //         $scope.showHideComment = false;
    //         $scope.getFormComment();
    //     });
    // };
    //
    // $scope.loadGlobalFiles = function(){
    //    $scope.globalfiles = [];
    //    var ApiEndPoints = '/file_listing?ph_id='+$localStorage.petitionId+'&form_id='+$localStorage.formId;
    //    ApiServices.GetGlobalFiles(ApiEndPoints,function(data){
    //      $scope.globalfiles = data;
    //     });
    // };
    // $scope.docIdFn = function(docId){
    //     var url = "http://182.75.177.246:8181/wberc_v2/api/file_delete_by_id?docId="+docId;
    //     ApiServices.getFileDownload(url,function(data){
    //       if(data.status==1){
    //         $scope.loadGlobalFiles();
    //         $rootScope.loadAllAttachFiles();
    //       }
    //     });
    // };
    // $scope.submit_attachment_Form = function(){
    //   if($scope.attachment.file != undefined){
    //     var url = "http://182.75.177.246:8181/wberc_v2/api/file_upload_v2";
    //     var formdata = new FormData();
    //     formdata.append('file', $scope.attachment.file);
    //     formdata.append('docName', $scope.attachment.file.name);
    //     formdata.append('description', $scope.attachment.comment);
    //     formdata.append('AttachmentType', $scope.attachment.attachment_type_desc)
    //     formdata.append('petitionHeaderId', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
    //     formdata.append('formID', $localStorage.formId);
    //     formdata.append('utilid', parseInt(localStorage.getItem('ngStorage-utilId').replace(/['"]+/g, '')));
    //     formdata.append('module', 'ARR');
    //     ApiServices.PostGlobalFileUpload(url, formdata,function(data){
    //       $scope.loadGlobalFiles();
    //       // $uibModalInstance.dismiss('cancel');
    //     });
    //   }else {
    //     alert("Please select file to attach..");
    //   }
    //   $scope.attachment = {
    //     'comment':'',
    //     'form_ref':'',
    //     'file':'',
    //   };
    // };
    // $scope.loadGlobalFiles();
    //
    // $scope.ctrlFn= function(){
    //   $scope.$apply();
    // }


  }]
)}
)()
