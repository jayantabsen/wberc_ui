(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab116Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab116';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



    $scope.getDataByForm = function(uid,tab,data) {
    		var result = [];
        result = data.vals;
        return result;
    }
  $scope.loadPlumpExcelData1 = function(formId,vId){
  //  182.75.177.246:8181/wberc_v2/api/f5_iiiJExcel?form_id=116&ph_id=576&ht_feeders_id=3&user_id=6

    var ApiEndPoints = '/f5_iiiJExcel?form_id='+formId+'&ph_id='+$localStorage.petitionId+'&ht_feeders_id='+vId+'&user_id='+$localStorage.userId;
     ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.subStationsData = [];
      		$scope.subStations = data.data.vals;
      		$scope.subStationsData = data.data;
          $rootScope.tableHeader116 = data.data.header;
       });
  	}





$scope.loadConsumerTypeSub1 = function(formId){
	$scope.voltageLevelList = [];
  //182.75.177.246:8181/wberc_v2/api/ht_feeders_for_combo?form_id=116&ph_id=576&user_id=6
		var ApiEndPoints = '/ht_feeders_for_combo?form_id='+formId+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
          $scope.voltageLevelList = data.data.htFeedersAl;
  				$localStorage.firstvoltageFluc = $scope.voltageLevelList[0].id;
          console.log($scope.voltageLevelList);
          $scope.loadPlumpExcelData1(116,$localStorage.firstvoltageFluc);
		  });
};
$scope.loadConsumerTypeSub1(116)
$scope.getSingleStations2 = function(Id,vId){
      if(vId==null){
      	return false;
      }
  $scope.is_show = 'yes';
	$scope.selectedTab = 'tab116';
	$scope.selectedItem = 116;
  // $scope.loadConsumerTypeSub1(116);
	// $timeout( function(){
    $scope.loadPlumpExcelData1(116,vId);
		$scope.is_show = 'no';
	// }, 500 );
}




  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f5_iiiUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };

  $scope.viewTabDetail = function(){
  //  182.75.177.246:8181/wberc_v2/api/f5_iiiJExcelTab2?form_id=116&ph_id=576&user_id=6
    var ApiEndPoints = '/f5_iiiJExcelTab2?form_id='+116+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    ApiServices.fetchFormData(ApiEndPoints,function(data){
       $scope.viewTable = data.data.f5_ivo3Al;
        $scope.total = data.listData.total;
       console.log('$scope.viewTable:', $scope.viewTable.length);
       if($scope.viewTable.length==0){
         $scope.showHidedata = true;
       }else if($scope.viewTable.length>0){
         $scope.showHidedata = false;
       }
     });
  }
  $scope.viewTabDetail();


    // $scope.loadHeader = function(){
    // $rootScope.tableHeader116 = "";
    //   var ApiEndPoints = '/form_header2_1a?form_id='+116+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    //     ApiServices.fetchFormData(ApiEndPoints,function(data){
    //         $scope.formHeader = data.data.header;
    //         $rootScope.tableHeader116 = data.data.header;
    //     });
    // }
    // $scope.loadHeader()

$scope.viewHeader = function(formId,vId,conClassId,conTypeId){
  var ApiEndPoints = '/all_petition_years?form_id=116&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
   ApiServices.fetchFormData(ApiEndPoints,function(data){
        $scope.viewHeader = data.data.years;
        $scope.yearLength =  ( $scope.viewHeader.length)+3;
     });
	}
$scope.viewHeader();






    /*formtab in form start*/

    $scope.formtab = 1;
    $scope.formsetTab = function(formnewTab){
      if(formnewTab==2){
        $scope.viewTabDetail();
        console.log('$scope.viewTable: ',$scope.viewTable);
      }else if(formnewTab==1){
        $scope.loadPlumpExcelData1(116,$localStorage.firstvoltageFluc);
      }
      $scope.formtab = formnewTab;
    };

    $scope.isformSet = function(formtabNum){
      return $scope.formtab === formtabNum;
    };







  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

  }]
)}
)()
