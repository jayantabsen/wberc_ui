(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab44Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab44';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };
    //$scope.AllStations = ["Plan","Nonplan","Total"];
    $scope.AllStations = ["Plan","Nonplan"];



$scope.getDataByForm = function(uid,tab,data) {
		var result = [];
    result = data.vals;
    return result;
}
$scope.loadPlumpExcelData1 = function(formId,expName){

		var ApiEndPoints = '/f1_19aJExcel?form_id='+44+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId+'&expenditure='+expName;
      ApiServices.fetchFormData(ApiEndPoints,function(data){

            		$scope.subStations = data.data.vals;
            		$scope.subStationsData = data.data;
                console.log('expName: ',expName);
     });
	}
// $scope.loadAllExpanditure = function(formId){
// 	$scope.AllStations = [];
// 		var ApiEndPoints = '/dashboard/expanditure.json';
// 		ApiServices.expanditure(ApiEndPoints,function(data){
// 				$scope.AllExpanditure = data.data.vals;
//         $scope.firstExp=$scope.AllExpanditure[0].name;
//         $scope.loadPlumpExcelData1(44,$scope.firstExp);
// 		  });
// };
$scope.loadAllExpanditure = function(formId){
  // if(expName=="Plan"){
  //  $scope.loadPlumpExcelData1(44,val_plan);
  // }else if(expName=="Nonplan"){
  //  $scope.loadPlumpExcelData1(44,val_nonplan);
  // }else if(expName=="Total"){
  //  $scope.loadPlumpExcelData1(44,total);
  // }
   $scope.loadPlumpExcelData1(44,'val_plan');
};
$scope.loadAllExpanditure(44);
$scope.getSingleStations = function(Id,expName){
  console.log('expName ',expName);
  if(expName==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab44';
  	$scope.selectedItem = 44;
    if(expName=="Plan"){
  	   $scope.loadPlumpExcelData1(44,'val_plan');
    }else if(expName=="Nonplan"){
  	   $scope.loadPlumpExcelData1(44,'val_nonplan');
    }else if(expName=="Total"){
  	   $scope.loadPlumpExcelData1(44,'total');
    }
		$timeout( function(){
			$scope.is_show = 'no';
		}, 1000 );
  }



  $scope.saveStationData = function(Seloption,formId){
  	  var postData = {};
  	  var raw = $scope.subStationsData;
      var url = 'f1_19aDataUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };


  $scope.$on('updateTotal', function(evt, data) {
      $scope.totalExpenditure=data;
          console.log('sum Total Array',$scope.totalExpenditure);
          $scope.$apply();
  });

    $scope.loadHeader = function(){
    $rootScope.tableHeader44 = "";
      var ApiEndPoints = '/form_header1_19a?form_id='+44+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
            $scope.formHeader = data.data.header;
            $rootScope.tableHeader44 = data.data.header;
        });
    }
    $scope.loadHeader()

  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };


  }]
)}
)()
