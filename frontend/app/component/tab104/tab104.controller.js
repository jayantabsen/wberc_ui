(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab104Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab104';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data.vals;
      return result;
  }
  $scope.loadPlumpExcelData1 = function(formId,pyId){
     var ApiEndPoints = '/f4_2JExcel?form_id=104&py_id='+pyId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;
          		$scope.subStationsData = data.data;
              $localStorage.voltages =  data.data.voltages;
          		//  for(var jj=0;jj < data.data.vals.length;jj++){
              //          data.data.vals[jj]['valForStationType'];
          		// }
       });
  	}

  $scope.loadAllStations = function(formId){
  	$scope.Ensuringyears = [];
  		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.Ensuringyears = data.data.ensuing_petition_year;
          $scope.firstStn=$scope.Ensuringyears[0].py_id;
          console.log($scope.Ensuringyears);
        $scope.loadPlumpExcelData1(104,$scope.firstStn);
  		  });
  };
  $scope.loadAllStations(104);
  //$scope.loadPlumpExcelData1(31,2518);
  $scope.getSingleStations = function(Id,stationId){
    if(stationId==null){
    	return false;
    }
     $scope.is_show = 'yes';
    	$scope.selectedTab = 'tab104';
    	$scope.selectedItem = 104;
    	$scope.loadPlumpExcelData1(104,stationId);
  		$timeout( function(){
  			$scope.is_show = 'no';
  		}, 1000 );
    }

    $scope.grandTotal = function(){
     var mainraw = angular.copy($scope.subStationsData);
     var rawData = mainraw.vals;
     var sums = rawData.reduce(function(result,row,i){
       //row.pop();
       result[1] += parseInt(row[1]);
       result[2] += parseInt(row[2]);
       return result
       console.log('row: ',row);
       console.log('result: ',result);
       console.log('i: ',i);

         //return r;
       },['Total', 0, 0])
      // raw= 0;
          sums[3] = sums[2] + sums[1];
          $scope.grandTotalArr = sums;
           console.log(sums)
    }

  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f4_2DataUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           //console.log('data.message: ',data.message);
           $scope.updateMessage=data.message;
           $timeout(function() {
               $scope.updateMessage='';
          }, 3000);
        })
  };

  // $scope.ctrlFn= function(){
  //     var raw = angular.copy($scope.subStationsData);
  //     var calcArray = raw.vals;
  //     var result = calcArray.reduce(function(r,o){
  //       console.log('o',o);
  //   		  o.slice(1).forEach(function(v,i){
  //   			   r[i] = (r[i] || 0 ) + parseFloat(v);
  //       	});
  //     	return r;
  //   },[]);
  //   $scope.totalArray = result;
  //   $scope.$apply();
  // }
  $scope.$on('updateTotal', function(evt, data) {
      $scope.totalExpenditure=data;
          $scope.$apply();
  });

    $scope.loadHeader = function(){
    $rootScope.tableHeader104 = "";
      var ApiEndPoints = '/form_header4_ii?form_id='+104+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
            $scope.formHeader = data.data.header;
            $rootScope.tableHeader104 = data.data.header;
        });
    }
    $scope.loadHeader();





  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };



  }]
)}
)()
