(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab59Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab59';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



    $scope.getDataByForm = function(uid,tab,data) {
    		var result = [];
        result = data
        return result;
    }
  $scope.loadPlumpExcelData1 = function(formId,pyId){
      var ApiEndPoints = '/f_C_JExcel?ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId+'&py_id='+pyId+'&form_id='+formId;
     ApiServices.fetchFormData(ApiEndPoints,function(data){
         $scope.subStationsData = data.data;
         $scope.seasonId = data.data.season_id;
       });
  	}

  $scope.loadAlEnsuingYear = function(formId){
  	$scope.Ensuringyears = [];
  		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.Ensuringyears = data.data.ensuing_petition_year;
          $localStorage.firstPyid=$scope.Ensuringyears[0].py_id;
          //console.log($scope.Ensuringyears);
          $timeout(function() {
            $scope.loadPlumpExcelData1(59,$localStorage.firstPyid);
          }, 1000);
  		  });
  };
  $scope.loadAlEnsuingYear(59);
$scope.getSingleStations1 = function(Id,pyId){
  if(pyId==null){
    return false;
  }
   $scope.is_show = 'yes';
    $scope.selectedTab = 'tab78';
    $scope.selectedItem = 78;
    $localStorage.firstPyid=pyId;
      $scope.loadPlumpExcelData1(59,pyId);
    $timeout( function(){
      $scope.is_show = 'no';
    }, 500 );
  }

  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f_C_DataSave';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
               $scope.errorMessage='';
          }, 5000);
        })
  };

    /*formtab in form start*/

    $scope.formtab = 1;
    $scope.formsetTab = function(formnewTab){
      if(formnewTab==2){
        //$scope.viewTabDetail();
        console.log('$scope.viewTable: ',$scope.viewTable);
      }
      $scope.formtab = formnewTab;
    };

    $scope.isformSet = function(formtabNum){
      return $scope.formtab === formtabNum;
    };



    $scope.loadHeader = function(){
    $rootScope.tableHeader59 = "";
      var ApiEndPoints = '/form_headerForm_C?form_id='+59+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
            //$scope.formHeader = data.data.header;
            $rootScope.tableHeader59 = data.data.header;
        });
    }
    $scope.loadHeader()

  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };


  }]
)}
)()
