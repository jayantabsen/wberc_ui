(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab19Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab19';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data.vals;
      return result;
  }
  $scope.loadPlumpExcelData1 = function(formId,stnId,sourceId){
     var ApiEndPoints = '/f1_10aJExcel?form_id='+19+'&stn_id='+stnId+'&source_id='+sourceId+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;
          		$scope.subStationsData = data.data;
          		//  for(var jj=0;jj < data.data.vals.length;jj++){
              //          data.data.vals[jj]['valForStationType'];
          		// }
       });
  	}

  $scope.loadAllStations = function(formId){
  	$scope.AllStationsFuel = [];
  		var ApiEndPoints = '/stncombo?form_id='+19+'&util_id='+$localStorage.utilId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.AllStationsFuel = data.data.stations;
          $localStorage.firstStn=$scope.AllStationsFuel[0].id;
          $timeout( function(){
            $scope.loadAllSource(19);
        }, 500 );
  		  });
  };
  $scope.loadAllStations(19);
  $scope.loadAllSource = function(formId){
  	$scope.AllSource = [];
  		var ApiEndPoints = '/sourceListing1_6a?form_id='+9+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.AllSource = data.data.sources;
          $localStorage.firstSource=$scope.AllSource[0].id;
      		//$timeout( function(){
          $scope.loadPlumpExcelData1(19,$localStorage.firstStn,$localStorage.firstSource);
      //  }, 500 );
  		  });
  };




  $scope.getSingleStations = function(Id,stationId){
    if(stationId==null){
    	return false;
    }
     $scope.is_show = 'yes';
    	$scope.selectedTab = 'tab19';
    	$scope.selectedItem = 19;
    	$scope.loadPlumpExcelData1(19,stationId,$localStorage.firstYear);
      $localStorage.firstStn=stationId;
  		$timeout( function(){
  			$scope.is_show = 'no';
  		}, 1000 );
    }
    $scope.getSingleSource = function(Id,pyId){
      if(pyId==null){
      	return false;
      }
       $scope.is_show = 'yes';
      	$scope.selectedTab = 'tab19';
      	$scope.selectedItem = 19;
        $localStorage.firstYear=pyId;
      	$scope.loadPlumpExcelData1(19,$localStorage.firstStn,pyId);
    		$timeout( function(){
    			$scope.is_show = 'no';
    		}, 1000 );
      }

  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f1_10aUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };

      $scope.loadHeader = function(){
        $rootScope.tableHeader19 = "";
        var ApiEndPoints = '/form_header1_10a?form_id='+19+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
          ApiServices.fetchFormData(ApiEndPoints,function(data){
              $scope.formHeader = data.data.header;
              $rootScope.tableHeader19 = data.data.header;
          });
      }
      $scope.loadHeader()
  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };



  }]
)}
)()
