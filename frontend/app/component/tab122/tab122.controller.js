(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab122Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab122';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



$scope.getDataByForm = function(uid,tab,data) {
		var result = [];
    result = data['vals'];
    return result;
}
$scope.loadAllStations = function(formId){
	$scope.AllStations = [];
  //182.75.177.246:8181/wberc_v2/api/f5_ixConsumerType?form_id=122&ph_id=527&user_id=7
		var ApiEndPoints = '/f5_ixConsumerType?form_id='+formId+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
				$scope.AllStations = data.data.ctAl;
        $scope.firstStn=$scope.AllStations[0].consumer_type_id;
        $scope.loadPlumpExcelData1(122,$scope.firstStn);
		  });
};
$scope.loadAllStations(122);
$scope.loadPlumpExcelData1 = function(formId,consumerId){
    //  182.75.177.246:8181/wberc_v2/api/f5_ixJExcel?form_id=122&ph_id=527&consumer_type_id=2&user_id=7
		var ApiEndPoints = '/f5_ixJExcel?form_id='+formId+'&ph_id='+$localStorage.petitionId+'&consumer_type_id='+consumerId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
        		$scope.subStations = data.data.vals;
        		$scope.subStationsData = data.data;

     });
	}

//$scope.loadPlumpExcelData1($localStorage.formId);

$scope.loadHeader = function(){
  $rootScope.tableHeader122 = "";
  var ApiEndPoints = '/form_header5_ix?form_id='+122+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    ApiServices.fetchFormData(ApiEndPoints,function(data){
        //$scope.formHeader = data.data.header;
        $rootScope.tableHeader122 = data.data.header;
        $rootScope.tableHeader1221 = data.data.header;
        $rootScope.tableHeader1222 = data.data.header;
    });
}
$scope.loadHeader()

  $scope.saveStationData = function(Seloption,formId){
  	  var postData = {};
  	  var raw = $scope.subStationsData;
      var url = 'f5_ixUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };


  $scope.viewTabDetail = function(){
    //182.75.177.246:8181/wberc_v2/api/f5_ixJExcelAll?form_id=122&ph_id=527&user_id=7
    var ApiEndPoints = '/f5_ixJExcelAll?form_id='+122+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    ApiServices.fetchFormData(ApiEndPoints,function(data){
      $scope.viewTable = data.listData;
     });
  }


  $scope.formtab = 1;
  $scope.formsetTab = function(formnewTab){
    if(formnewTab==2){
      $scope.viewTabDetail();
    }else if(formnewTab==1){
    $scope.loadPlumpExcelData1(122,$scope.firstStn);
  }
    $scope.formtab = formnewTab;
  };

  $scope.isformSet = function(formtabNum){
    return $scope.formtab === formtabNum;
  };

  $scope.getSingleStations = function(Id,consumerId){
    if(consumerId==null){
      return false;
    }
      $scope.is_show = 'yes';
      $scope.selectedTab = 'tab122';
      $scope.selectedItem = 122;
      $scope.loadPlumpExcelData1(Id,consumerId);
      $timeout( function(){
        $scope.is_show = 'no';
      }, 500 );
    }







  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };


  }]
)}
)()
