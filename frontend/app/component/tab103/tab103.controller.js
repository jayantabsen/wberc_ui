(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab103Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab103';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



    $scope.getDataByForm = function(uid,tab,data) {
    		var result = [];
        //result = data.valAll[uid].valAll;
        result = data.tableValue;
        //alert(data);
        return result;
    }
  $scope.loadPlumpExcelData1 = function(formId,pyId){
    //182.75.177.246:8181/wberc_v2/api/f4_1JExcel?form_id=103&py_id=2430&user_id=6
    var ApiEndPoints = '/f4_1JExcel?form_id='+formId+'&py_id='+pyId+'&user_id='+$localStorage.userId;
     ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.subStationsData = [];
          $scope.subStations = data.data.vals;
          $scope.subStationsData = data.data;
       });
  	}
    $scope.loadAllStations = function(formId){
    	$scope.Ensuringyears = [];
    		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
    		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
    				$scope.Ensuringyears = data.data.ensuing_petition_year;
            $scope.firstStn=$scope.Ensuringyears[0].py_id;
            $scope.loadPlumpExcelData1(103,$scope.firstStn);
    		  });
    };
    $scope.loadAllStations(103);
    $scope.getSingleStations = function(Id,stationId){
      if(stationId==null){
      	return false;
      }
       $scope.is_show = 'yes';
      	$scope.selectedTab = 'tab103';
      	$scope.selectedItem = 103;
      	$scope.loadPlumpExcelData1(103,stationId);
      	console.log('stationId',stationId);
    		$timeout( function(){
    			$scope.is_show = 'no';
    		}, 1000 );
      }




  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f4_1DataUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);
        
        })
  };



    $scope.loadHeader = function(){
    $rootScope.tableHeader103 = "";
      var ApiEndPoints = '/form_header4_i?form_id='+103+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
            $scope.formHeader = data.data.header;
            $rootScope.tableHeader103 = data.data.header;
        });
    }
    $scope.loadHeader()








    /*formtab in form start*/

    $scope.formtab = 1;
    $scope.formsetTab = function(formnewTab){
      if(formnewTab==2){
        $scope.viewTabDetail();
        console.log('$scope.viewTable: ',$scope.viewTable);
      }
      $scope.formtab = formnewTab;
    };

    $scope.isformSet = function(formtabNum){
      return $scope.formtab === formtabNum;
    };







  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

  }]
)}
)()
