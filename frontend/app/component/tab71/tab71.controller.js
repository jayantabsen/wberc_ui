(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab71Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab71';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



$scope.getDataByForm = function(uid,tab,data) {
		var result = [];
    result = data['vals'];
    return result;
}
$scope.loadPlumpExcelData1 = function(formId){
  //182.75.177.246:8181/wberc_v2/api/fe_tJExcel?form_id=71&ph_id=561&user_id=5
		var ApiEndPoints = '/fe_tJExcel?form_id='+71+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
        		$scope.subStations = data.data.vals;
        		$scope.subStationsData = data.data;
        		 for(var jj=0;jj < data.data.vals.length;jj++){
                     data.data.vals[jj]['valForStationType'];
        		}
     });
	}

$scope.loadPlumpExcelData1($localStorage.formId);
$scope.loadHeader = function(){
$rootScope.tableHeader71 = "";
  //182.75.177.246:8181/wberc_v2/api/form_headerE_B?form_id=71&ph_id=550&user_id=7
  var ApiEndPoints = '/form_headerE_T?form_id='+71+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    ApiServices.fetchFormData(ApiEndPoints,function(data){
        $rootScope.tableHeader71 = data.data.header;
    });
}
$scope.loadHeader()


  $scope.saveStationData = function(Seloption,formId){
  	  var postData = {};
  	  var raw = $scope.subStationsData;
      var url = 'fe_tUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
               $scope.errorMessage='';
          }, 5000);
        })
  };

  /*tab in form start*/

    $scope.tab = 1;
    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };


  }]
)}
)()
