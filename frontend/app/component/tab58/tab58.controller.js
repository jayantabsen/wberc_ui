(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab58Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab58';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data.vals;
      return result;
  }
  $scope.loadPlumpExcelData1 = function(formId,stnId,pyId){
    //182.75.177.246:8181/wberc_v2/api/f_B_JExcel?ph_id=130&user_id=2&py_id=1054&form_id=58
     var ApiEndPoints = '/f_B_JExcel?ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId+'&py_id='+pyId+'&form_id=58';
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;
          		$scope.subStationsData = data.data;
          		 for(var jj=0;jj < data.data.vals.length;jj++){
                       data.data.vals[jj]['valForStationType'];
          		}
       });
  	}

  $scope.loadAllStations = function(formId){
  	$scope.Ensuringyears = [];
      //182.75.177.246:8181/wberc_v2/api/stncombo?form_id=5&util_id=1
  		var ApiEndPoints = '/stncombo?form_id='+58+'&util_id='+$localStorage.utilId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.AllStationsFuel = data.data.stations;
          $localStorage.firstStn=$scope.AllStationsFuel[0].id;
          console.log($scope.AllStationsFuel);
  		  });
      // $scope.loadPlumpExcelData1(57,$localStorage.firstStn,$localStorage.firstYear);
  };
  $scope.loadAllStations(58);
  $scope.loadAllYears = function(formId){
  	$scope.Ensuringyearsforeign = [];
  		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.Ensuringyearsforeign = data.data.ensuing_petition_year;
          $localStorage.firstYear=$scope.Ensuringyearsforeign[0].py_id;
          console.log($scope.Ensuringyearsforeign);
          $scope.loadPlumpExcelData1(58,$localStorage.firstStn,$localStorage.firstYear);
  		});
  };
  $scope.loadAllYears(58);




  //$scope.loadPlumpExcelData1(31,2518);
  $scope.getSingleStations = function(Id,stationId){
    if(stationId==null){
    	return false;
    }
     $scope.is_show = 'yes';
    	$scope.selectedTab = 'tab58';
    	$scope.selectedItem = 58;
    	$scope.loadPlumpExcelData1(58,stationId,$localStorage.firstYear);
      $localStorage.firstStn=stationId;
  		$timeout( function(){
  			$scope.is_show = 'no';
  		}, 1000 );
    }
    $scope.getSingleYears = function(Id,pyId){
      // alert('pyId',pyId);
      if(pyId==null){
      	return false;
      }
       $scope.is_show = 'yes';
      	$scope.selectedTab = 'tab58';
      	$scope.selectedItem = 58;
        $localStorage.firstYear=pyId;
      	$scope.loadPlumpExcelData1(58,$localStorage.firstStn,pyId);
    		$timeout( function(){
    			$scope.is_show = 'no';
    		}, 1000 );
      }

  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f_B_DataSave';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
               $scope.errorMessage='';
          }, 5000);
        })
  };

      $scope.loadHeader = function(){
      $rootScope.tableHeader58 = "";
        var ApiEndPoints = '/form_headerForm_B?form_id='+58+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
          ApiServices.fetchFormData(ApiEndPoints,function(data){
              //$scope.formHeader = data.data.header;
              $rootScope.tableHeader58 = data.data.header;
          });
      }
      $scope.loadHeader()
  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };


  }]
)}
)()
