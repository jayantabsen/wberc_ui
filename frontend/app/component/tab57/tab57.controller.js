(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab57Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab57';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data.vals;
      return result;
  }
  $scope.loadPlumpExcelData1 = function(formId,stnId,pyId){
    //182.75.177.246:8181/wberc_v2/api/f1_23JExcel?stn_id=5&ph_id=365&form_id=51&user_id=2
    //182.75.177.246:8181/wberc_v2/api/f_A_JExcel?stn_id=1&py_id=512&form_id=57&user_id=2
     var ApiEndPoints = '/f_A_JExcel?stn_id='+stnId+'&py_id='+pyId+'&form_id=57'+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;
          		$scope.subStationsData = data.data;
          		 for(var jj=0;jj < data.data.vals.length;jj++){
                       data.data.vals[jj]['valForStationType'];
          		}
       });
  	}

  $scope.loadAllStations = function(formId){
  	$scope.Ensuringyears = [];
      //182.75.177.246:8181/wberc_v2/api/stncombo?form_id=5&util_id=1
  		var ApiEndPoints = '/stncombo?form_id='+57+'&util_id='+$localStorage.utilId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.AllStationsFuel = data.data.stations;
          $localStorage.firstStn=$scope.AllStationsFuel[0].id;
          console.log($scope.AllStationsFuel);
  		  });
      // $scope.loadPlumpExcelData1(57,$localStorage.firstStn,$localStorage.firstYear);
  };
  $scope.loadAllStations(57);
  $scope.loadAllYears = function(formId){
  	$scope.Ensuringyearsforeign = [];
  		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.Ensuringyearsforeign = data.data.ensuing_petition_year;
          $localStorage.firstYear=$scope.Ensuringyearsforeign[0].py_id;
          console.log($scope.Ensuringyearsforeign);
          $scope.loadPlumpExcelData1(57,$localStorage.firstStn,$localStorage.firstYear);
  		});
  };
  $scope.loadAllYears(57);




  //$scope.loadPlumpExcelData1(31,2518);
  $scope.getSingleStations = function(Id,stationId){
    if(stationId==null){
    	return false;
    }
     $scope.is_show = 'yes';
    	$scope.selectedTab = 'tab57';
    	$scope.selectedItem = 57;
    	$scope.loadPlumpExcelData1(57,stationId,$localStorage.firstYear);
      $localStorage.firstStn=stationId;
  		$timeout( function(){
  			$scope.is_show = 'no';
  		}, 1000 );
    }
    $scope.getSingleYears = function(Id,pyId){
      // alert('pyId',pyId);
      if(pyId==null){
      	return false;
      }
       $scope.is_show = 'yes';
      	$scope.selectedTab = 'tab57';
      	$scope.selectedItem = 57;
        $localStorage.firstYear=pyId;
      	$scope.loadPlumpExcelData1(57,$localStorage.firstStn,pyId);
    		$timeout( function(){
    			$scope.is_show = 'no';
    		}, 1000 );
      }

  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f_A_DataSave';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
               $scope.errorMessage='';
          }, 5000);
        })
  };

      $scope.loadHeader = function(){
      $rootScope.tableHeader57 = "";
        var ApiEndPoints = '/form_headerForm_A?form_id='+57+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
          ApiServices.fetchFormData(ApiEndPoints,function(data){
              $scope.formHeader = data.data.header;
              $rootScope.tableHeader57 = data.data.header;
          });
      }
      $scope.loadHeader()
  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };


  $scope.getFormComment = function(){
		var ApiEndPoints = '/get_form_comment?form_id='+57+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    ApiServices.getFormComment(ApiEndPoints,function(data){
        $scope.commentList = data.data.comments;
   });
  };
  $scope.getFormComment();

  $scope.daleteComment = function(commentId){
      var url = "http://182.75.177.246:8181/wberc_v2/api/delete_1_comment?arr_comment_id="+commentId;
      ApiServices.getFileDownload(url,function(data){
        if(data.status==1){
          $scope.getFormComment();
        }
      });
  };
  $scope.submit_comment_Form = function(){
      var url = "http://182.75.177.246:8181/wberc_v2/api/insert_1_comment";
      var formdata = new FormData();
      formdata.append('comment', $scope.comments.comment);
      formdata.append('form_id',57);
      formdata.append('ph_id', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
      formdata.append('user_id', parseInt(localStorage.getItem("ngStorage-userId").replace(/['"]+/g, '')));
      ApiServices.PostRequest(url, formdata,function(data){
        alert(data.message);
        $scope.getFormComment();
        $scope.comments.comment=" ";
      });
  };


    $scope.loadGlobalFiles = function(){
       $scope.globalfiles = [];
       var ApiEndPoints = '/file_listing?ph_id='+$localStorage.petitionId+'&form_id='+57;
       ApiServices.GetGlobalFiles(ApiEndPoints,function(data){
         $scope.globalfiles = data;
        });
    };
    $scope.docIdFn = function(docId){
        var url = "http://182.75.177.246:8181/wberc_v2/api/file_delete_by_id?docId="+docId;
        ApiServices.getFileDownload(url,function(data){
          if(data.status==1){
            $scope.loadGlobalFiles();
          }
        });
    };
    $scope.submit_attachment_Form = function(){
      if($scope.attachment.file != undefined){
        var url = "http://182.75.177.246:8181/wberc_v2/api/file_upload_v2";
        var formdata = new FormData();
        formdata.append('file', $scope.attachment.file);
        formdata.append('docName', $scope.attachment.file.name);
        formdata.append('description', $scope.attachment.comment);
        formdata.append('AttachmentType', $scope.attachment.attachment_type_desc)
        formdata.append('petitionHeaderId', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
        formdata.append('formID', 57);
        formdata.append('utilid', parseInt(localStorage.getItem('ngStorage-utilId').replace(/['"]+/g, '')));
        formdata.append('module', 'ARR');
        ApiServices.PostGlobalFileUpload(url, formdata,function(data){
          alert(data);
          $scope.loadGlobalFiles();
          $uibModalInstance.dismiss('cancel');
        });
      }else {
        alert("Please select file to attach..");
      }
      $scope.attachment = {
        'comment':'',
        'form_ref':'',
        'file':'',
      };
    };

    $scope.loadGlobalFiles();


  }]
)}
)()
