(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab23Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'ta23';
  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data.vals;
      return result;
  }
  $scope.$on('loadPlumpExcelData1', function(formId) {
    $scope.loadPlumpExcelData1(formId);
  });

  $scope.loadPlumpExcelData1 = function(formId,pyId){
     var ApiEndPoints = '/f1_13JExcel?ph_id='+$localStorage.petitionId+'&form_id=23'+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;
          		$scope.subStationsData = data.data;
       });
  	}

  $scope.loadPlumpExcelData1(23);
  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f1_13Update';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };

  $scope.loadHeader = function(){
    $rootScope.tableHeader23 = "";
    var ApiEndPoints = '/form_header1_13?form_id='+23+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.formHeader = data.data.header;
          $rootScope.tableHeader23 = data.data.header;
      });
  }
  $scope.loadHeader()



  /*tab in form start*/

    $scope.tab = 1;
    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    }

    $scope.grandTotal = function(){
     var mainraw = angular.copy($scope.subStationsData);
     var rawData = mainraw.vals;
       console.log('rawData: ',rawData.pop());
     var sums = rawData.reduce(function(result,row,i){
       //row.pop();
       result[1] += parseInt(row[1]);
       result[2] += parseInt(row[2]);
       return result
       console.log('row: ',row);
       console.log('result: ',result);
       console.log('i: ',i);

         //return r;
       },['Total', 0, 0])
      // raw= 0;
          sums[3] = sums[2] + sums[1];
          $scope.grandTotalArr = sums;
           console.log(sums)
    }
    

    }]
  )}
)()
