(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab17Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab17';



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data['vals'][uid]['usage'];
      return result;
  }
  $scope.loadseasonalData = function(formId,licenseeId){
    //182.75.177.246:8181/wberc_v2/api/f1_9cJExcel?form_id=17&ph_id=396&licensee_id=1&user_id=2
    var ApiEndPoints = '/f1_9cJExcel?form_id='+17+'&ph_id='+$localStorage.petitionId+'&licensee_id='+licenseeId+'&user_id='+$localStorage.userId;
    $scope.subStationsData = [];

      var verison = 1;
         ApiServices.fetchFormData(ApiEndPoints,function(data){
  		    $scope.subStationsData = [];
  			$scope.subStations = data.data.vals;
  			$scope.subStationsData = data.data;
  	  });
};
// $scope.loadseasonalData(17,1);

$scope.loadAllLicensee = function(formId){
	$scope.AllStations = [];
    //182.75.177.246:8181/wberc_v2/api/f1_9cLicensee?form_id=17&ph_id=396&user_id=2
		var ApiEndPoints = '/f1_9cLicensee?form_id='+17+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
				$scope.AllLicensee = data.data.licensee;
        $scope.firstLicensee=$scope.AllLicensee[0].id;
        $scope.loadseasonalData(17,$scope.firstLicensee);
		  });
};
$scope.loadAllLicensee(17);
$scope.getSingleStations = function(Id,licenseeId){
  if(licenseeId==null){
  	return false;
  }
    alert('form 1.9c',licenseeId);
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab17';
  	$scope.selectedItem = 17;
  	$scope.loadseasonalData(17,licenseeId);
		$timeout( function(){
			$scope.is_show = 'no';
		}, 1000 );
  }


 // $scope.grandTotal = function(){
 //  var raw = $scope.subStationsData.vals;
 //  $scope.grandTotalArr = raw.reduce(function(r,o){
 //      console.log('o.usage: ', o.usage.pop());
 //      o.usage.forEach(function(a){
 //        //console.log('a.slice(1): ', a.slice(1));
 //            a.slice(1).forEach(function(v,i){
 //               r[i] = (r[i] || 0 ) + parseFloat(v);
 //            });
 //        });
 //      return r;
 //    },[])
 //    console.log($scope.grandTotalArr);
 // }
 $scope.grandTotal = function(){
  var mainraw = angular.copy($scope.subStationsData);
  var raw = mainraw.vals;
  $scope.grandTotalArr = raw.reduce(function(r,o){
      console.log('o.usage: ', o.usage.pop());
      o.usage.forEach(function(a){
            a.slice(1).forEach(function(v,i){
               r[i] = (r[i] || 0 ) + parseFloat(v);
            });
        });
      return r;
    },[])
    raw= 0;
 }
// $scope.ctrlFn= function(){
//     var raw = angular.copy($scope.subStationsData);
//     var calcArray = raw.vals;
//     console.log('raw.vals: ',raw.vals);
//     var result = calcArray.reduce(function(r,o){
//       console.log('o.usage: ', o.usage.pop());
//     	o.usage.forEach(function(a){
//         console.log('a.slice(1): ', a.slice(1));
//       		  a.slice(1).forEach(function(v,i){
//       			   r[i] = (r[i] || 0 ) + parseFloat(v);
//           	});
//         });
//     	return r;
//   },[]);
//   console.log(result);
//   $scope.totalArray = result;
//   $scope.$apply();
// }

  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
   	  var raw = $scope.subStationsData;
           var url = 'f1_9cUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };

  $scope.loadGenerationPowerStation = function(formId,stationId){
  		$scope.subStationsData = [];
        		var ApiEndPoints = '/f1_9cSumJExcel?form_id='+17+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    				$scope.stationheaderInfo = {
    					'stationName':'GROSS GENERATION OF POWER STATION',
    					'capacity':'',
    					'unit':'MU',
    				};
    				var verison = 2;
    				ApiServices.fetchFormData(ApiEndPoints,function(data){
    					 $scope.totalStationsData = data.data;
    				});
    }
$scope.loadGenerationPowerStation();

$scope.$on('updateTotal', function(evt, data) {
      $scope.totalExpenditure=data;
          console.log('sum Total Array',$scope.totalExpenditure);
          $scope.$apply();
  });

    //
    // $scope.loadHeader = function(){
    //   var ApiEndPoints = '/form_header1_9c?form_id='+17+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    //     ApiServices.fetchFormData(ApiEndPoints,function(data){
    //         $scope.formHeader = data.data.header;
    //         $localStorage.tableHeader = data.data.header;
    //     });
    // }
    // $scope.loadHeader()

    $scope.loadHeader = function(){
      $rootScope.tableHeader17 = "";
      var ApiEndPoints = '/form_header1_9c?form_id='+17+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
            $scope.formHeader = data.data.header;
            $rootScope.tableHeader17 = data.data.header;
        });
    }
    $scope.loadHeader()

  /*tab in form start*/
  $scope.tab=1;
    $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };
  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

  }]
)}
)()
