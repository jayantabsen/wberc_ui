(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab125Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab125';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data['vals'];
      return result;
  }
  $scope.projectListFn = function(formId){
     var ApiEndPoints = '/f8project_list?form_id='+formId+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.projectList = data.data.vals;
              if($scope.projectList.length==0){
                $scope.showHidedata = true;
              }else if($scope.projectList.length>0){
                $scope.showHidedata = false;
              }
       });
  	}
  $scope.projectListFn(125);

$scope.editProject = function(data){
    var dataLength = (data.length)-1;
      console.log('project data id: ', data[dataLength]);
      var arrdf8a_id = data[dataLength];
      $localStorage.arrdf8a_id = data[dataLength];
      var comboBox = '/f8particulars_for_combo?form_id='+125+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      var ApiEndPoints = '/f8project_details?form_id='+125+'&ph_id='+$localStorage.petitionId+'&arrdf8a_id='+arrdf8a_id+'&user_id='+$localStorage.userId;
         ApiServices.fetchFormData(ApiEndPoints,function(data){
               $scope.projectUpdate = data.data;
               $scope.projectUpdate.package_desc=data.data.package_desc;
               $scope.projectUpdate.project_desc=data.data.project_desc;
               $scope.projectUpdate.reference_details=data.data.reference_details;
               $scope.projectUpdate.reference_no=data.data.reference_no;
           		$scope.subStations = data.data.vals;
           		$scope.subStationsData = data.data;
        });
        ApiServices.fetchFormData(comboBox,function(data){
              $scope.projectCombo = data.data.f8dfcAl;
              $localStorage.projectCombo = data.data.f8dfcAl;
       });
}
$scope.addProject = function(){
      var comboBox = '/f8particulars_for_combo?form_id='+125+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      var ApiEndPoints = '/f8project_details?form_id='+125+'&ph_id='+$localStorage.petitionId+'&arrdf8a_id='+0+'&user_id='+$localStorage.userId;
         ApiServices.fetchFormData(ApiEndPoints,function(data){
               $scope.projectUpdate = data.data;
               $scope.projectUpdate.package_desc=data.data.package_desc;
               $scope.projectUpdate.project_desc=data.data.project_desc;
               $scope.projectUpdate.reference_details=data.data.reference_details;
               $scope.projectUpdate.reference_no=data.data.reference_no;
             	 $scope.subStations = data.data.vals;
             	 $scope.subStationsData = data.data;
        });
        ApiServices.fetchFormData(comboBox,function(data){
              $scope.projectCombo = data.data.f8dfcAl;
              $localStorage.projectCombo = data.data.f8dfcAl;
       });
}

  $scope.loadHeader = function(){
  $rootScope.tableHeader125 = "";
    var ApiEndPoints = '/form_headerF8?form_id='+125+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          $rootScope.tableHeader125 = data.data.header;
      });
  }
  $scope.loadHeader();

$scope.errorMessage = "";
$scope.addModSuccessMessage = "";
$scope.addModErrorMessage = "";
$scope.errorMessage = "";
$scope.errorMsgShow = false;
$scope.successMsgShow = false;
$scope.partNotPresent = false;
$scope.descNotPresent = false;
$scope.amountNotPresent = false;
$scope.addModSuccessMsgShow = false;
$scope.addModSuccessErrorMsgShow = false;
$scope.descExdPresent = false;
$scope.confirmPetition = function(formId){
  console.log("$scope.subStationsData",$scope.subStationsData);
  if($scope.projectUpdate.package_desc != " " && $scope.projectUpdate.package_desc != "" && $scope.projectUpdate.package_desc != undefined){
    $scope.errorMsgShow = false;
    if($scope.projectUpdate.project_desc != " " && $scope.projectUpdate.project_desc != "" && $scope.projectUpdate.project_desc != undefined){
      $scope.errorMsgShow = false;
      if($scope.projectUpdate.reference_details != " " && $scope.projectUpdate.reference_details != "" && $scope.projectUpdate.reference_details != undefined){
        $scope.errorMsgShow = false;
        if($scope.projectUpdate.reference_no != " " && $scope.projectUpdate.reference_no != "" && $scope.projectUpdate.reference_no != undefined){
          $scope.errorMsgShow = false;
          for(var i=0;i<$scope.subStationsData.vals.length;i++){
            if($scope.subStationsData.vals[i][0] == "" || $scope.subStationsData.vals[i][0] == " " || $scope.subStationsData.vals[i][0] == "0"){
                $scope.partNotPresent = true;
                break;
            }
            else{
              $scope.partNotPresent = false;
            }
          }
          for(var i=0;i<$scope.subStationsData.vals.length;i++){
            if($scope.subStationsData.vals[i][1] == "" || $scope.subStationsData.vals[i][1] == " "){
                $scope.descNotPresent = true;
                break;
            }
            else{
              $scope.descNotPresent = false;
            }
          }
          for(var i=0;i<$scope.subStationsData.vals.length;i++){
            if($scope.subStationsData.vals[i][2] == "" || $scope.subStationsData.vals[i][2] == " "){
                $scope.amountNotPresent = true;
                break;
            }
            else{
              $scope.amountNotPresent = false;
            }
          }
          for(var i=0;i<$scope.subStationsData.vals.length;i++){
            var str = $scope.subStationsData.vals[i][1];
            if(str.length >2000){
                $scope.descExdPresent = true;
                break;
            }
            else{
              $scope.descExdPresent = false;
            }
          }

          if($scope.partNotPresent == true){
            $scope.errorMsgShow = true;
            $scope.errorMessage = "Please Select One Particular";
            $timeout(function() {
              $scope.errorMsgShow = false;
            }, 2000);
          }
          else{
            $scope.errorMsgShow = true;
            if($scope.descNotPresent == true){
              $scope.errorMsgShow = true;
              $scope.errorMessage = "Please Enter Particular Description";
              $timeout(function() {
                $scope.errorMsgShow = false;
              }, 2000);
            }
            else{
              $scope.errorMsgShow = false;
              if($scope.amountNotPresent == true){
                $scope.errorMsgShow = true;
                $scope.errorMessage = "Please Enter Amount";
                $timeout(function() {
                  $scope.errorMsgShow = false;
                }, 2000);
              }
              else{
                $scope.errorMsgShow = false;
                if($scope.descExdPresent == true){
                  $scope.errorMsgShow = true;
                  $scope.errorMessage = "Particular Description should not exceed 2000 characters";
                  $timeout(function() {
                    $scope.errorMsgShow = false;
                  }, 2000);
                }
                else{
                  var raw = $scope.subStationsData;
                       var url = 'f8Update';
                  ApiServices.setSubstaionData1({
                      message:'',
                      data:raw,
                      status:1,
                      url:url,
                    }, function(data){
                      if(data.status == '1'){
                        $scope.addModSuccessMessage=data.message;
                        $('#addForm').modal('hide');
                        $('#updateForm').modal('hide');
                        $scope.projectListFn(125);
                        $scope.addModSuccessMsgShow = true;
                        $timeout(function() {
                            $scope.addModSuccessMsgShow = false;
                            $scope.addModSuccessMessage='';
                       }, 3000);
                      }
                      else{
                        $scope.addModErrorMessage=data.message;
                        $('#addForm').modal('hide');
                        $('#updateForm').modal('hide');
                        $scope.projectListFn(125);
                        $scope.addModSuccessErrorMsgShow = true;
                        $timeout(function() {
                            $scope.addModSuccessErrorMsgShow = false;
                            $scope.addModErrorMessage='';
                       }, 3000);
                      }
                    })
                }
              }
            }
          }

        }
        else{
          $scope.errorMsgShow = true;
          $scope.errorMessage = "Please Enter Reference No";
          $timeout(function() {
            $scope.errorMsgShow = false;
          }, 2000);
        }
      }
      else{
        $scope.errorMsgShow = true;
        $scope.errorMessage = "Please Enter Reference Details";
        $timeout(function() {
          $scope.errorMsgShow = false;
        }, 2000);
      }
    }
    else{
        $scope.errorMsgShow = true;
        $scope.errorMessage = "Please Enter Project Description";
        $timeout(function() {
          $scope.errorMsgShow = false;
        }, 2000);
    }
  }
  else{
    $scope.errorMsgShow = true;
    $scope.errorMessage = "Please Enter Package Description";
    $timeout(function() {
      $scope.errorMsgShow = false;
    }, 2000);
  }

};

$scope.deleteProject = function(){
  //182.75.177.246:8181/wberc_v2/api/f8DeleteProject?form_id=125&ph_id=584&arrdf8a_id=4&user_id=6
  var ApiEndPoints = '/f8DeleteProject?form_id='+125+'&ph_id='+$localStorage.petitionId+'&arrdf8a_id='+$localStorage.arrdf8a_id+'&user_id='+$localStorage.userId;
     ApiServices.fetchFormData(ApiEndPoints,function(data){
           $scope.deleteProjectMessage = data;
           $scope.projectUpdate.package_desc="";
           $scope.projectUpdate.project_desc="";
           $scope.projectUpdate.reference_details="";
           $scope.projectUpdate.reference_no="";
           $scope.projectListFn(125);
           console.log('$scope.deleteProjectMessage: ',$scope.deleteProjectMessage);
    });

}

  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };




  }]
)}
)()
