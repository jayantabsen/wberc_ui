(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab16Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab16';
    $scope.searchName = "";



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data['vals'][uid]['usage'];
      return result;
  }
  $scope.loadseasonalData = function(formId){
    var ApiEndPoints = '/f1_9bJExcelFirstCall?form_id='+formId+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    $scope.subStationsData = [];
      var verison = 1;
         ApiServices.fetchFormData(ApiEndPoints,function(data){
  		    $scope.subStationsData = [];
  			$scope.subStations = data.data.vals;
  			$scope.subStationsData = data.data;
  			$scope.subStationsDataX = data.data;
        console.log('$scope.loadseasonalData: ',$scope.subStationsData);
  	  });
};
  $scope.loadseasonalData(16);

 $scope.grandTotal = function(){
  var mainraw = angular.copy($scope.subStationsData);
  var raw = mainraw.vals;
  $scope.grandTotalArr = raw.reduce(function(r,o){
      console.log('o.usage: ', o.usage.pop());
      o.usage.forEach(function(a){
            a.slice(1).forEach(function(v,i){
               r[i] = (r[i] || 0 ) + parseFloat(v);
            });
        });
      return r;
    },[])
    raw= 0;
 }
$scope.searchTable = function(name){
var ApiEndPoints = '/f1_9bJExcelSearchCall?form_id='+formId+'&ph_id='+$localStorage.petitionId+'&consumer_desc='+name+'&user_id='+$localStorage.userId;
     ApiServices.fetchFormData(ApiEndPoints,function(data){
  //  $scope.subStations = data.data.vals;
    $scope.subStationsData = data.data;
    if($scope.subStationsData==undefined || $scope.subStationsData==null){
      $scope.subStationsData = $scope.subStationsDataX;
    }
    //  console.log('subStationsData: ',data.status);
    //  if(data.stauts==1){
    //    $scope.consumerExist = true;
    //    console.log('consumerExist: ',data.stauts);
    // }else if(data.stauts==0){
    //      $scope.consumerExist = false;
    //      console.log('consumerExist: ',$scope.subStationsDataX);
    //        $scope.loadseasonalData(16);
    //  }
      // $scope.subStations = data.data.vals;
      // $scope.subStationsData = data.data;
      // if(data.stauts==0){
      //   $scope.consumerExist = false;
      //   console.log('consumerExist: ',data.data.stauts);
      //     $scope.loadseasonalData(16);
      // }else{
      //     $scope.consumerExist = true;
      //     console.log('consumerExist: ',data.data.stauts);
      // }
      console.log('$scope.subStationsData: ',$scope.subStationsData);
  });
}
  $scope.consumerNameReq = false;

 $scope.saveStationData = function(formId){
    var postData = {};
     //var mainraw = angular.copy($scope.subStationsData);
     var raw = $scope.subStationsData;
     raw.consumer_desc = $scope.searchName;
     //console.log('raw: ',mainraw);
     console.log('$scope.searchName: ',raw);
     var url = 'f1_9bInsertUpdate';
     if($scope.searchName!=''){
       $scope.consumerNameReq = false;
       ApiServices.setSubstaionData1({
           message:'',
           data:raw,
           status:1,
           url:url,
         }, function(data){
            if(data.status==0){
              $scope.errorMessage=data.message;
            }
            if(data.status==1){
            $scope.updateMessage=data.message;
            }
            $timeout(function() {
                $scope.updateMessage='';
                    $scope.errorMessage='';
           }, 5000);
         })
     }else{
         //$scope.errorMessage='Please enter consumer name';
       $scope.consumerNameReq = true;
     }
 };

 $scope.grandTotal = function(){
  var mainraw = angular.copy($scope.subStationsData);
  var raw = mainraw.vals;
  $scope.grandTotalArr = raw.reduce(function(r,o){
       o.usage.pop();
      o.usage.forEach(function(a){
            a.slice(1).forEach(function(v,i){
               r[i] = (r[i] || 0 ) + parseFloat(v);
            });
        });
      return r;
    },[])
    raw= 0;
 }

$scope.$on('updateTotal', function(evt, data) {
      $scope.totalExpenditure=data;
          console.log('sum Total Array',$scope.totalExpenditure);
          $scope.$apply();
  });


    $scope.loadHeader = function(){
      $rootScope.tableHeader16 = "";
      var ApiEndPoints = '/form_header1_9b?form_id='+16+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          //  $scope.formHeader = data.data.header;
            $rootScope.tableHeader16 = data.data.header;
        });
    }
    $scope.loadHeader()

$scope.viewTabDetail = function(){
  var ApiEndPoints = '/f1_9bConsumerComboService?form_id='+16+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
  ApiServices.fetchFormData(ApiEndPoints,function(data){
     $scope.viewTable = data.data.consumers;
   });
}
$scope.getSingleStations = function(Id){
  console.log('getSingleStations:', Id);
  $scope.searchTable(Id);
}

$scope.formtab = 1;
$scope.formsetTab = function(formnewTab){
  if(formnewTab==2){
    $scope.viewTabDetail();
  }
  $scope.formtab = formnewTab;
};

$scope.isformSet = function(formtabNum){
  return $scope.formtab === formtabNum;
};







  /*tab in form start*/
  $scope.tab=1;
    $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };
  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };


  // $scope.ctrlFn= function(){
  //     var raw = angular.copy($scope.subStationsData);
  //     var calcArray = raw.vals;
  //     var result = calcArray.reduce(function(r,o){
  //     	o.usage.forEach(function(a){
  //       		  a.slice(1).forEach(function(v,i){
  //       			   r[i] = (r[i] || 0 ) + parseFloat(v);
  //           	});
  //         });
  //     	return r;
  //   },[]);
  //   $scope.totalArray = result;
  //   $scope.$apply();
  // }
  //




  }]
)}
)()
