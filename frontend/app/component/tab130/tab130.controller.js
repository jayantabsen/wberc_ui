(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab130Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab130';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data.vals;
      return result;
  }
  $scope.loadPlumpExcelData1 = function(formId,stnId){
    //182.75.177.246:8181/wberc_v2/api/fP_D1JExcel?form_id=129&ph_id=174&stn_id=1&user_id=2
     var ApiEndPoints = '/fP_D2JExcel?form_id=130'+'&ph_id='+$localStorage.petitionId+'&stn_id='+stnId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;
          		$scope.subStationsData = data.data;
          		//  for(var jj=0;jj < data.data.vals.length;jj++){
              //          data.data.vals[jj]['valForStationType'];
          		// }
       });
  	}

  $scope.loadAllStations = function(formId){
  	$scope.Ensuringyears = [];
      //182.75.177.246:8181/wberc_v2/api/stncombo?form_id=5&util_id=1
  		var ApiEndPoints = '/stncombo?form_id='+130+'&util_id='+$localStorage.utilId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.AllStationsFuel = data.data.stations;
          $scope.firstStn=$scope.AllStationsFuel[0].id;
          console.log($scope.AllStationsFuel);
         $scope.loadPlumpExcelData1(130,$scope.firstStn);
  		  });
  };
  $scope.loadAllStations(130);





  //$scope.loadPlumpExcelData1(31,2518);
  $scope.getSingleStations = function(Id,stationId){
    if(stationId==null){
    	return false;
    }
     $scope.is_show = 'yes';
    	$scope.selectedTab = 'tab130';
    	$scope.selectedItem = 130;
    	$scope.loadPlumpExcelData1(130,stationId,$localStorage.firstYear);
      $localStorage.firstStn=stationId;
  		$timeout( function(){
  			$scope.is_show = 'no';
  		}, 1000 );
    }
    $scope.getSingleYears = function(Id,pyId){
      // alert('pyId',pyId);
      if(pyId==null){
      	return false;
      }
       $scope.is_show = 'yes';
      	$scope.selectedTab = '130';
      	$scope.selectedItem = 130;
        $localStorage.firstYear=pyId;
      	$scope.loadPlumpExcelData1(130,$localStorage.firstStn,pyId);
    		$timeout( function(){
    			$scope.is_show = 'no';
    		}, 1000 );
      }

  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'fP_D2Update';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };

  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };



  }]
)}
)()
