(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab73Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab73';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



    $scope.getDataByForm = function(uid,tab,data) {
    		var result = [];
        //result = data.valAll[uid].valAll;
        result = data.valAll;
        //alert(data);
        return result;
    }
  $scope.loadPlumpExcelData1 = function(formId,vId,conClassId,conTypeId){
    var ApiEndPoints = '/f2_1JExcel?ph_id='+$localStorage.petitionId+'&form_id='+73+'&user_id='+$localStorage.userId+'&consumer_class_id='+conClassId+'&consumer_type_id='+conTypeId+'&voltage_level_id='+vId+'&season=Summer';
     ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.subStationsData = [];
          $scope.subStations = data.data.valAll;
          $scope.subStationsData = data.data;
       });
  	}



    //$scope.loadPlumpExcelData1(31,2518);

    $scope.loadConsumerType = function(formId){
    	$scope.ConsumerTypes = [];
    		var ApiEndPoints = '/consumerTypesCombo';
    		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
    				$scope.ConsumerTypes = data.data.consumer_types;
            $localStorage.firstConsumerId=$scope.ConsumerTypes[0].consumer_type_id;
            console.log('ConsumerTypes',data.data);
            $scope.loadConsumerTypeSub1(4,$localStorage.firstConsumerId);
    		  });
    };
    $scope.loadConsumerType(73);

      $scope.loadConsumerTypeSub1 = function(formId,sub_id_1){
      	$scope.consumerClassList = [];
      		var ApiEndPoints = '/consumerClassNVoltageLevelCombo?consumer_type_id='+sub_id_1;
      		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
            $scope.consumer = data.data;
      				$scope.consumerClassList = data.data.consumerClassList.consumer_classes;
              $scope.voltageLevelList = data.data.voltageLevelList.voltage_levels;
              $localStorage.firstClass=$scope.consumerClassList[0].consumer_class_id;
      				$localStorage.firstvoltage = $scope.voltageLevelList[0].voltage_level_id;
              console.log($scope.consumerClassList);
      		  });
      };

$scope.getSingleStations2 = function(Id,consumerId){
      if(consumerId==null){
      	return false;
      }
  $scope.is_show = 'yes';
	$scope.selectedTab = 'tab73';
	$scope.selectedItem = 73;
  $localStorage.firstConsumerId = consumerId;
  $scope.loadConsumerTypeSub1(73,consumerId);
	$timeout( function(){
    $scope.loadPlumpExcelData1(73,$localStorage.firstvoltage,$localStorage.firstClass,consumerId);
		$scope.is_show = 'no';
	}, 500 );
}
$scope.getSingleStations3 = function(Id,classId){
  if(classId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab73';
  	$scope.selectedItem = 73;
		$timeout( function(){
      $localStorage.firstClass = classId;
      $scope.loadPlumpExcelData1(73,$localStorage.firstvoltage,classId,$localStorage.firstConsumerId)
			$scope.is_show = 'no';
		}, 500 );
  }
$scope.getSingleStations4 = function(Id,vId){
  if(vId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab73';
  	$scope.selectedItem = 73;
		$timeout( function(){
      $localStorage.firstvoltage = vId;
      $scope.loadPlumpExcelData1(73,vId,$localStorage.firstClass,$localStorage.firstConsumerId);
      console.log('vid: ',vId);
			$scope.is_show = 'no';
		}, 500 );
  }

  $scope.getSingleStations4(73,$localStorage.firstvoltage);


  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f2_1DataUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
               $scope.errorMessage='';
          }, 5000);
        })
  };
  $scope.getSingleStations5 = function(Id,pyId){
    if(pyId==null){
      return false;
    }
     $scope.is_show = 'yes';
      $scope.selectedTab = 'tab73';
      $scope.selectedItem = 73;
      $localStorage.firstPyid=pyId;
      $scope.loadPlumpExcelData1(73,pyId,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
      $timeout( function(){
        $scope.viewTabDetail(73,$localStorage.firstvoltage,$localStorage.firstConsumerId);
        $scope.is_show = 'no';
      }, 400 );
    }


    $scope.getSingleStations6 = function(Id,consumerId){
          if(consumerId==null){
          	return false;
          }
      $scope.is_show = 'yes';
    	$scope.selectedTab = 'tab73';
    	$scope.selectedItem = 73;
      $localStorage.firstConsumerId = consumerId;
      $scope.loadConsumerTypeSub1(73,consumerId);
    	$timeout( function(){
        $scope.viewTabDetail(73,$localStorage.firstvoltage,consumerId);
    		$scope.is_show = 'no';
    	}, 500 );
    }
    $scope.getSingleStations7 = function(Id,vId){
      if(vId==null){
      	return false;
      }
       $scope.is_show = 'yes';
      	$scope.selectedTab = 'tab73';
      	$scope.selectedItem = 73;
    		$timeout( function(){
          $localStorage.firstvoltage = vId;
            $scope.viewTabDetail(73,vId,$localStorage.firstConsumerId);
          console.log('vid: ',vId);
    			$scope.is_show = 'no';
    		}, 500 );
      }

      $scope.getSingleStations7(73,$localStorage.firstvoltage,$localStorage.viewFirstConsumerId);


  $scope.viewTabDetail = function(formId,vId,consumerId){
    var ApiEndPoints = '/f2_1JExcelView?ph_id='+$localStorage.petitionId+'&form_id='+formId+'&user_id='+$localStorage.userId+'&consumer_type_id='+consumerId+'&voltage_level_id='+vId+'&season=Summer';

    ApiServices.fetchFormData(ApiEndPoints,function(data){
       $scope.viewTable = data.listData;
        $scope.total = data.listData.total;
       console.log('$scope.viewTable:', $scope.viewTable);
       if($scope.viewTable.length==0){
         $scope.showHidedata = true;
       }else if($scope.viewTable.length>0){
         $scope.showHidedata = false;
       }
     });
  }
  $scope.viewTabDetail(73,$localStorage.firstvoltage,$localStorage.firstConsumerId);


    $scope.loadHeader = function(){
    $rootScope.tableHeader73 = "";
      var ApiEndPoints = '/form_header2_1a?form_id='+73+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
            $scope.formHeader = data.data.header;
            $rootScope.tableHeader73 = data.data.header;
        });
    }
    $scope.loadHeader()

$scope.viewHeader = function(formId,vId,conClassId,conTypeId){
  var ApiEndPoints = '/all_petition_years?form_id=73&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
   ApiServices.fetchFormData(ApiEndPoints,function(data){
        $scope.viewHeader = data.data.years;
        $scope.yearLength =  ( $scope.viewHeader.length)+3;
     });
	}
$scope.viewHeader();



    /*formtab in form start*/

    $scope.formtab = 1;
    $scope.formsetTab = function(formnewTab){
      if(formnewTab==2){
        $scope.viewTabDetail(73,$localStorage.firstvoltage,$localStorage.firstConsumerId);
        console.log('$scope.viewTable: ',$scope.viewTable);
      }else if(formnewTab==1){
        $scope.loadPlumpExcelData1(73,$localStorage.firstvoltage,$localStorage.firstClass,$localStorage.firstConsumerId);
      }
      $scope.formtab = formnewTab;
    };

    $scope.isformSet = function(formtabNum){
      return $scope.formtab === formtabNum;
    };




  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

  }]
)}
)()
