(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab45Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab45';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };
    $scope.AllStations = ["Plan","Nonplan"];

$scope.getDataByForm = function(uid,tab,data) {
		var result = [];
    result = data;
    return result;
}


$scope.loadPlumpExcelData1 = function(formId,expName){
  //182.75.177.246:8181/wberc_v2/api/f1_19bJExcel?form_id=45&ph_id=658&user_id=6&expenditure=val_nonplan
		var ApiEndPoints = '/f1_19bJExcel?form_id='+45+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId+'&expenditure='+expName;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
      		$scope.subStations = data.data.vals;
      		$scope.subStationsData = data.data;
          $localStorage.expenditureCombo = data.data.expenditures;
          $localStorage.stationsCombo = data.data.stations;

     });
	}

  $scope.loadAllExpanditure = function(formId){

     $scope.loadPlumpExcelData1(45,'val_plan');
  };
  $scope.loadAllExpanditure(45);
  $scope.getSingleStations = function(Id,expName){
    console.log('expName ',expName);
    if(expName==null){
    	return false;
    }
     $scope.is_show = 'yes';
    	$scope.selectedTab = 'tab45';
    	$scope.selectedItem = 45;
      if(expName=="Plan"){
    	   $scope.loadPlumpExcelData1(45,'val_plan');
      }else if(expName=="Nonplan"){
    	   $scope.loadPlumpExcelData1(45,'val_nonplan');
      }else if(expName=="Total"){
    	   $scope.loadPlumpExcelData1(45,'total');
      }
  		$timeout( function(){
  			$scope.is_show = 'no';
  		}, 1000 );
    }




  $scope.saveStationData = function(Seloption,formId){
  	  var postData = {};
  	  var raw = $scope.subStationsData;
      var url = 'f1_19bDataUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };

  $scope.grandTotal = function(){
   var mainraw = angular.copy($scope.subStationsData);
   var raw = $scope.subStationsData.vals;
     console.log('raw: ',$scope.subStationsData);
   $scope.grandTotalArr = raw.reduce(function(r,o){
     console.log('o: ',o);
        // o.data.pop();
        o.data.forEach(function(a){
          console.log('a: ',a);
             a.slice(2).forEach(function(v,i){
                r[i] = (r[i] || 0 ) + parseFloat(v);
             });
         });
       return r;
     },[])
     raw= 0;
  }

  $scope.loadHeader = function(){
  $rootScope.tableHeader44 = "";
  //182.75.177.246:8181/wberc_v2/api/form_header1_19b?form_id=45&ph_id=478&user_id=6
    var ApiEndPoints = '/form_header1_19b?form_id='+45+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          $rootScope.tableHeader45 = data.data.header;
      });
  }
  $scope.loadHeader()
  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };



  }]
)}
)()
