(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab31Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab31';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data.vals;
      return result;
  }
  $scope.loadPlumpExcelData1 = function(formId,pyId){
    //console.log('tab31 controller selectedTable: ',$localStorage.formId);
    //182.75.177.246:8181/wberc_v2/api/f1_17dJExcel?form_id=31&py_id=561&user_id=2
     var ApiEndPoints = '/f1_17dJExcel?form_id=31&py_id='+pyId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;
          		$scope.subStationsData = data.data;
          		 for(var jj=0;jj < data.data.vals.length;jj++){
                       data.data.vals[jj]['valForStationType'];
          		}
       });
  	}

  $scope.loadAllStations = function(formId){
  	$scope.Ensuringyearsforeign = [];
  		var ApiEndPoints = '/petition_ensuing_year?ph_id='+$localStorage.petitionId;
  		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
  				$scope.Ensuringyearsforeign = data.data.ensuing_petition_year;
          $scope.firstStn=$scope.Ensuringyearsforeign[0].py_id;
          console.log($scope.Ensuringyearsforeign);
        $scope.loadPlumpExcelData1(31,$scope.firstStn);
  		  });
  };
  $scope.loadAllStations(31);
  //$scope.loadPlumpExcelData1(31,2518);
  $scope.getSingleStations = function(Id,stationId){
    if(stationId==null){
    	return false;
    }
     $scope.is_show = 'yes';
    	$scope.selectedTab = 'tab31';
    	$scope.selectedItem = 31;
    	$scope.loadPlumpExcelData1(31,stationId);
  		$timeout( function(){
  			$scope.is_show = 'no';
  		}, 1000 );
    }

  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
  	  var raw = $scope.subStationsData;
           var url = 'f1_17dUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };

  $scope.loadPlumpExcelData1($localStorage.formId);
  $scope.loadHeader = function(){
  $rootScope.tableHeader31 = "";
    var ApiEndPoints = '/form_header1_17d?form_id='+31+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          //$scope.formHeader = data.data.header;
          $rootScope.tableHeader31 = data.data.header;
      });
  }
  $scope.loadHeader()



  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

    $scope.grandTotal = function(){
     var mainraw = angular.copy($scope.subStationsData);
     console.log(mainraw);
     var rawData = mainraw.vals;
     var sums = rawData.reduce(function(result,row,i){
       result[1] += parseInt(row[1]);
       result[2] += parseInt(row[2]);
       result[3] += parseInt(row[3]);

       console.log("row[3]:",row[3]);

       return result
       console.log('row: ',row);
       console.log('result: ',result);
       console.log('i: ',i);

     },['Total', 0,0,0])
          //sums[3] = sums[2] + sums[1];
          sums[4] = sums[1] *(sums[2] - sums[3]);
          $scope.grandTotalArr = sums;
           console.log(sums)
    }


  }]
)}
)()
