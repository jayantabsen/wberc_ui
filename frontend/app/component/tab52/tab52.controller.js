(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab52Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab52';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



$scope.getDataByForm = function(uid,tab,data) {
		var result = [];
    result = data['vals'];
    return result;
}
$scope.loadPlumpExcelData1 = function(formId){
  //console.log('tab1 controller selectedTable: ',$localStorage.formId);
//182.75.177.246:8181/wberc_v2/api/f1_18c2JExcel?form_id=42&ph_id=76&user_id=2

		var ApiEndPoints = '/f1_24JExcel?form_id='+52+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
        		$scope.subStations = data.data.vals;
        		$scope.subStationsData = data.data;
        		//  for(var jj=0;jj < data.data.vals.length;jj++){
            //          data.data.vals[jj]['valForStationType'];
        		// }
     });
	}

$scope.loadPlumpExcelData1(52);
// $scope.loadHeader = function(){
//   var ApiEndPoints = '/form_header?form_id='+1+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
//     ApiServices.fetchFormData(ApiEndPoints,function(data){
//         $scope.formHeader = data.data.header;
//         $localStorage.tableHeader = data.data.header;
//     });
// }
// $scope.loadHeader()

// $localStorage.tableHeader

  $scope.saveStationData = function(Seloption,formId){
  	  var postData = {};
  	  var raw = $scope.subStationsData;
      var url = 'f1_24Update';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };

  $scope.ctrlFn= function(){
      var raw = angular.copy($scope.subStationsData);
      var calcArray = raw.vals;
      var result = calcArray.reduce(function(r,o){
        console.log('o',o);
    		  o.slice(1).forEach(function(v,i){
    			   r[i] = (r[i] || 0 ) + parseFloat(v);
        	});
      	return r;
    },[]);
    $scope.totalArray = result;
    $scope.$apply();
  }


  $scope.loadHeader = function(){
  $rootScope.tableHeader52 = "";
    var ApiEndPoints = '/form_header1_24?form_id='+52+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.formHeader = data.data.header;
          $rootScope.tableHeader52 = data.data.header;
      });
  }
  $scope.loadHeader()

  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };

  }]
)}
)()
