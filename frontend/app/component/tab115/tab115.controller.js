(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab115Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab115';
    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };



$scope.getDataByForm = function(uid,tab,data) {
		var result = [];
    result = data['vals'];
    return result;
}
$scope.loadPlumpExcelData1 = function(formId){
  //console.log('tab1 controller selectedTable: ',$localStorage.formId);
//182.75.177.246:8181/wberc_v2/api/f_5_2JExcel?ph_id=$localStorage.petitionId&form_id=115&user_id=$localStorage.userId

		var ApiEndPoints = '/f_5_2JExcel?ph_id='+$localStorage.petitionId+'&form_id=115&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
        		$scope.subStations = data.data.vals;
        		$scope.subStationsData = data.data;
            $rootScope.tableHeader115 = data.data.header; //Priyanka 09.04.2018
            $scope.loadHeader(); //Priyanka 09.04.2018
        		//  for(var jj=0;jj < data.data.vals.length;jj++){
            //          data.data.vals[jj]['valForStationType'];
        		// }
     });
	}

$scope.loadPlumpExcelData1($localStorage.formId);



  $scope.saveStationData = function(Seloption,formId){
  	  var postData = {};
  	  var raw = $scope.subStationsData;
      var url = 'f5_2DataUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };

  /*$scope.loadHeader = function(){
  $rootScope.tableHeader115 = "";
    var ApiEndPoints = '/form_header4_iii?form_id='+115+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.formHeader = data.data.header;
          $rootScope.tableHeader115 = data.data.header;
      });
  }
  $scope.loadHeader()*/


  /*tab in form start*/

  $scope.tab = 1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };


  }]
)}
)()
