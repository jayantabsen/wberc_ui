(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab6Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab6';



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
    	result = data['vals'];
      //console.log('result: ',result);
      return result;
  }
  // $scope.loadPlumpExcelData1 = function(formId){
	// 		var ApiEndPoints = '/f1_3JExcel?form_id='+5+'&ph_id='+$localStorage.petitionId+'&stn_id='+1+'&user_id='+$localStorage.userId;
  //       ApiServices.fetchFormData(ApiEndPoints,function(data){
  //         		$scope.subStations = data.data.vals;
  //         		$scope.subStationsData = data.data;
  //         		 for(var jj=0;jj < data.data.vals.length;jj++){
  //                      data.data.vals[jj]['valForStationType'];
  //         		}
  //      });
  // 	}
  //
  // $scope.loadPlumpExcelData1($localStorage.formId);
  $scope.loadGenerationPowerStation = function(formId,stationId){
  		$scope.subStationsData = [];
			var ApiEndPoints = '/f1_4aJExcel?ph_id='+$localStorage.petitionId+'&form_id='+6+'&stn_id='+stationId+'&user_id='+$localStorage.userId;
			var verison = 2;
			ApiServices.fetchFormData(ApiEndPoints,function(data){
				$scope.subStationsData = data.data;
        //console.log('$scope.subStationsData: ',$scope.subStationsData)
				//calcualteTotal(data.data['vals'],'Grand Total');
			});
  }

$scope.loadAllStations = function(formId){
	$scope.AllStationsWise = [];
		var ApiEndPoints = '/stncombo?form_id='+6+'&util_id='+$localStorage.utilId;
    //console.log('ApiEndPoints', ApiEndPoints);
		ApiServices.requestDataWithDropdown(ApiEndPoints,function(data){
				$scope.AllStationsWise = data.data.stations;
        $scope.firstStn=$scope.AllStationsWise[0].id;
        $scope.loadGenerationPowerStation(6,$scope.firstStn);
  			$scope.optionValStationWise = $scope.firstStn;
		  });
};
$scope.loadAllStations(6);
$scope.getSingleStations = function(Id,stationId){
  if(stationId==null){
  	return false;
  }
   $scope.is_show = 'yes';
  	$scope.selectedTab = 'tab6';
  	$scope.selectedItem = 6;
  	$scope.loadGenerationPowerStation(6,stationId);
		$timeout( function(){
			$scope.is_show = 'no';
		}, 1000 );
  }


  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
   	  var raw = $scope.subStationsData;
           var url = 'f1_4aUpdate';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
                   $scope.errorMessage='';
          }, 5000);

        })
  };

    // $scope.loadHeader = function(){
    //   var ApiEndPoints = '/form_header1_4a?form_id='+6+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    //     ApiServices.fetchFormData(ApiEndPoints,function(data){
    //         $scope.formHeader = data.data.header;
    //         $localStorage.tableHeader = data.data.header;
    //     });
    // }
    // $scope.loadHeader()

  $scope.loadHeader = function(){
    $rootScope.tableHeader6 = "";
    var ApiEndPoints = '/form_header1_4a?form_id='+6+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.formHeader = data.data.header;
          $rootScope.tableHeader6 = data.data.header;
      });
  }
  $scope.loadHeader()

  /*tab in form start*/
  $scope.tab=1;
    $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };
  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };



  }]
)}
)()
