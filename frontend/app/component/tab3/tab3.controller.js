(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('Tab3Controller', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, NewpetitionServices,$state,$localStorage,$window){

    $scope.test = $localStorage.formId;
  	var Phid = $localStorage.petitionId;
    var formId = $localStorage.formId;
    $scope.selectedTab = 'tab3';



  $scope.getDataByForm = function(uid,tab,data) {
  		var result = [];
      result = data['vals'][uid]['valForStationType'];
      return result;
  }
  $scope.loadPlumpExcelData1 = function(formId){
    //console.log('tab2 controller selectedTable: ',$localStorage.formId);

			var ApiEndPoints = '/f1_2JExcel?form_id='+3+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
          		$scope.subStations = data.data.vals;
          		$scope.subStationsData = data.data;
          		 for(var jj=0;jj < data.data.vals.length;jj++){
                       data.data.vals[jj]['valForStationType'];
          		}
       });
  	}

  $scope.loadPlumpExcelData1($localStorage.formId);

  $scope.loadHeader = function(){
    $rootScope.tableHeader3 = "";
    var ApiEndPoints = '/form_header1_2?form_id='+3+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
          $scope.formHeader = data.data.header;
          $rootScope.tableHeader3 = data.data.header;
      });
  }
  $scope.loadHeader()


  $scope.saveStationData = function(Seloption,formId){
  	 var postData = {};
   	  var raw = $scope.subStationsData;
           var url = 'f1_2Update';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
             if(data.status==0){
               $scope.errorMessage=data.message;
             }
             if(data.status==1){
             $scope.updateMessage=data.message;
             }
             $timeout(function() {
                 $scope.updateMessage='';
                     $scope.errorMessage='';
            }, 5000);

        })
  };

$scope.tab=1;
  $scope.setTab = function(newTab){
    $scope.tab = newTab;
  };

  $scope.isSet = function(tabNum){
    return $scope.tab === tabNum;
  };


  $scope.getFormComment = function(){
		var ApiEndPoints = '/get_form_comment?form_id='+3+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    ApiServices.getFormComment(ApiEndPoints,function(data){
        $scope.commentList = data.data.comments;
        if($scope.commentList[0].comments==' '){
          $scope.showHideComment = true;
        }
   });
  };
  $scope.getFormComment();

  $scope.daleteComment = function(commentId){
      var url = "http://182.75.177.246:8181/wberc_v2/api/delete_1_comment?arr_comment_id="+commentId;
      ApiServices.getFileDownload(url,function(data){
        if(data.status==1){
          $scope.getFormComment();
        }
      });
  };
  $scope.submit_comment_Form = function(){
      var url = "http://182.75.177.246:8181/wberc_v2/api/insert_1_comment";
      var formdata = new FormData();
      formdata.append('comment', $scope.comments.comment);
      formdata.append('form_id', 3);
      formdata.append('ph_id', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
      formdata.append('user_id', parseInt(localStorage.getItem("ngStorage-userId").replace(/['"]+/g, '')));
      ApiServices.PostRequest(url, formdata,function(data){
        alert(data.message);
        $scope.comments.comment=" ";
        $scope.showHideComment = false;
      });
      $scope.getFormComment();
  };





  }]
)}
)()
