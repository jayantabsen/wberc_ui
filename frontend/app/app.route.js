(function() {
'use strict';

angular.module('mean').run( ['$rootScope', '$state', '$stateParams',
	function ($rootScope,   $state,   $stateParams) {
		$rootScope.$state = $state.current.name;
		$rootScope.$stateParams = $stateParams;
	}
])

  angular.module('mean').config(function($stateProvider,$locationProvider,$urlRouterProvider,$lazyLoadHelperProvider) {
    var modules={};
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
    $lazyLoadHelperProvider.setDefaultOptions({
      urlArg: new Date().getTime(),
      modules: modules,
      filePath: "/",
      resolve: {
          someResolve: ["$timeout", function($timeout) {
              return $timeout(function() {}, 1000);
          }]
      }
    });

	$urlRouterProvider.otherwise('/login');

    $stateProvider
    .state('login', {
      url: '/login',
      params: {utilId:''},
      templateUrl: 'app/login/views/index.html',
      controller: 'LoginController',
      controllerAs: 'vm',
      lazyModules: [
        'app/login/login.services.js',
        'app/login/login.controller.js',
        'app/tpl/api.services.js',
	       'app/index/front.controller.js',
      ],
      resolve: {
		   utilId: function($stateParams, $localStorage) {
			  if($stateParams.utilId) {
				  $localStorage.utilId = $stateParams.utilId;
			  }
			  return $localStorage.utilId;
		  }
	  }
    })
    .state('header',{
			template : '<main-header></main-header>',
      //controllerAs: 'vm',
      lazyModules: [
       'app/index/front.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js'
      ]
		})
    .state('sidebar',{
			template : '<main-sidebar></main-sidebar>',
      //controllerAs: 'vm',
      lazyModules: [
       'app/index/front.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js'
      ]
		})
    .state('details',{
			template : '<main-details></main-details>',
      lazyModules: [
       'app/index/front.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/tpl/api.services.js'
      ]
		})
    .state('attachment',{
			template : '<attachment-list></attachment-list>',
      lazyModules: [
       'app/index/front.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/tpl/api.services.js'
      ]
		})
    .state('comment',{
			template : '<comment-tab></comment-tab>',
      lazyModules: [
       'app/index/front.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/tpl/api.services.js'
      ]
		})
    .state('file-petition', {
      url: '/file-petition',
      params: {petitionId: '',taskId:'',utilId:''},
      templateUrl: 'app/dashboard/views/index.html',
      controller: 'DashboardController',
      controllerAs: 'vm',
      lazyModules: [
       'app/index/front.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
        'app/newpetition/newpetition.services.js',
      ],
      resolve: {
		   taskId: function($stateParams, $localStorage) {
			  if($stateParams.taskId) {
				  $localStorage.taskId = $stateParams.taskId;
			  }
			  return $localStorage.taskId;
		  },
		   utilId: function($stateParams, $localStorage) {
			  if($stateParams.utilId) {
				  $localStorage.utilId = $stateParams.utilId;
					  $localStorage.token = $stateParams.token;
			  }
			  return $localStorage.utilId;
			  return $localStorage.token;
		  }
	  }
    })
    .state('dashboard', {
      url: '/dashboard',
      templateUrl: 'app/index/views/index.html',
      controller: 'FrontController',
      controllerAs: 'vm',
      lazyModules: [
       'app/index/front.controller.js',
       'app/index/front.services.js',
       'app/dashboard/dashboard.services.js',
       'app/tpl/api.services.js',
       'app/newpetition/newpetition.services.js',
      ]
    })
    .state('forgot-password', {
      url: '/forgot-password',
      templateUrl: 'app/forgotPassword/views/index.html',
      controller: 'ForgotPasswordController',
      controllerAs: 'vm',
      lazyModules: [
			 'app/forgotPassword/forgotPassword.controller.js',
       'app/forgotPassword/forgotPassword.services.js',
       'app/tpl/api.services.js'      ]
    })
    .state('newpetition', {
      url: '/newpetition',
      params: {petitionId: ''},
      templateUrl: 'app/newpetition/views/index.html',
      controller: 'NewpetitionController',
      controllerAs: 'vm',
      lazyModules: [
			 'app/index/front.controller.js',
        'app/newpetition/newpetition.controller.js',
        'app/newpetition/newpetition.services.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
      ]
    })
    .state('home', {
      url: '/home',
      templateUrl: 'app/home/views/index.html',
      controller: 'HomeController',
      controllerAs: 'vm',
      lazyModules: [
			 'app/index/front.controller.js',
        'app/home/home.controller.js',
        'app/tpl/api.services.js',
      ]
    })
    .state('layout1', {
      url: '/layout1',
      templateUrl: 'app/component/tab1/tab1.html',
      params: {annextureId: '',taskId:'',utilId:''},
      controller: 'Tab1Controller',
      controllerAs: 'vm',
      lazyModules: [
			 'app/index/front.controller.js',
        'app/component/tab1/tab1.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ],
      resolve: {
		   annextureId: function($stateParams, $localStorage) {
			  if($stateParams.annextureId) {
				  $localStorage.annextureId = $stateParams.annextureId;
			  }
			  return $localStorage.annextureId;
		  }
	  }
    })
    .state('layout2', {
      url: '/layout2',
      templateUrl: 'app/component/tab2/tab2.html',
      controller: 'Tab2Controller',
      controllerAs: 'vm',
      lazyModules: [
		 'app/index/front.controller.js',
        'app/component/tab2/tab2.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
       'app/index/front.services.js',
       'app/newpetition/newpetition.services.js',
       'app/sidebar/sidebar.controller.js',
       'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout3', {
      url: '/layout3',
      templateUrl: 'app/component/tab3/tab3.html',
      controller: 'Tab3Controller',
      controllerAs: 'vm',
      lazyModules: [
		 'app/index/front.controller.js',
        'app/component/tab3/tab3.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
       'app/index/front.services.js',
       'app/newpetition/newpetition.services.js',
       'app/sidebar/sidebar.controller.js',
       'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout4', {
      url: '/layout4',
      templateUrl: 'app/component/tab4/tab4.html',
      controller: 'Tab4Controller',
      controllerAs: 'vm',
      lazyModules: [
		 'app/index/front.controller.js',
        'app/component/tab4/tab4.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
       'app/index/front.services.js',
       'app/newpetition/newpetition.services.js',
       'app/sidebar/sidebar.controller.js',
       'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout5', {
      url: '/layout5',
      templateUrl: 'app/component/tab5/tab5.html',
      controller: 'Tab5Controller',
      controllerAs: 'vm',
      lazyModules: [
		 'app/index/front.controller.js',
        'app/component/tab5/tab5.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout6', {
      url: '/layout6',
      templateUrl: 'app/component/tab6/tab6.html',
      controller: 'Tab6Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab6/tab6.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout7', {
      url: '/layout7',
      templateUrl: 'app/component/tab7/tab7.html',
      controller: 'Tab7Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab7/tab7.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout8', {
      url: '/layout8',
      templateUrl: 'app/component/tab8/tab8.html',
      controller: 'Tab8Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab8/tab8.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout9', {
      url: '/layout9',
      templateUrl: 'app/component/tab9/tab9.html',
      controller: 'Tab9Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab9/tab9.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout10', {
      url: '/layout10',
      templateUrl: 'app/component/tab10/tab10.html',
      controller: 'Tab10Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab10/tab10.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout11', {
      url: '/layout11',
      templateUrl: 'app/component/tab11/tab11.html',
      controller: 'Tab11Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab11/tab11.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout12', {
      url: '/layout12',
      templateUrl: 'app/component/tab12/tab12.html',
      controller: 'Tab12Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab12/tab12.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout13', {
      url: '/layout13',
      templateUrl: 'app/component/tab13/tab13.html',
      controller: 'Tab13Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab13/tab13.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout14', {
      url: '/layout14',
      templateUrl: 'app/component/tab14/tab14.html',
      controller: 'Tab14Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab14/tab14.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout15', {
      url: '/layout15',
      templateUrl: 'app/component/tab15/tab15.html',
      controller: 'Tab15Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab15/tab15.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout16', {
      url: '/layout16',
      templateUrl: 'app/component/tab16/tab16.html',
      controller: 'Tab16Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab16/tab16.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout17', {
      url: '/layout17',
      templateUrl: 'app/component/tab17/tab17.html',
      controller: 'Tab17Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab17/tab17.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout18', {
      url: '/layout18',
      templateUrl: 'app/component/tab18/tab18.html',
      controller: 'Tab18Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab18/tab18.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout19', {
      url: '/layout19',
      templateUrl: 'app/component/tab19/tab19.html',
      controller: 'Tab19Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab19/tab19.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout20', {
      url: '/layout20',
      templateUrl: 'app/component/tab20/tab20.html',
      controller: 'Tab20Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab20/tab20.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout21', {
      url: '/layout21',
      templateUrl: 'app/component/tab21/tab21.html',
      controller: 'Tab21Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab21/tab21.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout22', {
      url: '/layout22',
      templateUrl: 'app/component/tab22/tab22.html',
      controller: 'Tab22Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab22/tab22.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout23', {
      url: '/layout23',
      templateUrl: 'app/component/tab23/tab23.html',
      controller: 'Tab23Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab23/tab23.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout24', {
      url: '/layout24',
      templateUrl: 'app/component/tab24/tab24.html',
      controller: 'Tab24Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab24/tab24.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout25', {
      url: '/layout25',
      templateUrl: 'app/component/tab25/tab25.html',
      controller: 'Tab25Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab25/tab25.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout26', {
      url: '/layout26',
      templateUrl: 'app/component/tab26/tab26.html',
      controller: 'Tab26Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab26/tab26.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout27', {
      url: '/layout27',
      templateUrl: 'app/component/tab27/tab27.html',
      controller: 'Tab27Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab27/tab27.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout28', {
      url: '/layout28',
      templateUrl: 'app/component/tab28/tab28.html',
      controller: 'Tab28Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab28/tab28.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout29', {
      url: '/layout29',
      templateUrl: 'app/component/tab29/tab29.html',
      controller: 'Tab29Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab29/tab29.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout30', {
      url: '/layout30',
      templateUrl: 'app/component/tab30/tab30.html',
      controller: 'Tab30Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab30/tab30.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout31', {
      url: '/layout31',
      templateUrl: 'app/component/tab31/tab31.html',
      controller: 'Tab31Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab31/tab31.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout32', {
      url: '/layout32',
      templateUrl: 'app/component/tab32/tab32.html',
      controller: 'Tab32Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab32/tab32.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout33', {
      url: '/layout33',
      templateUrl: 'app/component/tab33/tab33.html',
      controller: 'Tab33Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab33/tab33.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout34', {
      url: '/layout34',
      templateUrl: 'app/component/tab34/tab34.html',
      controller: 'Tab34Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab34/tab34.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout35', {
      url: '/layout35',
      templateUrl: 'app/component/tab35/tab35.html',
      controller: 'Tab35Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab35/tab35.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout36', {
      url: '/layout36',
      templateUrl: 'app/component/tab36/tab36.html',
      controller: 'Tab36Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab36/tab36.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout37', {
      url: '/layout37',
      templateUrl: 'app/component/tab37/tab37.html',
      controller: 'Tab37Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab37/tab37.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout38', {
      url: '/layout38',
      templateUrl: 'app/component/tab38/tab38.html',
      controller: 'Tab38Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab38/tab38.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout39', {
      url: '/layout39',
      templateUrl: 'app/component/tab39/tab39.html',
      controller: 'Tab39Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab39/tab39.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout40', {
      url: '/layout40',
      templateUrl: 'app/component/tab40/tab40.html',
      controller: 'Tab40Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab40/tab40.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout41', {
      url: '/layout41',
      templateUrl: 'app/component/tab41/tab41.html',
      controller: 'Tab41Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab41/tab41.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout42', {
      url: '/layout42',
      templateUrl: 'app/component/tab42/tab42.html',
      controller: 'Tab42Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab42/tab42.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout43', {
      url: '/layout43',
      templateUrl: 'app/component/tab43/tab43.html',
      controller: 'Tab43Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab43/tab43.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout44', {
      url: '/layout44',
      templateUrl: 'app/component/tab44/tab44.html',
      controller: 'Tab44Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab44/tab44.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout45', {
      url: '/layout45',
      templateUrl: 'app/component/tab45/tab45.html',
      controller: 'Tab45Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab45/tab45.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout46', {
      url: '/layout46',
      templateUrl: 'app/component/tab46/tab46.html',
      controller: 'Tab46Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab46/tab46.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout47', {
      url: '/layout47',
      templateUrl: 'app/component/tab47/tab47.html',
      controller: 'Tab47Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab47/tab47.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout48', {
      url: '/layout48',
      templateUrl: 'app/component/tab48/tab48.html',
      controller: 'Tab48Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab48/tab48.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout49', {
      url: '/layout49',
      templateUrl: 'app/component/tab49/tab49.html',
      controller: 'Tab49Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab49/tab49.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout50', {
      url: '/layout50',
      templateUrl: 'app/component/tab50/tab50.html',
      controller: 'Tab50Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab50/tab50.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout51', {
      url: '/layout51',
      templateUrl: 'app/component/tab51/tab51.html',
      controller: 'Tab51Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab51/tab51.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout52', {
      url: '/layout52',
      templateUrl: 'app/component/tab52/tab52.html',
      controller: 'Tab52Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab52/tab52.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout53', {
      url: '/layout53',
      templateUrl: 'app/component/tab53/tab53.html',
      controller: 'Tab53Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab53/tab53.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout54', {
      url: '/layout54',
      templateUrl: 'app/component/tab54/tab54.html',
      controller: 'Tab54Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab54/tab54.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout55', {
      url: '/layout55',
      templateUrl: 'app/component/tab55/tab55.html',
      controller: 'Tab55Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab55/tab55.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout56', {
      url: '/layout56',
      templateUrl: 'app/component/tab56/tab56.html',
      controller: 'Tab56Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab56/tab56.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout57', {
      url: '/layout57',
      templateUrl: 'app/component/tab57/tab57.html',
      controller: 'Tab57Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab57/tab57.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout58', {
      url: '/layout58',
      templateUrl: 'app/component/tab58/tab58.html',
      controller: 'Tab58Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab58/tab58.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout59', {
      url: '/layout59',
      templateUrl: 'app/component/tab59/tab59.html',
      controller: 'Tab59Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab59/tab59.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout60', {
      url: '/layout60',
      templateUrl: 'app/component/tab60/tab60.html',
      controller: 'Tab60Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab60/tab60.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout61', {
      url: '/layout61',
      templateUrl: 'app/component/tab61/tab61.html',
      controller: 'Tab61Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab61/tab61.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout62', {
      url: '/layout62',
      templateUrl: 'app/component/tab62/tab62.html',
      controller: 'Tab62Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab62/tab62.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout63', {
      url: '/layout63',
      templateUrl: 'app/component/tab63/tab63.html',
      controller: 'Tab63Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab63/tab63.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout64', {
      url: '/layout64',
      templateUrl: 'app/component/tab64/tab64.html',
      controller: 'Tab64Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab64/tab64.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout65', {
      url: '/layout65',
      templateUrl: 'app/component/tab65/tab65.html',
      controller: 'Tab65Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab65/tab65.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout66', {
      url: '/layout66',
      templateUrl: 'app/component/tab66/tab66.html',
      controller: 'Tab66Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab66/tab66.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout67', {
      url: '/layout67',
      templateUrl: 'app/component/tab67/tab67.html',
      controller: 'Tab67Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab67/tab67.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout68', {
      url: '/layout68',
      templateUrl: 'app/component/tab68/tab68.html',
      controller: 'Tab68Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab68/tab68.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout69', {
      url: '/layout69',
      templateUrl: 'app/component/tab69/tab69.html',
      controller: 'Tab69Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab69/tab69.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout70', {
      url: '/layout70',
      templateUrl: 'app/component/tab70/tab70.html',
      controller: 'Tab70Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab70/tab70.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout71', {
      url: '/layout71',
      templateUrl: 'app/component/tab71/tab71.html',
      controller: 'Tab71Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab71/tab71.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout72', {
      url: '/layout72',
      templateUrl: 'app/component/tab72/tab72.html',
      controller: 'Tab72Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab72/tab72.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout73', {
      url: '/layout73',
      templateUrl: 'app/component/tab73/tab73.html',
      controller: 'Tab73Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab73/tab73.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout74', {
      url: '/layout74',
      templateUrl: 'app/component/tab74/tab74.html',
      controller: 'Tab74Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab74/tab74.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout75', {
      url: '/layout75',
      templateUrl: 'app/component/tab75/tab75.html',
      controller: 'Tab75Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab75/tab75.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout76', {
      url: '/layout76',
      templateUrl: 'app/component/tab76/tab76.html',
      controller: 'Tab76Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab76/tab76.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout77', {
      url: '/layout77',
      templateUrl: 'app/component/tab77/tab77.html',
      controller: 'Tab77Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab77/tab77.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout78', {
      url: '/layout78',
      templateUrl: 'app/component/tab78/tab78.html',
      controller: 'Tab78Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab78/tab78.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout79', {
      url: '/layout79',
      templateUrl: 'app/component/tab79/tab79.html',
      controller: 'Tab79Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab79/tab79.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout80', {
      url: '/layout80',
      templateUrl: 'app/component/tab80/tab80.html',
      controller: 'Tab80Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab80/tab80.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout81', {
      url: '/layout81',
      templateUrl: 'app/component/tab81/tab81.html',
      controller: 'Tab81Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab81/tab81.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout82', {
      url: '/layout82',
      templateUrl: 'app/component/tab82/tab82.html',
      controller: 'Tab82Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab82/tab82.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout83', {
      url: '/layout83',
      templateUrl: 'app/component/tab83/tab83.html',
      controller: 'Tab83Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab83/tab83.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout84', {
      url: '/layout84',
      templateUrl: 'app/component/tab84/tab84.html',
      controller: 'Tab84Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab84/tab84.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout85', {
      url: '/layout85',
      templateUrl: 'app/component/tab85/tab85.html',
      controller: 'Tab85Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab85/tab85.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout86', {
      url: '/layout86',
      templateUrl: 'app/component/tab86/tab86.html',
      controller: 'Tab86Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab86/tab86.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout87', {
      url: '/layout87',
      templateUrl: 'app/component/tab87/tab87.html',
      controller: 'Tab87Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab87/tab87.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout88', {
      url: '/layout88',
      templateUrl: 'app/component/tab88/tab88.html',
      controller: 'Tab88Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab88/tab88.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout89', {
      url: '/layout89',
      templateUrl: 'app/component/tab89/tab89.html',
      controller: 'Tab89Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab89/tab89.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout90', {
      url: '/layout90',
      templateUrl: 'app/component/tab90/tab90.html',
      controller: 'Tab90Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab90/tab90.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout91', {
      url: '/layout91',
      templateUrl: 'app/component/tab91/tab91.html',
      controller: 'Tab91Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab91/tab91.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout92', {
      url: '/layout92',
      templateUrl: 'app/component/tab92/tab92.html',
      controller: 'Tab92Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab92/tab92.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout93', {
      url: '/layout93',
      templateUrl: 'app/component/tab93/tab93.html',
      controller: 'Tab93Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab93/tab93.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout94', {
      url: '/layout94',
      templateUrl: 'app/component/tab94/tab94.html',
      controller: 'Tab94Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab94/tab94.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout95', {
      url: '/layout95',
      templateUrl: 'app/component/tab95/tab95.html',
      controller: 'Tab95Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab95/tab95.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout96', {
      url: '/layout96',
      templateUrl: 'app/component/tab96/tab96.html',
      controller: 'Tab96Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab96/tab96.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout97', {
      url: '/layout97',
      templateUrl: 'app/component/tab97/tab97.html',
      controller: 'Tab97Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab97/tab97.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout98', {
      url: '/layout98',
      templateUrl: 'app/component/tab98/tab98.html',
      controller: 'Tab98Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab98/tab98.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout99', {
      url: '/layout99',
      templateUrl: 'app/component/tab99/tab99.html',
      controller: 'Tab99Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab99/tab99.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout100', {
      url: '/layout100',
      templateUrl: 'app/component/tab100/tab100.html',
      controller: 'Tab100Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab100/tab100.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout101', {
      url: '/layout101',
      templateUrl: 'app/component/tab101/tab101.html',
      controller: 'Tab101Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab101/tab101.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout102', {
      url: '/layout102',
      templateUrl: 'app/component/tab102/tab102.html',
      controller: 'Tab102Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab102/tab102.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout103', {
      url: '/layout103',
      templateUrl: 'app/component/tab103/tab103.html',
      controller: 'Tab103Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab103/tab103.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout104', {
      url: '/layout104',
      templateUrl: 'app/component/tab104/tab104.html',
      controller: 'Tab104Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab104/tab104.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout105', {
      url: '/layout105',
      templateUrl: 'app/component/tab105/tab105.html',
      controller: 'Tab105Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab105/tab105.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout106', {
      url: '/layout106',
      templateUrl: 'app/component/tab106/tab106.html',
      controller: 'Tab106Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab106/tab106.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout107', {
      url: '/layout107',
      templateUrl: 'app/component/tab107/tab107.html',
      controller: 'Tab107Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab107/tab107.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout108', {
      url: '/layout108',
      templateUrl: 'app/component/tab108/tab108.html',
      controller: 'Tab108Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab108/tab108.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout109', {
      url: '/layout109',
      templateUrl: 'app/component/tab109/tab109.html',
      controller: 'Tab109Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab109/tab109.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout110', {
      url: '/layout110',
      templateUrl: 'app/component/tab110/tab110.html',
      controller: 'Tab110Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab110/tab110.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout111', {
      url: '/layout111',
      templateUrl: 'app/component/tab111/tab111.html',
      controller: 'Tab111Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab111/tab111.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout112', {
      url: '/layout112',
      templateUrl: 'app/component/tab112/tab112.html',
      controller: 'Tab112Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab112/tab112.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout113', {
      url: '/layout113',
      templateUrl: 'app/component/tab113/tab113.html',
      controller: 'Tab113Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab113/tab113.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout114', {
      url: '/layout114',
      templateUrl: 'app/component/tab114/tab114.html',
      controller: 'Tab114Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab114/tab114.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout115', {
      url: '/layout115',
      templateUrl: 'app/component/tab115/tab115.html',
      controller: 'Tab115Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab115/tab115.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout116', {
      url: '/layout116',
      templateUrl: 'app/component/tab116/tab116.html',
      controller: 'Tab116Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab116/tab116.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout117', {
      url: '/layout117',
      templateUrl: 'app/component/tab117/tab117.html',
      controller: 'Tab117Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab117/tab117.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout118', {
      url: '/layout118',
      templateUrl: 'app/component/tab118/tab118.html',
      controller: 'Tab118Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab118/tab118.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout119', {
      url: '/layout119',
      templateUrl: 'app/component/tab119/tab119.html',
      controller: 'Tab119Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab119/tab119.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout120', {
      url: '/layout120',
      templateUrl: 'app/component/tab120/tab120.html',
      controller: 'Tab120Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab120/tab120.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout121', {
      url: '/layout121',
      templateUrl: 'app/component/tab121/tab121.html',
      controller: 'Tab121Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab121/tab121.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout122', {
      url: '/layout122',
      templateUrl: 'app/component/tab122/tab122.html',
      controller: 'Tab122Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab122/tab122.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout123', {
      url: '/layout123',
      templateUrl: 'app/component/tab123/tab123.html',
      controller: 'Tab123Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab123/tab123.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout124', {
      url: '/layout124',
      templateUrl: 'app/component/tab124/tab124.html',
      controller: 'Tab124Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab124/tab124.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout125', {
      url: '/layout125',
      templateUrl: 'app/component/tab125/tab125.html',
      controller: 'Tab125Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab125/tab125.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout126', {
      url: '/layout126',
      templateUrl: 'app/component/tab126/tab126.html',
      controller: 'Tab126Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab126/tab126.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout127', {
      url: '/layout127',
      templateUrl: 'app/component/tab127/tab127.html',
      controller: 'Tab127Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab127/tab127.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout128', {
      url: '/layout128',
      templateUrl: 'app/component/tab128/tab128.html',
      controller: 'Tab128Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab128/tab128.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout129', {
      url: '/layout129',
      templateUrl: 'app/component/tab129/tab129.html',
      controller: 'Tab129Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab129/tab129.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })
    .state('layout130', {
      url: '/layout130',
      templateUrl: 'app/component/tab130/tab130.html',
      controller: 'Tab130Controller',
      controllerAs: 'vm',
      lazyModules: [
        'app/component/tab130/tab130.controller.js',
        'app/dashboard/dashboard.controller.js',
        'app/dashboard/dashboard.services.js',
        'app/tpl/api.services.js',
         'app/index/front.controller.js',
         'app/index/front.services.js',
         'app/newpetition/newpetition.services.js',
         'app/sidebar/sidebar.controller.js',
         'app/attachment-list/attachmentlist.controller.js'
      ]
    })

});
})();
