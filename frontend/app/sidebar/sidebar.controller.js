(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('SidebarController', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','$rootScope', '$stateParams','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,$rootScope, $stateParams,$state,$localStorage,$window){
    $rootScope.state = $state.current.name;
    $rootScope.$stateParams = $stateParams;
    console.log('$state',$state.current.name);
      $scope.signedIn = false;

      $scope.sidebar = 'test side bar';

      var tokenValue =  localStorage.getItem('token');
    if($localStorage.petitionId==undefined || $localStorage.petitionId == ""){
        $rootScope.message = 'You Cannot access the link directly. Please create a new petition first.';
         $location.path('/dashboard');
    }
    var Phid = $localStorage.petitionId;
    var utilid = $localStorage.utilId;
    $scope.petitionId = $localStorage.petitionId;
    $scope.user = ApiServices.getAuthUser();
    var valUser = JSON.parse(localStorage.getItem("user"));


      $scope.init = function(){
        $scope.signedIn = ApiServices.loggedIn();
          if(!$scope.signedIn){
            $location.path('/login')
          }else{
             $scope.utilid = valUser.utilid;
              $scope.allMenu = valUser.menu_status;
              var showState;
              for(var i =0; i<$scope.allMenu.length; i++){
                showState = $scope.allMenu[i].state;
              }
              if(showState=='1'){
                $scope.showMenu == true;
              }else if(showState=='0'){
                $scope.showMenu == !$scope.showMenu;
              }
          }
      };


      /*side menu start*/
      $scope.sidebarMenuFn = function(user){
            ApiServices.sidebarMenu(function(_data){
              if(_data.status == '1'){
                $scope.sidebarMenuList =_data.data.formList;
                $localStorage.sidebarMenuList =_data.data.formList;
                $scope.activeMenu = $scope.sidebarMenuList[0].form_code;
                $scope.tabcode = _data.data.formList;

              }else{
                  console.log('_data failed',_data.data);
              }
            });
          }
      $scope.sidebarMenuFn();
      /*side menu end*/
//$scope.activeClass = "";
$scope.formIdfn = function($event,formId,page,annexId){
  $localStorage.formId = formId;
  console.log('event.target(): ',$event.currentTarget.parentNode.parentNode.parentNode.parentNode)
    var parent=$event.currentTarget.parentNode.parentNode.parentNode.parentNode;
    $localStorage.page = page;
    $rootScope.annextureId = annexId;
    $localStorage.annextureId = annexId;
     if(formId<68){
       console.log('className: ',document.getElementById('collapse1'));
      document.getElementById('collapse1').className += " panel-collapse collapse in";
       document.getElementById('collapse2').className += " panel-collapse collapse";
       document.getElementById('collapse3').className += " panel-collapse collapse";
       document.getElementById('collapse4').className += " panel-collapse collapse";
       document.getElementById('collapse5').className += " panel-collapse collapse";
       document.getElementById('collapse6').className += " panel-collapse collapse";
       document.getElementById('collapse7').className += " panel-collapse collapse";
     }
      if(formId>71 && formId<90){
        document.getElementById('collapse2').className += " in";
        document.getElementById('collapse1').removeClass('in');
        document.getElementById('collapse3').removeClass('in');
        document.getElementById('collapse4').removeClass('in');
        document.getElementById('collapse5').removeClass('in');
        document.getElementById('collapse6').removeClass('in');
        document.getElementById('collapse7').removeClass('in');
     }
      if(formId>89 && formId<103){
          document.getElementById('collapse3').className += " in";
          document.getElementById('collapse2').removeClass('in');
          document.getElementById('collapse1').removeClass('in');
          document.getElementById('collapse4').removeClass('in');
          document.getElementById('collapse5').removeClass('in');
          document.getElementById('collapse6').removeClass('in');
          document.getElementById('collapse7').removeClass('in');
     }
     if(formId>102 && formId<114){
         document.getElementById('collapse4').className += " in";
         document.getElementById('collapse2').removeClass('in');
         document.getElementById('collapse3').removeClass('in');
         document.getElementById('collapse1').removeClass('in');
         document.getElementById('collapse5').removeClass('in');
         document.getElementById('collapse6').removeClass('in');
         document.getElementById('collapse7').removeClass('in');
     }
     if(formId>113 && formId<124){
         document.getElementById('collapse5').className += " in";
         document.getElementById('collapse2').removeClass('in');
         document.getElementById('collapse3').removeClass('in');
         document.getElementById('collapse4').removeClass('in');
         document.getElementById('collapse1').removeClass('in');
         document.getElementById('collapse6').removeClass('in');
         document.getElementById('collapse7').removeClass('in');
     }
      if(formId==124){
          document.getElementById('collapse6').className += " in";
          document.getElementById('collapse2').removeClass('in');
          document.getElementById('collapse3').removeClass('in');
          document.getElementById('collapse4').removeClass('in');
          document.getElementById('collapse5').removeClass('in');
          document.getElementById('collapse1').removeClass('in');
          document.getElementById('collapse7').removeClass('in');
     }
      if(formId==125){
        document.getElementById('collapse7').className += " in";
        document.getElementById('collapse2').removeClass('in');
        document.getElementById('collapse3').removeClass('in');
        document.getElementById('collapse4').removeClass('in');
        document.getElementById('collapse5').removeClass('in');
        document.getElementById('collapse6').removeClass('in');
        document.getElementById('collapse1').removeClass('in');
     }
}

$scope.navClass = function (page) {
        //var currentRoute = $location.path().substring(1) || 'home';
        return page === currentRoute ? 'active' : '';
        var currentRoute = $location.path();
        console.log('currentRoute: ',currentRoute);

    };


$scope.init();



  }]);
})();
