(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('HomeController', ['$scope','$location','ApiServices','$filter','$window',function($scope,$location,ApiServices,$filter,$window){

  	$scope.init = function(){

  	$scope.signedIn = ApiServices.loggedIn();
      if(!$scope.signedIn){
        $location.path('/login')
      }else{
        $scope.user = ApiServices.getAuthUser();
        //console.log("loggedIn false");
      }

  	};

  	$scope.proceed = function() {

  		console.log("Proceed Cluicked.");

  	};

  	$scope.init();

  }]);
})();
