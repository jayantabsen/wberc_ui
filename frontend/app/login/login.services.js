(function() {
  'use strict';
  angular.module('mean')
    .factory('LoginServices', function($http, $q) {
      return {
        changevalue: function(params, callback) {
            var cb = callback || angular.noop;
            var deferred = $q.defer();
            $http.post('/api/login', {
                ver: '1.0',
                jsonrpc: '2.0',
                params: params
            }).
            success(function(data) {
                deferred.resolve(data);
                return cb(data.result);
            }).
            error(function(err) {
                deferred.reject(err);
                return cb(err);
            }.bind(this));
            return deferred.promise;
        }
      };
    });
})();
