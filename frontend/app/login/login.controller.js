(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('LoginController', ['$rootScope','$scope','LoginServices','ApiServices','$filter','$location','$localStorage','$window','$timeout',function($rootScope,$scope,LoginServices,ApiServices,$filter,$location,$localStorage,$window,$timeout){
    $scope.login = {
      loginID:'',
      password:''
    };
    $scope.error = {
      msg:''
    }
    $scope.signedIn = false;

    $window.localStorage.clear();
  //  $rootScope.forgotPwd = false;
    $scope.mailSuccess = false;
    $scope.init = function(){
    }
    $scope.doLogin = function(user){
      if(user.loginID != ""){
        if(user.password != ""){
          ApiServices.checkLogin(user, function(_data){
            if(_data.status == '1'){
              console.log('_data',_data.data);
              $rootScope.rollId = _data.data.roleid;
              $rootScope.utilId = _data.data.utilid;
              $localStorage.userId = _data.data.userid;
              $localStorage.utypeId = _data.data.utype_id;
              ApiServices.storeUser(_data.data, _data.data.token);
              var tokenValue =  localStorage.getItem('token');
                if(tokenValue==undefined || tokenValue==null){
                  $state.go('login');
                }else if(tokenValue!=undefined || tokenValue!=null){
                  $location.path('/dashboard');
                }
              }else{
              $scope.error.msg = _data.message;
              $scope.loginError = true;
            }
          });
        }
        else{
          $scope.loginError = true;
          $scope.error.msg = "Please Enter Password";
          $timeout(function() {
            $scope.loginError = false;
            $scope.error.msg ="";
          }, 2000);
        }
      }
      else{
        $scope.loginError = true;
        $scope.error.msg = "Please Enter User Name";
        $timeout(function() {
          $scope.loginError = false;
          $scope.error.msg = "";
        }, 2000);
      }
    }
    // By Najma
    // Forgot password
    $scope.onForgotPassword = function(user){
     console.log("forgot pwd");
     console.log("hideForgotBox",$rootScope.hideForgotBox);
     $rootScope.forgotPwd = true;
     $rootScope.hideForgotBox = false;
    }
    // Back to login page
    $scope.backToLogin = function(){
       $rootScope.forgotPwd = false;
       $scope.mailSuccess = false;
       $scope.forgotUserId="";
    }

    // On forgot submit
    // $rootScope.hideForgotBox = false;
    // $rootScope.forgotError = false;
    // $rootScope.forgotSuccess = false;
    $scope.forgotUserId="";
    // $rootScope.forgotErrorMsg="";
    // $rootScope.forgotSuccessMsg="";
    $scope.submitForgotPwd = function(userId){
     $scope.forgotUserId =  userId;
     console.log("user id",$scope.forgotUserId);
     if(userId != "" && userId != undefined){
       $scope.loginError = false;
       ApiServices.sendPasswordInMail(userId, function(_data){
         if(_data.status == '1'){
           $rootScope.forgotSuccessMsg = _data.message;
           $rootScope.forgotSuccess = true;
           $rootScope.forgotError = false;
           $rootScope.hideForgotBox = true;
           }
           else{
             $rootScope.forgotErrorMsg = _data.message;
             $rootScope.forgotSuccess = false;
             $rootScope.forgotError = true;
             $rootScope.hideForgotBox = false;
             $timeout(function() {
               $rootScope.forgotError = false;
               $rootScope.forgotErrorMsg = "";
             }, 2000);
           }
       });
     }
     else{
       $scope.loginError = true;
       $scope.error.msg = "Please Enter User Name Or Email Id"
     }
    }






    // var token = localStorage.getItem('token');
    // if(!token){
    //   $state.go('login');
    //   console.log('token undefined');
    // }

    $scope.init();
  }]);
})();
