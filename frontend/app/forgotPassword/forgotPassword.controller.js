(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('ForgotPasswordController', ['$rootScope','$scope','ApiServices','$filter','$location','$localStorage','$window','ForgotPasswordServices','$timeout',function($rootScope,$scope,ApiServices,$filter,$location,$localStorage,$window,ForgotPasswordServices,$timeout){
    $window.localStorage.clear();
    $scope.forgotPwd = false;
    $scope.init = function(){
    }

    // By Najma
    // On reset password submit
    $scope.errorMsg="";
    $scope.pwdError=false;
    $scope.resetSuccess=false;
    $scope.resetSuccessMsg="";
    $scope.onSubmitResetPassword = function(oldPwd,newPwd,confirmPwd){
     console.log("pwd",oldPwd,newPwd,confirmPwd);
     if(oldPwd != "" && oldPwd != undefined){
       if(newPwd != "" && newPwd != undefined){
         if(confirmPwd != "" && confirmPwd != undefined){
           if(newPwd != confirmPwd){
             $scope.errorMsg="New Password And Confirm Password Did Not Match";
             $scope.pwdError=true;
             $timeout(function() {
               $scope.pwdError = false;
               $scope.errorMsg = "";
             }, 2000);
           }
           else{
              $scope.pwdError=false;
              console.log("service call here");
              var loginId = $location.search().login_id;
              var resetObj ={
                "loginID":loginId,
                "currentPwd":oldPwd,
                "newPwd":newPwd,
                "confirmPwd":confirmPwd
              }
              console.log("resetObj",resetObj);
              ApiServices.resetPassword(resetObj, function(_data){
                if(_data.status == '1'){
                  $scope.resetSuccessMsg= _data.message;
                  $scope.resetSuccess=true;
                  $scope.pwdError = false;
                  // $location.path('/login');
                  }
                  else{
                  $scope.errorMsg = _data.message;
                  $scope.pwdError = true;
                  $scope.resetSuccess=false;
                  $timeout(function() {
                    $scope.pwdError = false;
                    $scope.errorMsg = "";
                  }, 2000);
                  }
              });
           }
         }
         else{
           $scope.errorMsg="Please Enter Confirm Password";
           $scope.pwdError=true;
           $timeout(function() {
             $scope.pwdError = false;
             $scope.errorMsg = "";
           }, 2000);
         }
       }
       else{
         $scope.errorMsg="Please Enter New Password";
         $scope.pwdError=true;
         $timeout(function() {
           $scope.pwdError = false;
           $scope.errorMsg = "";
         }, 2000);
       }
     }
     else{
       $scope.errorMsg="Please Enter Current Password";
       $scope.pwdError=true;
       $timeout(function() {
         $scope.pwdError = false;
         $scope.errorMsg = "";
       }, 2000);
     }

    }

    // Back to login page
    $scope.backToLogin = function(){
      $location.path('/login');
    }




    // var token = localStorage.getItem('token');
    // if(!token){
    //   $state.go('login');
    //   console.log('token undefined');
    // }

  }]);
})();
