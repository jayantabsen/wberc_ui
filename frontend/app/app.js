(function() {
  'use strict';
  angular.module('mean', [
    'ui.router',
    'ui.bootstrap',
    'fl.lazy',
    'ngCookies',
    'ngStorage',
    'angular.filter',
  ]);
})();
