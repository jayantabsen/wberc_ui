(function() {
  'use strict';
  var app = angular.module('mean');
  app.factory('DashboardServices', function($http, $q, $cookies) {
    var DashboardServices = {};
    return DashboardServices;
  });

app.directive('attachFileModel', function ($parse) {
        return {
            restrict: 'A', //the directive can be used as an attribute only
            link: function (scope, element, attrs) {
                var model = $parse(attrs.attachFileModel),
                    modelSetter = model.assign;
                element.bind('change', function () {
                    //Call apply on scope, it checks for value changes and reflect them on UI
                    scope.$apply(function () {
                        //set the model value
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    });
app.directive('validFile',function(){
  return {
    require:'ngModel',
    link:function(scope,el,attrs,ngModel){
      //change event is fired when file is selected
      el.bind('change',function(){
        scope.$apply(function(){
          ngModel.$setViewValue(el.val());
          ngModel.$render();
        })
      })
    }
  }
});
app.directive('excellForm',['ApiServices','$compile','$localStorage','$rootScope','$timeout', function (ApiServices,$compile,$localStorage,$rootScope,$timeout) {
      var getTemplate = function(){
        return "<button>Save</button>";
      };
      return {
         restrict: 'E',
         scope: {
           headerUrl: '@',
           uid:'=',
           tab:'@',
           values:'=',
           newTotalArray:'=',
          someCtrlFn: '&callbackFn'
         },
         link: function(scope, element, attrs) {
           var loadDirective = function(){
             scope.$watch('values', function(newValue, oldValue) {
    					  var columnAttr = [];
                  if(scope.tab=='tab1'){
                    setTimeout(function(){
                      element.find('thead.jexcel_label').before($rootScope.tableHeader1);
                    });
                  }
                 else if(scope.tab=='tab2'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader2);
                      });
                    }
                 else if(scope.tab=='tab3'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader3);
                      });
                    }
                 else if(scope.tab=='tab4'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader4);
                      });
                    }
                 else if(scope.tab=='tab5'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader5);
                      });
                    }
                 else if(scope.tab=='tab5-1'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader5);
                      });
                    }
                 else if(scope.tab=='tab6'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader6);
                      });
                    }
                 else if(scope.tab=='tab7'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader7);
                      });
                    }
                 else if(scope.tab=='tab8'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader8);
                      });
                    }
                 else if(scope.tab=='tab9'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader9);
                      });
                    }
                 else if(scope.tab=='tab10'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader10);
                      });
                    }
                 else if(scope.tab=='tab11'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader11);
                      });
                    }
                 else if(scope.tab=='tab12'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader12);
                      });
                    }
                 else if(scope.tab=='tab13'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader13);
                      });
                    }
                 else if(scope.tab=='tab14'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader14);
                      });
                    }
                 else if(scope.tab=='tab15'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader15);
                      });
                    }
                else if(scope.tab=='tab16'){
                     setTimeout(function(){
                         element.find('thead.jexcel_label').before($rootScope.tableHeader16);
                     });
                   }
                 else if(scope.tab=='tab17' || scope.tab=='tab17-1'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader17);
                      });
                    }
                 else if(scope.tab=='tab18'){
                      setTimeout(function(){
                        if($("#header18 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader18);
                        }
                      });
                    }
                 else if(scope.tab=='tab19'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader19);
                      });
                    }
                 else if(scope.tab=='tab20'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader20);
                      });
                    }
                 else if(scope.tab=='tab21'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader21);
                      });
                    }
                 else if(scope.tab=='tab22'){
                      setTimeout(function(){
                        if($("#header22 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader22);
                        }
                      });
                    }
                 else if(scope.tab=='tab23'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader23);
                      });
                    }
                 else if(scope.tab=='tab24'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader24);
                      });
                    }
                 else if(scope.tab=='tab25'){
                      setTimeout(function(){
                        if($("#header25 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader25);
                        }
                      });
                    }
                 else if(scope.tab=='tab26'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader26);
                      });
                    }
                 else if(scope.tab=='tab27'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader27);
                      });
                    }
                 else if(scope.tab=='tab28'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader28);
                      });
                    }
                 else if(scope.tab=='tab29'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader29);
                      });
                    }
                 else if(scope.tab=='tab30'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader30);
                      });
                    }
                 else if(scope.tab=='tab31'){
                      setTimeout(function(){
                        if($("#header31 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader31);
                        }
                      });
                    }
                 else if(scope.tab=='tab32'){
                      setTimeout(function(){
                        if($("#header32 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader32);
                        }
                      });
                    }
                 else if(scope.tab=='tab33'){
                      setTimeout(function(){
                        if($("#header33 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader33);
                        }
                      });
                    }
                 else if(scope.tab=='tab34'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader34);
                      });
                    }
                 else if(scope.tab=='tab35'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader35);
                      });
                    }
                 else if(scope.tab=='tab36'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader36);
                      });
                    }
                 else if(scope.tab=='tab37'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader37);
                      });
                    }
                 else if(scope.tab=='tab38'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader38);
                      });
                    }
                 else if(scope.tab=='tab39'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader39);
                      });
                    }
                 else if(scope.tab=='tab40'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader40);
                      });
                    }
                 else if(scope.tab=='tab41'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader41);
                      });
                    }
                 else if(scope.tab=='tab42'){
                      setTimeout(function(){
                        if($("#header42 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader42);
                        }
                      });
                    }
                 else if(scope.tab=='tab43'){
                      setTimeout(function(){
                        if($("#header43 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader43);
                        }
                      });
                    }
                 else if(scope.tab=='tab44'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader44);
                      });
                    }
                 else if(scope.tab=='tab45-0'||scope.tab=='tab45-1'||scope.tab=='tab45-2'){
                      setTimeout(function(){
                        //if($("#header45 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader45);
                        //}
                      });
                    }
                 else if(scope.tab=='tab46'){
                      setTimeout(function(){
                        if($("#header46 tr").length == 0){
                            element.find('thead.jexcel_label').before($rootScope.tableHeader46);
                        }
                      });
                    }
                 else if(scope.tab=='tab47'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader47);
                      });
                    }
                 else if(scope.tab=='tab48'){
                      setTimeout(function(){
                        if($("#header48 tr").length == 0){
                            element.find('thead.jexcel_label').before($rootScope.tableHeader48);
                        }
                      });
                    }
                 else if(scope.tab=='tab49'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader49);
                      });
                    }
                 else if(scope.tab=='tab50'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader50);
                      });
                    }
                 else if(scope.tab=='tab51'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader51);
                      });
                    }
                 else if(scope.tab=='tab52'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader52);
                      });
                    }
                 else if(scope.tab=='tab53'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader53);
                      });
                    }
                 else if(scope.tab=='tab54'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader54);
                      });
                    }
                 else if(scope.tab=='tab55'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader55);
                      });
                    }
                 else if(scope.tab=='tab56'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader56);
                      });
                    }
                 else if(scope.tab=='tab57'){
                      setTimeout(function(){
                        if($("#header57 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader57);
                        }
                      });
                    }
                 else if(scope.tab=='tab58'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader58);
                      });
                    }
                 else if(scope.tab=='tab59-1' || scope.tab=='tab59-2' || scope.tab=='tab59-3'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader59);
                      });
                    }
                 else if(scope.tab=='tab61'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader61);
                      });
                    }
                 else if(scope.tab=='tab62'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader62);
                      });
                    }
                 else if(scope.tab=='tab63'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader63);
                      });
                    }
                 else if(scope.tab=='tab66'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader66);
                      });
                    }
                 else if(scope.tab=='tab67'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader67);
                      });
                    }
                 else if(scope.tab=='tab71'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader71);
                      });
                    }
                 else if(scope.tab=='tab72'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader72);
                      });
                    }
                 else if(scope.tab=='tab73'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader73);
                      });
                    }
                 else if(scope.tab=='tab74'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader74);
                      });
                    }
                 else if(scope.tab=='tab75'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader75);
                      });
                    }
                 else if(scope.tab=='tab76'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader76);
                      });
                    }
                 else if(scope.tab=='tab77'){
                      setTimeout(function(){
                        if($("#header77 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader77);
                        }
                      });
                    }
                 else if(scope.tab=='tab78'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader78);
                      });
                    }
                 else if(scope.tab=='tab79'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader79);
                      });
                    }
                 else if(scope.tab=='tab80'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader80);
                      });
                    }
                 else if(scope.tab=='tab81'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader81);
                      });
                    }
                 else if(scope.tab=='tab83'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader83);
                      });
                    }
                 else if(scope.tab=='tab84'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader84);
                      });
                    }
                 else if(scope.tab=='tab85'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader85);
                      });
                    }
                 else if(scope.tab=='tab86'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader86);
                      });
                    }
                 else if(scope.tab=='tab87'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader87);
                      });
                    }
                 else if(scope.tab=='tab88'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader88);
                      });
                    }
                 else if(scope.tab=='tab90'){
                    setTimeout(function(){
                        if($("#header90 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader90);
                        }
                      });
                    }

                 else if(scope.tab=='tab91'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader91);
                      });
                    }
                 else if(scope.tab=='tab92'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader92);
                      });
                    }
                 else if(scope.tab=='tab93'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader93);
                      });
                    }
                 else if(scope.tab=='tab94'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader94);
                      });
                    }
                 else if(scope.tab=='tab96'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader96);
                      });
                    }
                 else if(scope.tab=='tab97'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader97);
                      });
                    }
                 else if(scope.tab=='tab98'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader98);
                      });
                    }
                 else if(scope.tab=='tab99'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader99);
                      });
                    }
                 else if(scope.tab=='tab100'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader100);
                      });
                    }
                 else if(scope.tab=='tab101'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader101);
                      });
                    }
                 else if(scope.tab=='tab103'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader103);
                      });
                    }
                 else if(scope.tab=='tab104'){
                      setTimeout(function(){
                        //$('#header104').find('tr').remove();
                        if($("#header104 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader104);
                        }
                      });
                    }
                 else if(scope.tab=='tab105'){
                      setTimeout(function(){
                        //$('#header105').find('tr').remove();
                        if($("#header105 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader105);
                        }
                      });
                    }
                 else if(scope.tab=='tab106'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader106);
                      });
                    }
                 else if(scope.tab=='tab107'){
                      setTimeout(function(){
                        if($("#header107 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader107);
                        }
                      });
                    }
                 else if(scope.tab=='tab108'){
                      setTimeout(function(){
                        if($("#header108 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader108);
                        }
                      });
                    }
                 else if(scope.tab=='tab109'){
                      setTimeout(function(){
                        if($("#header109 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader109);
                        }
                      });
                    }
                 else if(scope.tab=='tab110'){
                      setTimeout(function(){
                        if($("#header110 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader110);
                        }
                      });
                    }
                 else if(scope.tab=='tab111'){
                      setTimeout(function(){
                        //$('#header111').find('tr').remove();
                        if($("#header111 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader111);
                        }
                      });
                    }
                 else if(scope.tab=='tab112'){
                      setTimeout(function(){
                        if($("#header112 tr").length == 0){
                              element.find('thead.jexcel_label').before($rootScope.tableHeader112);
                        }
                      });
                    }
                 else if(scope.tab=='tab113'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader113);
                      });
                    }
                 else if(scope.tab=='tab114'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader114);
                      });
                    }
                 else if(scope.tab=='tab115'){
                      setTimeout(function(){
                        //$('#header115').find('tr').remove();
                        if($("#header115 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader115);
                        }
                      });
                    }
                 else if(scope.tab=='tab116'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader116);
                      });
                    }
                 else if(scope.tab=='tab117'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader117);
                      });
                    }
                 else if(scope.tab=='tab118'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader118);
                      });
                    }
                 else if(scope.tab=='tab119'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader119);
                      });
                    }
                 else if(scope.tab=='tab120'){
                      setTimeout(function(){
                        //$('#header120').find('tr').remove();
                        if($("#header120 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader120);
                        }
                      });
                    }
                 else if(scope.tab=='tab121'){
                      setTimeout(function(){
                        //$('#header121').find('tr').remove();
                        if($("#header121 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader121);
                        }
                      });
                    }
                 else if(scope.tab=='tab122'){
                      setTimeout(function(){
                        //$('#header121').find('tr').remove();
                        if($("#header122 tr").length == 0){
                          element.find('thead.jexcel_label').before($rootScope.tableHeader122);
                        }
                      });
                    }
                 else if(scope.tab=='tab122-1'){
                      setTimeout(function(){
                       element.find('thead.jexcel_label').before($rootScope.tableHeader1221);
                     });

                    }
                 else if(scope.tab=='tab122-2'){
                      setTimeout(function(){
                       element.find('thead.jexcel_label').before($rootScope.tableHeader1222);
                     });

                    }
                 else if(scope.tab=='tab123'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader123);
                      });
                    }
                 else if(scope.tab=='tab124'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader124);
                      });
                    }
                 else if(scope.tab=='tab125' || scope.tab=='tab125-1'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader125);
                      });
                    }
                 else if(scope.tab=='tab129'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader129);
                      });
                    }
                 else if(scope.tab=='tab130'){
                      setTimeout(function(){
                        element.find('thead.jexcel_label').before($rootScope.tableHeader130);
                      });
                    }else{}
              if(scope.tab=='tab46'){
                var deleteRow = false;
                  var update =  function (obj, cel, val) {
                        scope.someCtrlFn();
                           var arr = $(element).jexcel('getData', false);
                           var result = arr.reduce(function(s,v){
                               var removeValFromIndex = [0,1,2,4,10];
                               for (var i = removeValFromIndex.length -1; i >= 0; i--){
                                  v.splice(removeValFromIndex[i],1);
                                }
                                //console.log('v2: ', v);
                                v.forEach(function(x,y){
                                  s[y]=(s[y] || 0)+ +x;
                                })
                                return s;
                       },[]);
                       //console.log('result: ', result);
                       result.splice(0,0,'Overall Expenditure','-','-');
                       result.splice(4,0,'-');
                       result.splice(10,0,'-');
                       //console.log('dataTotalVal: ', result);
                       $rootScope.$broadcast('updateTotal', result);
                       var id = $(cel).prop('id').split('-')
                  }
              }
              if(scope.tab=='tab18'||scope.tab=='tab15' || scope.tab=='tab33' || scope.tab=='tab42'||scope.tab=='tab43'|| scope.tab=='tab112'){
                var deleteRow = false;
                var update =  function (obj, cel, val) {
                     scope.someCtrlFn();
                          var redundantCols = $(element).find(".tableDataHeader").children('tr').children('td').length;
                          var arr = $(element).jexcel('getData', false);
                         var temp_array = [];
                         for(var loopi = 0; loopi < arr.length; loopi++) {
                           var cube = arr[loopi];
                           var tmp = [];
                           for(var loopj = 0; loopj < (arr[loopi].length - redundantCols); loopj++) {
                               tmp.push(cube[loopj]);
                           }
                           temp_array.push(tmp);
                         }
                         arr = temp_array;

                         var result = arr.reduce(function(s,v){
                           console.log("REd S",s);
                             var removeValFromIndex = [0];
                             for (var i = removeValFromIndex.length -1; i >= 0; i--){
                                v.splice(removeValFromIndex[i],1);

                              }
                              v.forEach(function(x,y){
                                s[y]=(s[y] || 0)+ +x;
                              })
                              return s;
                     },[]);
                     result.splice(0,0,'#','Overall Expenditure');
                     $rootScope.$broadcast('updateTotal', result);
                     var id = $(cel).prop('id').split('-')
                }
            }
            if(scope.tab=='tab111'){
              var deleteRow = false;
              var update =  function (obj, cel, val) {
                   scope.someCtrlFn();
                        var redundantCols = $(element).find("#header111").children('tr').children('td').length;
                        var arr = $(element).jexcel('getData', false);
                       var temp_array = [];
                       for(var loopi = 0; loopi < arr.length; loopi++) {
                         var cube = arr[loopi];
                         var tmp = [];
                         for(var loopj = 0; loopj < (arr[loopi].length - redundantCols); loopj++) {
                             tmp.push(cube[loopj]);
                         }
                         temp_array.push(tmp);
                       }
                       arr = temp_array;

                       var result = arr.reduce(function(s,v){
                         console.log("REd S",s);
                           var removeValFromIndex = [0];
                           for (var i = removeValFromIndex.length -1; i >= 0; i--){
                              v.splice(removeValFromIndex[i],1);

                            }
                            v.forEach(function(x,y){
                              s[y]=(s[y] || 0)+ +x;
                            })
                            return s;
                   },[]);
                   result.splice(0,0,'#','Overall Expenditure');
                   $rootScope.$broadcast('updateTotal', result);
                   var id = $(cel).prop('id').split('-')
              }
          }else{
                var update =  function (obj, cel, val) {
                      scope.someCtrlFn();
                      var id = $(cel).prop('id').split('-')
                    }
            }
          if(scope.tab=='tab5' || scope.tab=='tab6' || scope.tab=='tab7' || scope.tab=='tab8' || scope.tab=='tab9' || scope.tab=='tab24' || scope.tab=='tab17'){
            var deleteRow = false;
              var insertRow=false;
                var columnAttr =  [
                  { type: 'text', readOnly:true},
                  { type: 'numeric' }
                ]
                setTimeout(function(){
                  //console.log('element', element);
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 3) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab11'){
                var insertRow=false;
                  var columnAttr =  [
                    { type: 'text', readOnly:true},
                    { type: 'numeric' }
                  ]
                var deleteRow = false;
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 8) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab12'){
                var deleteRow = false;
                  var insertRow=false;
                var columnAttr =  [
                  { type: 'text', readOnly:true},
                  { type: 'text', readOnly:true},
                  { type: 'text', readOnly:true},
                  { type: 'numeric' }
                ]
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 0 || row == 1 || row == 3 || row == 5 || row == 6 || row == 7 || row == 8 || row == 10 || row == 15 || row == 16 || row == 18 || row == 22 || row == 23 || row == 24) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab13'){
                var deleteRow = false;
                  var insertRow=false;
                var columnAttr =  [
                  { type: 'text', readOnly:true},
                  { type: 'text', readOnly:true},
                  { type: 'text', readOnly:true},
                  { type: 'numeric' }
                ]
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 0 || row == 1 || row == 2 || row == 3 || row == 5 || row == 6 ) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab21'){
                var deleteRow = false;
                  var insertRow=false;
                var columnAttr =  [
                  { type: 'text', readOnly:true},
                  { type: 'text', readOnly:true},
                  { type: 'numeric', readOnly:true},
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' }
                ]
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 2 || row == 4 || row == 6 || row == 8 || row == 9 || row == 11 || row == 15 || row == 16 || row == 17) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab22'){
                var deleteRow = false;
                  var insertRow=false;
                var columnAttr =  [
                  { type: 'text', readOnly:true},
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' }
                ]
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                        if (row == 0 || row == 1) {
                          $(cell).addClass('readonly singleCol');
                        }
                        if ( row == 2 || row == 9 || row == 13 || row == 18 || row == 31) {
                            $(cell).addClass('readonly');
                        }
                      }
                  });
                })
              }
              // else if(scope.tab=='tab26'){
              //   var deleteRow = false;
              //     var insertRow=false;
              //   var columnAttr =  [
              //     { type: 'text', readOnly:true},
              //     { type: 'numeric' },
              //     { type: 'numeric' },
              //     { type: 'numeric' },
              //     { type: 'numeric' },
              //     { type: 'numeric' },
              //     { type: 'numeric' },
              //     { type: 'numeric' }
              //   ]
              //   setTimeout(function(){
              //     $(element).jexcel('updateSettings', {
              //         cells: function (cell, col, row) {
              //           if (row == 0 || row == 1 || row == 4 || row == 9) {
              //             $(cell).addClass('readonly singleCol');
              //           }
              //           if ( row == 24) {
              //               $(cell).addClass('readonly');
              //           }
              //         }
              //     });
              //   })
              // }
              else if(scope.tab=='tab27'){
                var deleteRow = false;
                  var insertRow=false;
                var columnAttr =  [
                  { type: 'text', readOnly:true},
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' }
                ]
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 3 || row == 4 || row == 7 || row == 15 || row == 16 || row == 18 || row == 38) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab32'){
                var deleteRow = false;
                  var insertRow=false;
                var columnAttr =  [
                  { type: 'text', readOnly:true},
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' }
                ]
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 4 ) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab39'){
                var deleteRow = false;
                  var insertRow=false;
                var columnAttr =  [
                  { type: 'text', readOnly:true},
                  { type: 'numeric'},
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' }
                ]
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 0 ) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab23'){
                var deleteRow = false;
    							var columnAttr = [
                    { type: "text", readOnly: true },
                    { type: 'numeric'}
                  ]
                    setTimeout(function(){
                      var deleteRow = false;
                      $(element).jexcel('updateSettings', {
                          cells: function (cell, col, row) {
                              if (row == 0) {
                                  $(cell).addClass('readonly singleCol');
                              }
                              if ( row == 2 || row == 7 || row == 12 || row == 25) {
                                  $(cell).addClass('readonly');
                              }
                          }
                      });
                    })
    					}else if(scope.tab=='tab25'){
                var deleteRow = false;
                  var insertRow=false;
    							var columnAttr = [
                    { type: "text", readOnly: true },
                    { type: "numeric", readOnly: false },
                    { type: 'numeric', readOnly: false }
                  ]
                    setTimeout(function(){
                      $(element).jexcel('updateSettings', {
                          cells: function (cell, col, row) {
                              if (row == 0 || row == 1 || row == 3 || row == 8 || row == 13) {
                                  $(cell).addClass('readonly singleCol');
                              }
                              if ( row == 26) {
                                  $(cell).addClass('readonly');
                              }
                          }
                      });
                    })
    					}else if(scope.tab=='tab30'){
                var deleteRow = false;
                  var insertRow=false;
    							var columnAttr = [
                    { type: "text", readOnly: true },
                    { type: "numeric", readOnly: false },
                    { type: 'numeric', readOnly: false }
                  ]
    					}else if(scope.tab=='tab59-1' || scope.tab=='tab59-2' || scope.tab=='tab59-3'){
                var deleteRow = false;
    							var columnAttr = [
                    { type: "text" },
                    { type: "numeric" },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' }
                  ]
                    setTimeout(function(){
                      $(element).jexcel('updateSettings', {
                          cells: function (cell, col, row) {
                              if (row == 0) {
                                  $(cell).addClass('readonly singleCol');
                              }
                          }
                      });
                    })
    					}else if(scope.tab=='tab38'){
                var deleteRow = false;
                  var insertRow=false;
    							var columnAttr = [
                    { type: "text", readOnly: true },
                    { type: "numeric", readOnly: false },
                    { type: 'text', readOnly: false }
                  ]
                    setTimeout(function(){
                      $(element).jexcel('updateSettings', {
                          cells: function (cell, col, row) {
                              if (row == 0 || row == 8) {
                                  $(cell).addClass('readonly singleCol');
                              }
                          }
                      });
                    })
    					}
              else if(scope.tab=='tab50'){
                var deleteRow = false;
                  var insertRow=false;
                var columnAttr =  [
                  { type: 'text', readOnly:true},
                  { type: 'text', readOnly:true},
                  { type: 'numeric' }
                ]
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 0 || row == 2) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
              }
              else if(scope.tab=='tab51'){
                var deleteRow = false;
                  var insertRow=false;
                var columnAttr =  [
                  { type: 'text', readOnly:true},
                  { type: 'text', readOnly:true},
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' }
                ]
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 4 || row == 6 || row == 16 || row == 17 || row == 18) {
                                $(cell).addClass('readonly');
                            }
                            if (row == 0 || row == 7 || row == 13) {
                                $(cell).addClass('readonly singleCol');
                            }
                        }
                    });
                  })
              }
              else if(scope.tab=='tab58'){
                var deleteRow = false;
                  var insertRow=false;
    							var columnAttr = [
                    { type: "text", readOnly: true },
                    { type: "numeric", readOnly: false },
                    { type: 'numeric', readOnly: false }
                  ]
                    setTimeout(function(){
                      $(element).jexcel('updateSettings', {
                          cells: function (cell, col, row) {
                              if (row == 0 || row == 3 || row == 6  || row == 9 || row == 12) {
                                  $(cell).addClass('readonly singleCol');
                              }
                              if (row == 2 || row == 5 || row == 8 || row == 11 || row == 14) {
                                  $(cell).addClass('readonly');
                              }
                          }
                      });
                    })
    					}else if(scope.tab=='tab26'){
                var deleteRow = false;
                  var insertRow=false;
    							var columnAttr = [
                    { type: "text", readOnly: true },
                    { type: 'numeric' }
                  ]
                    setTimeout(function(){
                      $(element).jexcel('updateSettings', {
                          cells: function (cell, col, row) {
                              if (row == 0 || row == 1 || row == 4 || row == 9) {
                                  $(cell).addClass('readonly singleCol');
                              }
                              if ( row == 24) {
                                  $(cell).addClass('readonly');
                              }
                          }
                      });
                    })
    					}
              else if(scope.tab=='tab56'){
                var insertRow=true;
                  var deleteRow = false;
                var columnAttr =  [
                  { type: 'text', readOnly:false },
                  { type: 'calendar', options: { format:'YYYY/MM/DD'} },
                  { type: 'text' },
                  { type: 'calendar', options: { format:'YYYY/MM/DD'} },
                ]
              }
              else if(scope.tab=='tab62'){
                var deleteRow = false;
                  var insertRow=false;
    							var columnAttr = [
                    { type: "text", readOnly: true }
                  ]
                    setTimeout(function(){
                      $(element).jexcel('updateSettings', {
                          cells: function (cell, col, row) {
                              if (row == 5) {
                                  $(cell).addClass('readonly');
                              }
                          }
                      });
                    })
    					}
              else if(scope.tab=='tab28'){
                var deleteRow = false;
                  var insertRow=false;
              var columnAttr =  [
                { type: 'text', readOnly:true },
                { type: 'numeric' },
                { type: 'numeric' },
                { type: 'numeric',}
              ]
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 4 ) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab29'){
                var deleteRow = false;
                  var insertRow=false;
                var columnAttr =  [
                  { type: 'text', readOnly:true },
                  { type: 'numeric'},
                  { type: 'numeric'},
                  { type: 'numeric'},
                  { type: 'numeric'},
                  { type: 'numeric'}
                ]
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 4 || row == 6) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab30'){
                var deleteRow = false;
                  var insertRow=false;
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 8) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }

              else if(scope.tab=='tab40'){
                var deleteRow = false;
                  var insertRow=false;
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 4) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab5-1' || scope.tab=='tab8-1'  || scope.tab=='tab10-1' || scope.tab=='tab9-1' || scope.tab=='tab46-1' ){
                var insertRow=false;
                  var deleteRow = false;
      					var columnAttr = [{ type: "text", readOnly:true },
                { type: "text", readOnly:true },{ type: "text", readOnly:true },{ type: "text", readOnly:true },{ type: "text", readOnly:true },{ type: "text", readOnly:true },{ type: "text", readOnly:true },{ type: "text", readOnly:true },{ type: "text", readOnly:true }];
      				}else if(scope.tab=='tab42' || scope.tab=='tab43'){
                var insertRow=true;
                  var deleteRow = false;
                var columnAttr =  [
                  { type: 'text', readOnly:false },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' }
                ]
              }
              else if(scope.tab=='tab55'){
                var deleteRow = false;
                  var insertRow=false;
                var columnAttr =  [
                  { type: 'text', readOnly:true},
                  { type: 'text', readOnly:true},
                  { type: 'text', readOnly:true},
                ]
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 0 ||row == 22 || row == 27 || row == 28 || row == 29 || row == 30 || row == 31 || row == 32) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab104'){
                var insertRow=true;
                  var deleteRow = false;
                var columnAttr =  [
                  { type: 'text', readOnly:false },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric', readOnly:true}
                ]
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 30) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }else if(scope.tab=='tab107'){
                var insertRow=true;
                  var deleteRow = false;
                var columnAttr =  [
                  { type: 'text', readOnly:false },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric', readOnly:true},
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric', readOnly:true},
                  { type: 'numeric', readOnly:true}
                ]
              }else if(scope.tab=='tab48'){
                var insertRow=false;
                  var deleteRow = false;
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 2 || row == 5 || row == 6 || row == 7 || row == 8 || row == 10 || row == 11 || row == 12 || row == 14 || row == 15 ||row == 16 ) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  });
                var columnAttr =  [
                  { type: 'text', readOnly:true },
                  { type: 'text', readOnly:true },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' },
                  { type: 'numeric' }
                ]
              }else if(scope.tab=='tab49'){
                var insertRow=false;
                  var deleteRow = false;
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 0) {
                                $(cell).addClass('readonly singleCol');
                            }
                            if (row == 3 ) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
              }else if(scope.tab=='tab35' || scope.tab=='tab36'){
                var insertRow=false;
                  var deleteRow = false;
              var columnAttr =  [
                { type: 'text', readOnly:true },
                { type: 'numeric',  },
                { type: 'numeric', },
                { type: 'numeric' },
                { type: 'numeric' },
                { type: 'numeric' },
                { type: 'numeric' },
                { type: 'numeric' },
                { type: 'numeric' },
                { type: 'numeric' }
              ]
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 1 || row == 5 ) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
              }else if(scope.tab=='tab41'){
                var insertRow=false;
                  var deleteRow = false;
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 2 || row == 5 || row == 6) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
              }else if(scope.tab=='tab44'){
                var insertRow=false;
                  var deleteRow = false;
                var columnAttr =  [
                  { type: 'text', readOnly:true },
                  { type: 'numeric',  }
                ]
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 0 || row == 1) {
                                $(cell).addClass('readonly singleCol');
                            }
                            if (row == 9 || row == 10 || row == 15 ) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
              }else if(scope.tab=='tab47'){
                var insertRow=false;
                  var deleteRow = false;
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 3 || row == 6 || row == 7 || row == 8 || row == 10 || row == 11 || row == 12 ) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
              }else if(scope.tab=='tab51'){
                var insertRow=false;
                  var deleteRow = false;
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 4 || row == 6 || row == 16 || row == 17 || row == 18) {
                                $(cell).addClass('readonly');
                            }
                            if (row == 0 || row == 7 || row == 13) {
                                $(cell).addClass('readonly singleCol');
                            }
                        }
                    });
                  })
              }else if(scope.tab=='tab54'){
                var insertRow=false;
                  var deleteRow = false;
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 14 ) {
                                $(cell).addClass('readonly');
                            }
                            if (row == 0) {
                                $(cell).addClass('readonly singleCol');
                            }
                        }
                    });
                  })
              }else if(scope.tab=='tab124'){
                var insertRow=false;
                  var deleteRow = false;
                var columnAttr =  [
                  { type: 'text', readOnly:true },
                  { type: 'numeric'  },
                ]
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 4 || row == 8 || row == 16 || row == 17 || row == 26 || row == 33) {
                                $(cell).addClass('readonly');
                            }
                            if (row == 0 || row == 5 || row == 9 || row == 10 || row == 19 || row == 27) {
                                $(cell).addClass('readonly singleCol');
                            }
                        }
                    });
                  })
              }else if(scope.tab=='tab125' || scope.tab=='tab125-1'){
                var insertRow=true;
                var deleteRow = true;
    							var columnAttr = [
                    { type: 'dropdown',source:$localStorage.projectCombo},
                    { type: 'text' },
                    { type: 'numeric' },
                  ]
              }else if(scope.tab=='tab45-0'){
                var insertRow=true;
                var deleteRow = false;
    							var columnAttr = [
                    { type: 'dropdown',source:$localStorage.stationsCombo, readOnly:false},
                    { type: 'dropdown',source:$localStorage.expenditureCombo},
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                  ]
              }else if(scope.tab=='tab45-1'||scope.tab=='tab45-2'){
                var insertRow=true;
                var deleteRow = false;
    							var columnAttr = [
                    { type: 'text', readOnly:false},
                    { type: 'dropdown',source:$localStorage.expenditureCombo},
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                  ]
              }else if(scope.tab=='tab34'){
                var insertRow=false;
                  var deleteRow = false;
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 2 || row == 4 || row == 5) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
              }else if(scope.tab=='tab1-12'){
                var insertRow=false;
                  var deleteRow = false;
                var columnAttr =  [
                  { type: 'numeric', readOnly:false }
                ]
              }else if(scope.tab=='tab31'){
                var insertRow=false;
                  var deleteRow = false;
                var columnAttr =  [
                  { type: 'text', readOnly:false },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:false }
                ]
                setTimeout(function(){
                  $(element).jexcel('updateSettings', {
                      cells: function (cell, col, row) {
                          if (row == 30) {
                              $(cell).addClass('readonly');
                          }
                      }
                  });
                })
              }
              else if(scope.tab=='tab33'){
                var insertRow=true;
                  var deleteRow = false;
                var columnAttr =  [
                  { type: 'text', readOnly:false },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:false }
                ]
              }else if(scope.tab=='tab117'){
                var insertRow=false;
                  var deleteRow = false;
                var columnAttr =  [
                  { type: 'text', readOnly:true },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:true },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:true },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:false },
                  { type: 'numeric', readOnly:true }
                ]
              }
    					else if(scope.tab=='tab105'){
                var insertRow=true;
                  var deleteRow = false;
    							var columnAttr = [
                    { type: "text" },
                    { type: 'dropdown',source:$localStorage.voltages},
                    { type: 'numeric' },
                    { type: 'numeric' },
                  ]
    					}
    					else if(scope.tab=='tab108' || scope.tab=='tab109' || scope.tab=='tab111' || scope.tab=='tab15' || scope.tab=='tab18'){
                var insertRow=true;
                  var deleteRow = false;
    							var columnAttr = [
                    { type: "text" },
                    { type: 'numeric' },
                    { type: 'numeric' },
                  ]
    					}
    					else if(scope.tab=='tab110'){
                var insertRow=true;
                  var deleteRow = false;
    							var columnAttr = [
                    { type: "text", readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: true },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: true },
                    { type: 'numeric', readOnly: true },
                  ]
    					}
    					else if(scope.tab=='tab113'||scope.tab=='tab106'){
                var insertRow=false;
    							var columnAttr = [
                    { type: "text", readOnly: true },
                    { type: "text", readOnly: true },
                    { type: 'numeric', readOnly: true }
                  ]
                    setTimeout(function(){
                      $(element).jexcel('updateSettings', {
                          cells: function (cell, col, row) {
                              if (row == 0 || row == 6 || row == 11) {
                                  $(cell).addClass('readonly singleCol');
                              }
                          }
                      });
                    })
    					}
    					else if(scope.tab=='tab112'){
                var insertRow=true;
                  var deleteRow = false;
    							var columnAttr = [
                    { type: "text", readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: true }
                  ]
    					}
    					else if(scope.tab=='tab115' || scope.tab=='tab120' || scope.tab=='tab121'){
                var insertRow=true;
                  var deleteRow = false;
    							var columnAttr = [
                    { type: "text", readOnly: false },
                    { type: 'numeric'},
                    { type: 'numeric' },
                    { type: 'numeric' },
                  ]
    					}
    					else if(scope.tab=='tab10'){
                var insertRow=false;
                  var deleteRow = false;
    							var columnAttr = [
                    { type: "text", readOnly: true },
                    { type: 'numeric'},
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric'},
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric'},
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric'},
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric', readOnly: true },
                  ]
    					}
    					else if(scope.tab=='tab123'){
                var insertRow=false;
                  var deleteRow = false;
    							var columnAttr = [
                    { type: "text", readOnly: true },
                    { type: 'numeric'},
                    { type: 'numeric' },
                    { type: 'numeric', readOnly: true },
                  ]
    					}
    					else if(scope.tab=='tab46'){
                var insertRow=true;
                  var deleteRow = false;
    							var columnAttr = [
                    { type: "text" },
                    { type: 'dropdown',source:$localStorage.expenditures},
                    { type: 'calendar', options: { format:'YYYY/MM/DD'} },
                    { type: 'numeric' },
                    { type: 'calendar', options: { format:'YYYY/MM/DD'} },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'calendar', options: { format:'YYYY/MM/DD'} },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                  ]
    					}
    					else if(scope.tab=='tab57'){
                var insertRow=true;
                  var deleteRow = false;
    							var columnAttr = [
                    { type: "text" },
                    { type: 'calendar', options: { format:'YYYY/MM/DD'} },
                    { type: 'calendar', options: { format:'YYYY/MM/DD'} },
                    { type: 'dropdown',source:['planned', 'non planned']},
                    { type: 'numeric' },
                    { type: 'text' },
                    { type: 'text' },
                    { type: 'text' },
                    { type: 'text' },
                    { type: 'text' },
                    { type: 'text' }
                  ]
    					}
              else if(scope.tab=='tab78'||scope.tab=='tab79'||scope.tab=='tab80'||scope.tab=='tab81'||scope.tab=='tab91'||scope.tab=='tab92'||scope.tab=='tab93'){
                var datainsert = scope.values.isInsert;
                var dataVal = scope.$parent.getDataByForm(scope.uid,scope.tab,scope.values);
                  var insertRow=true;
                    var deleteRow = false;
                   var columnAttr =  [
                      { type: 'text', readOnly:false },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                  ]
               if(datainsert == "false"){
                   var insertRow=false;
                     var deleteRow = false;
                   var columnAttr =  [
                      { type: 'text', readOnly:true },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                      { type: 'numeric', },
                  ]
               }
              }

    					else if(scope.tab=='tab85'||scope.tab=='tab86'||scope.tab=='tab98'||scope.tab=='tab99'){
                  var insertRow=true;
                    var deleteRow = false;
    							var columnAttr = [
                    { type: "text", readOnly: false },
                    { type: "numeric", readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false }
                  ]
    					}
    					else if(scope.tab=='tab100'||scope.tab=='tab87'){
                  var insertRow=false;
                    var deleteRow = false;
    							var columnAttr = [
                    { type: "text", readOnly: true },
                    { type: "text", readOnly: true },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric', readOnly: false },
                    { type: "numeric", readOnly: true }
                  ]
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 4 || row == 5) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
    					}
    					else if(scope.tab=='tab19'){
                var deleteRow = false;
                  var insertRow=false;
    							var columnAttr = [
                    { type: "text", readOnly: true },
                    { type: "text", readOnly: true },
                    { type: 'text', readOnly:true},
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' }
                  ]
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 0 || row == 2 || row == 3 || row == 8 || row == 13 || row == 20 || row == 22 || row == 25 || row == 29 || row == 33) {
                                $(cell).addClass('readonly singleCol');
                            }else if (row == 7 || row == 12 || row == 17 || row == 18|| row == 19) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
    					}
    					else if(scope.tab=='tab14'){
                var deleteRow = false;
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 0 || row == 17) {
                                $(cell).addClass('readonly singleCol');
                            }else if (row == 1 || row == 2 || row == 3 || row == 4 || row == 5 || row == 6 || row == 7 || row == 8 || row == 9 || row == 10 || row == 11 || row == 12 || row == 13 || row == 14 || row == 15 || row == 16 || row == 18 || row == 19 || row == 20 || row == 21 || row == 22 || row == 23) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
    					}
    					else if(scope.tab=='tab20'){
                var deleteRow = false;
                  var insertRow=false;
    							var columnAttr = [
                    { type: "text", readOnly: true },
                    { type: "text", readOnly: true },
                    { type: 'text', readOnly:true},
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' }
                  ]
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 0 || row == 2 || row == 4 || row == 5 || row == 10 ) {
                                $(cell).addClass('readonly singleCol');
                            }else if (row == 1 || row == 3 || row == 6 || row == 7 || row == 8 || row == 9 || row == 11 || row == 12 || row == 13 || row == 14 || row == 15 || row == 16 || row == 17 || row == 18 || row == 19 || row == 20 || row == 21 || row == 22 || row == 23 || row == 28) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
    					}
               else if(scope.tab=='tab96'){
                   //  var datainsert = scope.values.isInsert;
                   //    var insertRow=true;
                   //      var deleteRow = false;
                   //     var columnAttr =  [
                   //        { type: 'text', readOnly:false },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //    ]
                   // if(datainsert == "false"){
                   //     var insertRow=false;
                   //       var deleteRow = false;
                   //     var columnAttr =  [
                   //        { type: 'text', readOnly:true },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //        { type: 'numeric', },
                   //    ]
                   // }
               }
    					else if(scope.tab=='tab84'||scope.tab=='tab97'){
                var deleteRow = false;
    							var columnAttr = [
                    { type: 'numeric', readOnly: false },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' }
                  ]
    					}
    					else if(scope.tab=='tab63'){
                  var insertRow=false;
                    var deleteRow = false;
    							var columnAttr = [
                    { type: 'numeric', readOnly: true },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric' },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: false  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: false  }
                  ]
    					}
    					else if(scope.tab=='tab16'){
                var deleteRow = false;
                  var insertRow=false;
    							var columnAttr = [
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: false  },
                  ]
                  setTimeout(function(){
                    //console.log('element', element);
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 3) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
    					}
    					else if(scope.tab=='tab16-2'){
                var deleteRow = false;
                  var insertRow=false;
    							var columnAttr = [
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  }
                  ]
                  setTimeout(function(){
                    //console.log('element', element);
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 3) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
    					}
    					else if(scope.tab=='tab122-1'){
                var deleteRow = false;
    							var columnAttr = [
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  }
                  ]
    					}
    					else if(scope.tab=='tab122-2'){
                var deleteRow = false;
    							var columnAttr = [
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  }
                  ]
    					}
    					else if(scope.tab=='tab122' ){
                var deleteRow = false;
    							var columnAttr = [
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: false  },
                    { type: 'numeric', readOnly: false  },
                    { type: 'numeric', readOnly: false  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: false  },
                    { type: 'numeric', readOnly: false  },
                    { type: 'numeric', readOnly: false  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: false  },
                    { type: 'numeric', readOnly: false  },
                    { type: 'numeric', readOnly: false  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: false  },
                    { type: 'numeric', readOnly: false  },
                    { type: 'numeric', readOnly: true  },
                    { type: 'numeric', readOnly: true  }
                  ]
                  setTimeout(function(){
                    $(element).jexcel('updateSettings', {
                        cells: function (cell, col, row) {
                            if (row == 24 ) {
                                $(cell).addClass('readonly');
                            }
                        }
                    });
                  })
    					}
              else{
                  var insertRow=false;
                //  var deleteRow = false;
                  var columnAttr =  [
                     { type: 'text', readOnly:true },
                     { type: 'numeric', },
                     { type: 'numeric', },
                     { type: 'numeric', },
                     { type: 'numeric', },
                     { type: 'numeric', },
                     { type: 'numeric', },
                     { type: 'numeric', },
                     { type: 'numeric', },
                     { type: 'numeric', },
                     { type: 'numeric', },
                     { type: 'numeric', },
                 ]
               }
    					$(element).attr('id', scope.tab+scope.uid);
              var dataVal = scope.$parent.getDataByForm(scope.uid,scope.tab,scope.values);
              if(scope.tab=='tab77-1' || scope.tab=='tab78-1' || scope.tab=='tab79-1' || scope.tab=='tab80-1' || scope.tab=='tab91-1' || scope.tab=='tab92-1' || scope.tab=='tab93-1' || scope.tab=='tab90-1'){
                var dataVal = scope.$parent.getDataByForm1(scope.uid,scope.tab,scope.values);
              }else if(scope.tab=='tab77-2' || scope.tab=='tab78-2' || scope.tab=='tab79-2' || scope.tab=='tab80-2' || scope.tab=='tab91-2' || scope.tab=='tab92-2' || scope.tab=='tab93-2' || scope.tab=='tab90-2'){
                var dataVal = scope.$parent.getDataByForm2(scope.uid,scope.tab,scope.values);
              }else if(scope.tab=='tab59-1'){
                var dataVal = scope.$parent.getDataByForm(scope.uid,scope.tab,scope.values.vals1);
              }else if(scope.tab=='tab59-2'){
                var dataVal = scope.$parent.getDataByForm(scope.uid,scope.tab,scope.values.vals2);
              }else if(scope.tab=='tab59-3'){
                var dataVal = scope.$parent.getDataByForm(scope.uid,scope.tab,scope.values.vals3);
              }
               else if(scope.tab=='tab94' || scope.tab=='tab73' || scope.tab=='tab74' || scope.tab=='tab75' || scope.tab=='tab103'){
                    var datainsert = scope.values.isInsert;
                    var dataVal = scope.$parent.getDataByForm(scope.uid,scope.tab,scope.values);
                      var insertRow=true;
                       var columnAttr =  [
                          { type: 'text', readOnly:false },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                      ]
                   if(datainsert == "false"){
                       var insertRow=false;
                       var columnAttr =  [
                          { type: 'text', readOnly:true },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                          { type: 'numeric', },
                      ]
                   }
               }
               else if(scope.tab=='tab76'){
                    var datainsert = scope.values.isInsert;
                    var dataVal = scope.$parent.getDataByForm(scope.uid,scope.tab,scope.values);
                      var insertRow=true;
                       var columnAttr =  [
                          { type: 'text', readOnly:false },
                          { type: 'numeric' },
                          { type: 'numeric' },
                          { type: 'text'}
                      ]
                   if(datainsert == "false"){
                       var insertRow=false;
                       var columnAttr =  [
                          { type: 'text', readOnly:false },
                          { type: 'numeric' },
                          { type: 'numeric' },
                          { type: 'text'}
                      ]
                   }
               }
               else if(scope.tab=='tab72'){
                 var columnAttr =  [
                    { type: 'text', readOnly:true },
                    { type: 'numeric', readOnly:true  },
                    { type: 'numeric', readOnly:true  },
                    { type: 'numeric', readOnly:true },
                    { type: 'numeric', readOnly:true },
                    { type: 'numeric', readOnly:true },
                    { type: 'numeric', readOnly:true },
                    { type: 'numeric', readOnly:true },
                    { type: 'numeric', readOnly:true }
                ]
               }
               else if(scope.tab=='tab66'){
                       var columnAttr =  [
                          { type: 'text', readOnly:true },
                          { type: 'numeric' },
                          { type: 'numeric' },
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'}
                      ]
                      setTimeout(function(){
                        $(element).jexcel('updateSettings', {
                            cells: function (cell, col, row) {
                                if (row == 0 || row == 1 || row == 3 || row == 4 || row == 5 || row == 6 || row == 7 || row == 8 || row == 10 || row == 11 || row == 13 || row == 15 || row == 16 || row == 18 || row == 21 || row == 22 || row == 23) {
                                    $(cell).addClass('readonly');
                                }
                            }
                        });
                      })
               }
               else if(scope.tab=='tab67'){
                       var columnAttr =  [
                          { type: 'text', readOnly:true },
                          { type: 'text', readOnly:true },
                          { type: 'text', readOnly:true },
                          { type: 'numeric' },
                          { type: 'numeric' },
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'}
                      ]
                      setTimeout(function(){
                        $(element).jexcel('updateSettings', {
                            cells: function (cell, col, row) {
                              if (row == 2 || row == 25 || row == 28 || row == 29 || row == 30 || row == 31 || row == 32 || row == 37 ) {
                                    $(cell).addClass('readonly');
                                }
                            }
                        });
                      })
               }
               else if(scope.tab=='tab71'){
                       var columnAttr =  [
                          { type: 'text', readOnly:true },
                          { type: 'text', readOnly:true },
                          { type: 'text', readOnly:true },
                          { type: 'numeric' },
                          { type: 'numeric' },
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'}
                      ]
                      setTimeout(function(){
                        $(element).jexcel('updateSettings', {
                            cells: function (cell, col, row) {
                                if (row == 0 || row == 5 ) {
                                    $(cell).addClass('readonly singleCol');
                                }else if (row == 25 || row == 28 || row == 29 || row == 30 || row == 31 || row == 35) {
                                    $(cell).addClass('readonly');
                                }
                            }
                        });
                      })
               }
               else if(scope.tab=='tab83' || scope.tab=='tab96'){
                    var insertRow=false;
                    var columnAttr =  [
                          { type: 'text', readOnly:true},
                          { type: 'text', readOnly:true },
                          { type: 'text', readOnly:true },
                          { type: 'numeric' },
                          { type: 'numeric' },
                          { type: 'numeric'}
                    ]

                    var datainsert = scope.values.valAll[scope.uid].isInsert;
                    if(datainsert=="true"){
                        var rowSet = 6;
                        var temp = 0;
                        setTimeout(function(){
                            $(element).jexcel('updateSettings', {
                                cells: function (cell, col, row) {
                                  if ((row == 0 || row == temp) && col == 0  ) {
                                      $(cell).removeClass('readonly ');
                                      temp = parseInt(row) + parseInt(rowSet);
                                  }
                                }
                            });
                          })
                        }
               }
               else if(scope.tab=='tab114'){
                       var columnAttr =  [
                          { type: 'text', readOnly:true },
                          { type: 'numeric' },
                          { type: 'numeric' },
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'text'}
                      ]
               }
               else if(scope.tab=='tab116'){
                       var columnAttr =  [
                          { type: 'text', readOnly:true },
                          { type: 'numeric' },
                          { type: 'numeric' },
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'numeric'},
                          { type: 'text'}
                      ]
               }
    					  $(element).jexcel({
                   format:'YYYY/MM/DD',
                   data: dataVal,
                   onchange:update,
                   csvHeaders:false,
    	             columns:columnAttr,
                   colAlignments:[],
                   minSpareRows:0,
                   minSpareCols:0,
                   allowInsertRow:insertRow,
                   allowDeleteRow:deleteRow,
                   allowInsertColumn:false,
                   allowDeleteColumn:false
                  });
                }, true);
             }
             loadDirective();
             //$timeout(loadDirective, 10000);
         }
      };
}]);

})();
