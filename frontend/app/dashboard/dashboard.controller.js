(function() {
  'use strict';
  var app = angular.module('mean');
  app.controller('DashboardController', ['$scope','$location','ApiServices','$filter','$timeout','$uibModal','DashboardServices','$rootScope', '$stateParams', 'taskId','utilId','NewpetitionServices','$state', '$localStorage' ,'$window',function($scope,$location,ApiServices,$filter,$timeout,$uibModal,DashboardServices,$rootScope, $stateParams, taskId,utilId,NewpetitionServices,$state,$localStorage,$window){

// var tokenValue =  localStorage.getItem('token');
// $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
//   if(tokenValue==undefined || tokenValue==null){
//         event.preventDefault();
//         return $state.go('login');
//     }
//
//     return;
// });

$scope.init = function(){
  var tokenValue =  localStorage.getItem('token');
  if(tokenValue==undefined || tokenValue==null){
    $state.go('login');
  }else if(tokenValue!=undefined || tokenValue!=null){
    $scope.user = {};
    $scope.dashboardSidebar = '';
    $scope.selectedTab = 'tab1';
    $scope.subStations = {};
    $scope.subUnitsData = {};
    $scope.subStationsData = [];
    $scope.subStationsData3 = [];
    $scope.showPopup = false;
    $scope.totalExpenditure = '';
	  $scope.is_show = 'no';

	$scope.stationheaderInfo = {
		'stationName':'',
		'capacity':'',
		'unit':'',
	};

    $scope.comments = {
      'comment':'',
      'form_id':'',
      'ph_id':'',
      'user_id':'',
    };
    $scope.attachment = {
      'comment':'',
      'form_ref':'',
      'file':'',
    };
    $scope.grandTotal118c=[];
    $scope.globalfiles = [];


	if($localStorage.petitionId==undefined || $localStorage.petitionId == ""){
			$rootScope.message = 'You Cannot access the link directly. Please create a new petition first.';
			 $location.path('/dashboard');
	}
	var Phid = $localStorage.petitionId;
	var utilid = $localStorage.utilId;
	$scope.petitionId = $localStorage.petitionId;
	$scope.user = ApiServices.getAuthUser();
      var valUser = JSON.parse(localStorage.getItem("user"));



  /*menu start*/
     $scope.utilid = valUser.utilid;
      $scope.allMenu = valUser.menu_status;
      var showState;
      for(var i =0; i<$scope.allMenu.length; i++){
        showState = $scope.allMenu[i].state;
      }
      if(showState=='1'){
        $scope.showMenu == true;
      }else if(showState=='0'){
        $scope.showMenu == !$scope.showMenu;
      }


    $scope.start_process = function(){
		var post = {"variables":
							{"petitionversion":
							{"type":"String","value":"1","valueInfo":{}
					},
							"petitionyear":{"type":"String","value":"2017-18","valueInfo":{}},
							"petitionid":{"type":"String","value":Phid,"valueInfo":{}
							}
						}
					};
		ApiServices.startProcess(post,function(data){
			if(data.definitionId){
				$rootScope.message = 'Your Petition has been sent Successfully.';
					localStorage.removeItem('ngStorage-petitionId');
					localStorage.removeItem('ngStorage-utilId');
					localStorage.removeItem('ngStorage-taskId');
				$state.go('dashboard',null);
			}
		});

	}

  var dashboardPetitionApi = function(){
      var ApiEndPoints = '/dashboard_petition_details?user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
            $scope.petitionDetails =  data.data.petitions;
       });
    }

  dashboardPetitionApi();

/******Logout*******/
$scope.logout = function(){
  //console.log('logout');
  ApiServices.logout(function(_data){
    if(_data.status == '1'){
    //  console.log('logout',_data.message);
      $window.localStorage.clear();
      $location.path('/login')
    }else{
      $scope.error.msg = _data.message;
    }
  });
}
/******Logout*******/
$scope.showDashboardSidebar = function(setter){
  if($scope.dashboardSidebar == setter)
    $scope.dashboardSidebar = '';
  else
  $scope.dashboardSidebar = setter;
};
$scope.collapseDashboardSidebar = function(){
  $scope.collapseSidebar = 1;
};




$scope.ctrlFn= function(){
    var raw = angular.copy($scope.subStationsData);
    var calcArray = raw.vals;
  //  console.log('raw.vals: ',raw.vals);
    var result = calcArray.reduce(function(r,o){
    //  console.log('o.usage: ', o.usage.pop());
    	o.usage.forEach(function(a){
    //    console.log('a.slice(1): ', a.slice(1));
      		  a.slice(1).forEach(function(v,i){
      			   r[i] = (r[i] || 0 ) + parseFloat(v);
          	});
        });
    	return r;
  },[]);
//  console.log(result);
  $scope.totalArray = result;
  $scope.$apply();
}
$scope.grandTotalFn= function(){
    var raw = angular.copy($scope.subStationsData);
    var calcArray = raw.val;
//  console.log('raw.val: ',raw.val);
    var result =   Object.keys(calcArray).reduce(function (r, o) {
           calcArray[o].pop();
           calcArray[o].forEach(function(a){
             var x=a.slice(1);
          		  x.forEach(function(v,i){
          			   r[i] = (r[i] || 0 ) + parseFloat(v);
              	});
            });
        	return r;
      }, []);
//  console.log(result);
  $scope.totalArray = result;
  $scope.$apply();
}
$scope.arrayTotalFn= function(){
    var raw = angular.copy($scope.subStationsData);
    var calcArray = raw.vals;
    var result =  calcArray.reduce(function (r, o) {
           calcArray.forEach(function(a){
             var x=a.slice(1);
          		  x.forEach(function(v,i){
          			   r[i] = (r[i] || 0 ) + parseFloat(v);
              	});
            });
        	return r;
      }, []);
//  console.log(result);
  $scope.totalArray = result;
  $scope.$apply();
}
$scope.addRowTotal= function(){
  var raw = angular.copy($scope.subStationsData);
  var calcArray = raw.vals;
  var result = calcArray.reduce(function(r, o) {
       calcArray.forEach(function(a){
         var x=a.slice(1);
      //   console.log('a: ', x);
            x.forEach(function(v,i){
              x[i] = (x[i] || 0 ) + parseFloat(v);
                return x;
            });
        });
    //  return r;
  },[]);
  $scope.totalArray = result;
  $scope.$apply();
}
/*************Add attachment****************/

var attachFileListApi = function(){
 ApiServices.attachFileList().then(
   function(data){
       $scope.attachFileList = data.data.fileList;
   },
   function(){
  //   console.log('data can not be retrieved');
   }
 )
};

attachFileListApi();

$scope.docIdFn = function(docId){
    var url = "http://182.75.177.246:8181/wberc_v2/api/file_delete_by_id?docId="+docId;
    ApiServices.getFileDownload(url,function(data){
    //  console.log('$scope.docIdFn',data.status);
      if(data.status==1){
        $scope.loadGlobalFiles();
        $rootScope.loadAllAttachFiles();
      }
    });
};

 $scope.loadGlobalFiles = function(){
    $scope.globalfiles = [];
    var getParam = {
      phid:parseInt(localStorage.getItem("ngStorage-petitionId")),
      utilid: parseInt(localStorage.getItem('ngStorage-utilId')),
      module:'arr',
    };
    ApiServices.GetGlobalFiles(getParam, function(data){
      $scope.globalfiles = data;
      var formList = $localStorage.sidebarMenuList;
      for(var i=0; i<data.length; i++){
        for(var j=0; j<formList.length; j++){
          if(data[i].formId==formList[j].formid){
            data[i].form_code=formList[j].form_code;
          }
        }
      }
    })
 };

 $scope.add_attachment = function(){
	  var modalInstance = $uibModal.open({
          templateUrl: 'app/dashboard/views/attachment/attachment_list.html',
          size:"lg",
          scope: $scope,
          controller: ModalInstanceCtrl,
          resolve: {
            attachment: function () {
              return $scope.attachment;
            }
          }
        });
 };
 var ModalInstanceCtrl = function ($scope, $uibModalInstance, attachment) {
    $scope.attachment = {};
    $scope.closeModal = function(){
      $uibModalInstance.dismiss('cancel');
    }
    $scope.submit_attachment = function(){
      $rootScope.loadSuccessfull = false;
      if($scope.attachment.file != undefined  && $scope.attachment.comment != undefined && $scope.attachment.typeRef != undefined){
        $rootScope.validateFile = false;
        $rootScope.validateDescription = false;
        $rootScope.validateTyperef = false;
        var url = "http://182.75.177.246:8181/wberc_v2/api/file_upload_v2";
        var formdata = new FormData();
        formdata.append('file', $scope.attachment.file);
        formdata.append('docName', $scope.attachment.file.name);
        formdata.append('description', $scope.attachment.comment);
        formdata.append('petitionHeaderId', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
        formdata.append('AttachmentType', $scope.attachment.attachment_type_desc)
        formdata.append('formID', $scope.attachment.typeRef);
        formdata.append('utilid', parseInt(localStorage.getItem('ngStorage-utilId').replace(/['"]+/g, '')));
        formdata.append('module', 'ARR');
        formdata.append('user_id', parseInt(localStorage.getItem("ngStorage-userId").replace(/['"]+/g, '')));
        ApiServices.PostGlobalFileUpload(url, formdata,function(data){
            $rootScope.loadSuccessfull = true;
          $scope.loadGlobalFiles();
          $rootScope.loadAllAttachFiles();
          $uibModalInstance.dismiss('cancel');
        });
      }
      if($scope.attachment.file == undefined || $scope.attachment.file == ''){
          $rootScope.loadSuccessfull = true;
          $rootScope.validateFile = true;
      }
      if($scope.attachment.comment == undefined || $scope.attachment.comment == ''){
          $rootScope.loadSuccessfull = true;
          $rootScope.validateDescription = true;
      }
      if($scope.attachment.typeRef == undefined || $scope.attachment.typeRef == ''){
          $rootScope.loadSuccessfull = true;
          $rootScope.validateTyperef = true;
      }
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
};

$scope.submit_attachment_Form = function(){
  if($scope.attachment.file != undefined){
    var url = "http://182.75.177.246:8181/wberc_v2/api/file_upload_v2";
    var formdata = new FormData();
    formdata.append('file', $scope.attachment.file);
    formdata.append('docName', $scope.attachment.file.name);
    formdata.append('description', $scope.attachment.comment);
    formdata.append('AttachmentType', $scope.attachment.attachment_type_desc);
    formdata.append('petitionHeaderId', parseInt(localStorage.getItem("ngStorage-petitionId").replace(/['"]+/g, '')));
    formdata.append('formID', $scope.attachment.typeRef);
    formdata.append('utilid', parseInt(localStorage.getItem('ngStorage-utilId').replace(/['"]+/g, '')));
    formdata.append('module', 'ARR');
    ApiServices.PostGlobalFileUpload(url, formdata,function(data){
      alert(data.message);
      $scope.loadGlobalFiles();
      $rootScope.loadAllAttachFiles();
      $uibModalInstance.dismiss('cancel');
    });
  }else {
    alert("Please select file to attach..");
  }
};

 $scope.Assign_RejectPetition = function(status){
	 var url = "http://182.75.177.246:8080/engine-rest/task/"+taskId+"/complete";
	 if(status=='complete'){
		var post = {"variables":
						{"petitionversion":{"type":"String","value":"1","valueInfo":{}},
						"petitionyear":{"type":"String","value":"2017-18","valueInfo":{}},
						"petitionid":{"type":"String","value":Phid,"valueInfo":{}},
						"isverified":{"type":"boolean","value":true,"valueInfo":{}}}
						}
	}else{
			var post = {"variables":
						{"petitionversion":{"type":"String","value":"1","valueInfo":{}},
						"petitionyear":{"type":"String","value":"2017-18","valueInfo":{}},
						"petitionid":{"type":"String","value":Phid,"valueInfo":{}},
						"isverified":{"type":"boolean","value":false,"valueInfo":{}}}
						}
		}
	ApiServices.PostRequest(url,post,function(data){
			$rootScope.message = 'Your Petition has been sent Successfully.';
			localStorage.removeItem('ngStorage-petitionId');
			localStorage.removeItem('ngStorage-utilId');
			localStorage.removeItem('ngStorage-taskId');
			$state.go('dashboard',null);
	});

};



$scope.showTypeRef = function (data) {
  $localStorage.showTypeRef = ''
  $localStorage.showTypeRef = data;
  var showTypeRef = data;
//  attachmentTypeApi();
};
$scope.showTypeDesc = function(data){
  $localStorage.showTypeDesc = '';
  $localStorage.showTypeDesc = data;
  var showTypeDesc = data;
  console.log('showTypeDesc: ',showTypeDesc);
}


/*tab in form start*/

$scope.tab = 1;
// $scope.tab = 'tab'+$localStorage.formId;
$scope.setTab = function(newTab){
  $scope.tab = newTab;
};

$scope.isSet = function(tabNum){
  return $scope.tab === tabNum;
};



if($localStorage.utypeId==1 || $localStorage.utypeId==3){

  $scope.genco = true;
  $scope.discom = true;
  $scope.transco = false;

  $scope.test = $localStorage.formId;
  var Phid = $localStorage.petitionId;
  //var formId = $localStorage.formId;
  $scope.selectedTab = 'tab1';

  $scope.getDataByForm = function(uid,tab,data) {
    var result = [];
    result = data['vals'][uid]['valForStationType'];
    return result;
  }
  $scope.loadPlumpExcelData1 = function(formId){
    var ApiEndPoints = '/f1_1JExcel?form_id='+1+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
      ApiServices.fetchFormData(ApiEndPoints,function(data){
            $scope.subStations = data.data.vals;
            $scope.subStationsData = data.data;
             for(var jj=0;jj < data.data.vals.length;jj++){
                     data.data.vals[jj]['valForStationType'];
            }
     });
      $rootScope.currentRoute = $location.path();
  }

  $scope.loadPlumpExcelData1(1);
  $scope.loadHeader = function(){
  $rootScope.tableHeader1 = "";
  var ApiEndPoints = '/form_header1_1?form_id='+1+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
    ApiServices.fetchFormData(ApiEndPoints,function(data){
        $scope.formHeader = data.data.header;
        $rootScope.tableHeader1 = data.data.header;
    });
  }
  $scope.loadHeader();

  $scope.saveStationData = function(Seloption,formId){
      var postData = {};
      var raw = $scope.subStationsData;
      var url = 'f1_1Update';
      ApiServices.setSubstaionData1({
          message:'',
          data:raw,
          status:1,
          url:url,
        }, function(data){
           if(data.status==0){
             $scope.errorMessage=data.message;
           }
           if(data.status==1){
           $scope.updateMessage=data.message;
           }
           $timeout(function() {
               $scope.updateMessage='';
               $scope.errorMessage='';
          }, 5000);
        })
  };
/*tab in form start*/

  $scope.tab=1;
    $scope.setTab = function(newTab){
      $scope.tab = newTab;
    };

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };

}else if($localStorage.utypeId==2){
  $scope.genco = false;
  $scope.discom = false;
  $scope.transco = true;
      //$scope.test = $localStorage.formId;
    	var Phid = $localStorage.petitionId;
    //  var formId = $localStorage.formId;
      //$scope.selectedTab = 'ta23';
    $scope.getDataByForm = function(uid,tab,data) {
    		var result = [];
        result = data.vals;
        return result;
    }


    $scope.loadPlumpExcelData1 = function(formId){
       var ApiEndPoints = '/f1_13JExcel?ph_id='+$localStorage.petitionId+'&form_id=23'+'&user_id='+$localStorage.userId;
          ApiServices.fetchFormData(ApiEndPoints,function(data){
            		$scope.subStations = data.data.vals;
            		$scope.subStationsData = data.data;
         });
    	}

    $scope.loadPlumpExcelData1(23);
    $scope.saveStationData = function(Seloption,formId){
    	 var postData = {};
    	  var raw = $scope.subStationsData;
             var url = 'f1_13Update';
        ApiServices.setSubstaionData1({
            message:'',
            data:raw,
            status:1,
            url:url,
          }, function(data){
             if(data.status==0){
               $scope.errorMessage=data.message;
             }
             if(data.status==1){
             $scope.updateMessage=data.message;
             }
             $timeout(function() {
                 $scope.updateMessage='';
                     $scope.errorMessage='';
            }, 5000);

          })
    };

    $scope.loadHeader = function(){
      $rootScope.tableHeader23 = "";
      var ApiEndPoints = '/form_header1_13?form_id='+23+'&ph_id='+$localStorage.petitionId+'&user_id='+$localStorage.userId;
        ApiServices.fetchFormData(ApiEndPoints,function(data){
            $scope.formHeader = data.data.header;
            $rootScope.tableHeader23 = data.data.header;
        });
    }
    $scope.loadHeader()



    /*tab in form start*/

      $scope.tab = 1;
      $scope.setTab = function(newTab){
        $scope.tab = newTab;
      };

      $scope.isSet = function(tabNum){
        return $scope.tab === tabNum;
      }

    }
  }

}
    $scope.init();
  }]);
})();
