'use strict';
const express = require('express'),
    app = express(),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    ejs = require('ejs'),
    router = require('./route'),
    env = require('./env'),
    fileUpload = require('express-fileupload'),
    raven = require('raven');

// Middlewares
app.set('view engine', 'ejs');
app.set('views', __dirname + '/../view');
app.use(morgan('dev')); // 'dev' for development output
app.use(express.static(__dirname + '/../../frontend'));
//app.use(bodyParser.json({limit: env.request.limit}));
app.use(fileUpload());



function onError(err, req, res, next) {
    res.statusCode = 500;
    res.end(res.sentry + '\n');
}


// The request handler must be the first item
app.use(raven.middleware.express.requestHandler(env.sentry_dsn));

// Main Routing
app.use('/', router);

// The error handler must be before any other error middleware
app.use(raven.middleware.express.errorHandler(env.sentry_dsn));

// Optional fallthrough error handler
app.use(onError);

module.exports = app;
