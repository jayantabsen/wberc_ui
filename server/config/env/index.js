'use strict';

const path = require('path');
const uscore = require('underscore');

const commonConfig = {
    env: process.env.NODE_ENV,
    port: process.env.PORT || 4001
};

module.exports = uscore.extend(commonConfig, require('./' + process.env.NODE_ENV + '.js') || {});
