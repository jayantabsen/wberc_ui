'use strict';

const router = require('express').Router(),
    env = require('./env');

/*
apiRoute = require(__base + 'server/app/module');
router.use('/api', apiRoute);
router.post('/api',apiRoute);
*/

router.get('/*', function(req, res) {
  res.render('index', {
    
  });
});

module.exports = router;
